(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-towns-towns-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/towns/towns-edit/towns-edit.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/towns/towns-edit/towns-edit.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"kt-portlet\">\r\n\t<kt-portlet-header [title]=\"getComponentTitle()\" [class]=\"'kt-portlet__head--lg'\" [viewLoading$]=\"loading$\">\r\n\t\t<ng-container ktPortletTools>\r\n\t\t\t<a [routerLink]=\"['../']\" class=\"btn btn-secondary kt-margin-r-10\" mat-raised-button matTooltip=\"Назад к списку\">\r\n\t\t\t\t<i class=\"la la-arrow-left\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Назад</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-primary kt-margin-r-10\" color=\"primary\" (click)=\"onSubmit(false)\" mat-raised-button matTooltip=\"Сохранить и продолжить\">\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сохранить</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-secondary kt-margin-r-10\" (click)=\"reset()\" mat-raised-button matTooltip=\"Отменить изменения\">\r\n\t\t\t\t<i class=\"la la-cog\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сбросить</span>\r\n\t\t\t</a>\r\n\t\t\t<button mat-icon-button [matMenuTriggerFor]=\"menu\" color=\"primary\">\r\n\t\t\t\t<mat-icon>more_vert</mat-icon>\r\n\t\t\t</button>\r\n\t\t\t<mat-menu #menu=\"matMenu\">\r\n\t\t\t\t<button mat-menu-item color=\"primary\" (click)=\"onSubmit(true)\">Сохранить & Выйти</button>\r\n\t\t\t\t<!--<button mat-menu-item color=\"primary\">Save & Duplicate</button>-->\r\n\t\t\t\t<button mat-menu-item color=\"primary\" (click)=\"onSubmit(false)\">Сохранить & Продолжить</button>\r\n\t\t\t</mat-menu>\r\n\t\t</ng-container>\r\n\t</kt-portlet-header>\r\n\t<br>\r\n\t<!--begin::Form-->\r\n\t<kt-portlet-body>\r\n\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\t\t\t<kt-alert *ngIf=\"hasFormErrors\" type=\"warn\" [showCloseButton]=\"true\" [duration]=\"10000\" (close)=\"onAlertClose($event)\">\r\n\t\t\t\tОшибка! Форма не прошла валидацию.\r\n\t\t\t</kt-alert>\r\n\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t<ng-container [ngTemplateOutlet]=\"formTemplate\"></ng-container>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</kt-portlet-body>\r\n</div>\r\n\r\n<ng-template #formTemplate>\r\n\t<form class=\"kt-form kt-form--label-right\" [formGroup]=\"modelForm\" >\r\n\t\t<ng-container *ngFor=\"let control of formConfig(); trackBy:trackByFn\">\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'horizontal' && isFieldVisible(control.permission)\">\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'text' || control['type'] == 'password'  || control['type'] == 'email'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<input [type]=\"control['type']\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\">\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'select'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<select  formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\" class=\"form-control\">\r\n\t\t\t\t\t\t\t<option *ngIf=\"control.nullValue\" value=\"\">-- Выберите --</option>\r\n\t\t\t\t\t\t\t<option *ngFor=\"let option of control.options\" [value]=\"option.value\">{{option.label}}</option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'vertical'\"></ng-container>\r\n\t\t</ng-container>\r\n\t</form>\r\n</ng-template>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/towns/towns-list/towns-list.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/towns/towns-list/towns-list.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<kt-portlet>\r\n\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n\t\t<div class=\"kt-portlet__head-label\">\r\n\t\t\t<h3 class=\"kt-portlet__head-title\">\r\n\t\t\t\t{{title}}\r\n\t\t\t</h3>\r\n\t\t</div>\r\n\t\t<div class=\"kt-portlet__head-toolbar\">\r\n\t\t\t<div class=\"kt-portlet__head-wrapper\">\r\n\t\t\t\t<div class=\"dropdown dropdown-inline\">\r\n\t\t\t\t\t<button  (click)=\"redirectCreateModel();\" mat-raised-button>Добавить</button>&nbsp;\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body\">\r\n\t\t<div class=\"kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10\">\r\n\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-xl-8 order-2 order-xl-1\">\r\n\t\t\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t\t\t<div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--left\">\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Поиск...\" id=\"generalSearch\">\r\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--left\">\r\n\t\t\t\t\t\t\t<span><i class=\"la la-search\"></i></span>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body kt-portlet__body--fit\">\r\n\t\t<div class=\"my_datatable\" id=\"kt_datatable_towns\"></div>\r\n\t</div>\r\n</kt-portlet>\r\n");

/***/ }),

/***/ "./src/app/core/town/resolvers/town.ts":
/*!*********************************************!*\
  !*** ./src/app/core/town/resolvers/town.ts ***!
  \*********************************************/
/*! exports provided: TownEditResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TownEditResolver", function() { return TownEditResolver; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
/**
 * Created by Алексей on 09.02.2018.
 */



let TownEditResolver = class TownEditResolver {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    resolve(route, state) {
        if (route.queryParams.id) {
            return this.http.get('/api/town/' + route.queryParams.id);
        }
        else {
            return;
        }
    }
};
TownEditResolver.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] }
];
TownEditResolver = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
], TownEditResolver);



/***/ }),

/***/ "./src/app/views/pages/towns/towns-edit/towns-edit.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/pages/towns/towns-edit/towns-edit.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL3Rvd25zL3Rvd25zLWVkaXQvdG93bnMtZWRpdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/pages/towns/towns-edit/towns-edit.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/pages/towns/towns-edit/towns-edit.component.ts ***!
  \**********************************************************************/
/*! exports provided: TownsEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TownsEditComponent", function() { return TownsEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







let TownsEditComponent = class TownsEditComponent {
    constructor(subheader, userModel, router, titleService, http, Fb, cdr, route) {
        this.subheader = subheader;
        this.userModel = userModel;
        this.router = router;
        this.titleService = titleService;
        this.http = http;
        this.Fb = Fb;
        this.cdr = cdr;
        this.route = route;
        this.config = {
            orientation: 'horizontal'
        };
        this.title = "";
        this.hasFormErrors = false;
        this.model = {};
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.roles = {};
        this.errors = [];
        this.submitted = false;
    }
    ngOnInit() {
        this.createForm();
        this.initFormValues();
    }
    setTitles() {
        let breadcrumbs = [{
                title: 'Список городов',
                page: '/towns'
            }];
        this.subheader.setTitle("Города");
        if (this.state == 'edit') {
            this.title = "Редактирование города";
            breadcrumbs.push({
                title: 'Редактирование',
                page: '/towns/edit?id=' + this.modelForm.controls['id'].value,
            });
        }
        else {
            this.title = "Добавление города";
            breadcrumbs.push({
                title: 'Добавление',
                page: '/towns/edit'
            });
        }
        this.subheader.appendBreadcrumbs(breadcrumbs);
        this.titleService.setTitle(this.title);
    }
    getComponentTitle() {
        return this.title;
    }
    reset() {
        this.modelForm.reset();
    }
    onSubmit(redirect) {
        if (this.state == 'edit') {
            this.http.put('api/town', this.modelForm.value).subscribe((data) => {
                if (redirect)
                    this.router.navigate(['/towns']);
                this.hasFormErrors = false;
            }, (data) => {
                this.errors = data.error;
                this.hasFormErrors = true;
                this.cdr.detectChanges();
            });
        }
        else {
            this.http.post('api/town', this.modelForm.value).subscribe((data) => {
                if (redirect)
                    this.router.navigate(['/towns']);
                this.hasFormErrors = false;
            }, (data) => {
                this.errors = data.error;
                this.hasFormErrors = true;
                this.cdr.detectChanges();
            });
        }
    }
    initFormValues() {
        console.log(this.route.queryParams.subscribe((params) => {
            const id = params['id'];
            if (id > 0) {
                this.state = 'edit';
                this.route.data
                    .subscribe((data) => {
                    if (data['data']) {
                        this.modelForm.patchValue(data['data'], {
                            emitEvent: false,
                            onlySelf: true
                        });
                    }
                });
            }
            else {
                this.state = 'create';
            }
            this.setTitles();
        }));
    }
    createForm() {
        this.modelForm = this.Fb.group({
            id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            label: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            blocked: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
        });
    }
    formConfig() {
        return [
            {
                field: 'label',
                label: 'Название',
                type: 'text',
                permission: 'manage_towns'
            },
            {
                field: 'blocked',
                label: 'Блокировка',
                type: 'select',
                permission: 'manage_towns',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ]
            },
        ];
    }
    isFieldVisible(permission) {
        return this.userModel.hasPermissionTo(permission);
    }
    trackByFn(index, item) {
        return index;
    }
    getErrorMessage(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
        else {
            return '';
        }
    }
    hasError(field) {
        let classList = {
            'is-invalid': this.errors[field],
            'form-control': true,
        };
        return classList;
    }
    onAlertClose($event) {
        this.hasFormErrors = false;
    }
};
TownsEditComponent.ctorParameters = () => [
    { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_6__["SubheaderService"] },
    { type: _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
TownsEditComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'kt-towns-edit',
        template: __importDefault(__webpack_require__(/*! raw-loader!./towns-edit.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/towns/towns-edit/towns-edit.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./towns-edit.component.scss */ "./src/app/views/pages/towns/towns-edit/towns-edit.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_6__["SubheaderService"],
        _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], TownsEditComponent);



/***/ }),

/***/ "./src/app/views/pages/towns/towns-list/towns-list.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/pages/towns/towns-list/towns-list.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL3Rvd25zL3Rvd25zLWxpc3QvdG93bnMtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/pages/towns/towns-list/towns-list.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/pages/towns/towns-list/towns-list.component.ts ***!
  \**********************************************************************/
/*! exports provided: TownsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TownsListComponent", function() { return TownsListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _core_auth_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/auth/_services */ "./src/app/core/auth/_services/index.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};








let TownsListComponent = class TownsListComponent {
    constructor(subheader, cdr, auth, router, titleService, store, http) {
        this.subheader = subheader;
        this.cdr = cdr;
        this.auth = auth;
        this.router = router;
        this.titleService = titleService;
        this.store = store;
        this.http = http;
        this.title = 'Города';
        this.baseUrl = 'towns';
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"]();
    }
    ngOnInit() {
        this.subheader.setBreadcrumbs([
            {
                title: 'Список городов',
                url: 'users'
            }
        ]);
        console.log(this.subheader.breadcrumbs$);
        this.subheader.setTitle(this.title);
        this.titleService.setTitle(this.title);
        this.initDatatable();
        this.cdr.detectChanges();
    }
    redirectCreateModel() {
        this.router.navigate(['/towns/edit']);
    }
    initDatatable() {
        const token = this.auth.getUserToken();
        this.datatable = $('#kt_datatable_towns').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: 'api/towns',
                        headers: {
                            'Authorization': token,
                        },
                        map: (raw) => {
                            // sample data mapping
                            var dataSet = raw;
                            console.log(raw);
                            if (typeof raw.models !== 'undefined') {
                                dataSet = raw.models;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },
            // column sorting
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            // columns definition
            columns: this.getColumnsByRole(),
        });
        this.datatable.on('kt-datatable--on-layout-updated', () => {
            setTimeout(() => {
                $('.editModel').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    this.edit(id);
                });
                $('.deleteModel').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    if (confirm('Вы уверены что хотите удалть?')) {
                        this.delete(id);
                    }
                });
                $('.toggleStatus').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    this.toggleStatus(id);
                });
            });
        });
        this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
            this.store.dispatch(new _core_auth__WEBPACK_IMPORTED_MODULE_1__["Logout"]());
        });
    }
    toggleStatus(id) {
        this.http.post('/api/town/status/' + id, {}).subscribe(() => {
            this.datatable.reload();
        });
    }
    edit(id) {
        this.router.navigate(['/towns/edit',], { queryParams: { id: id } });
    }
    delete(id) {
        this.http.delete('/api/town/' + id).subscribe(() => {
            this.datatable.reload();
        });
    }
    getColumnsByRole() {
        var config = [];
        if (this.user.hasPermissionTo('manage_' + this.baseUrl)) {
            config = [{
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    width: 40,
                    type: 'number',
                    selector: false,
                    textAlign: 'center',
                    autoHide: !1,
                }, {
                    field: 'label',
                    title: 'Название',
                    autoHide: !1
                },];
        }
        if (this.user.hasPermissionTo('manage_towns')) {
            config.push({
                field: 'blocked',
                title: 'Статус',
                width: 140,
                template: function (row, index, datatable) {
                    if (row['blocked'] == 1) {
                        var html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-danger btn-sm btn-pill">Заблокирован</button>';
                    }
                    else {
                        var html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-success btn-sm btn-pill">Активен</button>';
                    }
                    return html;
                }
            });
        }
        config.push({
            field: 'Actions',
            title: 'Действия',
            sortable: false,
            overflow: 'visible',
            textAlign: 'center',
            autoHide: !1,
            that: this,
            template: function (row, index, datatable) {
                let visible = this.that.user.hasPermissionTo('manage_towns');
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                let result = '<button href="javascript:;" data-id=' + row['id'] + ' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
                        <i class="la la-edit"></i>\
                    </button>';
                if (visible) {
                    result +=
                        '<button href="javascript:;" data-id=' + row['id'] + ' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
                        <i class="la la-trash"></i>\
                    </button>';
                }
                return result;
            },
        });
        return config;
    }
};
TownsListComponent.ctorParameters = () => [
    { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_5__["SubheaderService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] },
    { type: _core_auth_services__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }
];
TownsListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'kt-towns-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./towns-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/towns/towns-list/towns-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./towns-list.component.scss */ "./src/app/views/pages/towns/towns-list/towns-list.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_5__["SubheaderService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
        _core_auth_services__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
        _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
], TownsListComponent);



/***/ }),

/***/ "./src/app/views/pages/towns/towns.module.ts":
/*!***************************************************!*\
  !*** ./src/app/views/pages/towns/towns.module.ts ***!
  \***************************************************/
/*! exports provided: TownsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TownsModule", function() { return TownsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _towns_edit_towns_edit_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./towns-edit/towns-edit.component */ "./src/app/views/pages/towns/towns-edit/towns-edit.component.ts");
/* harmony import */ var _towns_list_towns_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./towns-list/towns-list.component */ "./src/app/views/pages/towns/towns-list/towns-list.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _core_town_resolvers_town__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/town/resolvers/town */ "./src/app/core/town/resolvers/town.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/auth/_interceptor/interceptor */ "./src/app/core/auth/_interceptor/interceptor.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _partials_partials_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../partials/partials.module */ "./src/app/views/partials/partials.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};












const routes = [
    { path: '', redirectTo: 'list' },
    { path: 'list', component: _towns_list_towns_list_component__WEBPACK_IMPORTED_MODULE_3__["TownsListComponent"] },
    { path: 'edit', component: _towns_edit_towns_edit_component__WEBPACK_IMPORTED_MODULE_2__["TownsEditComponent"], resolve: {
            data: _core_town_resolvers_town__WEBPACK_IMPORTED_MODULE_5__["TownEditResolver"]
        } }
];
let TownsModule = class TownsModule {
};
TownsModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        declarations: [_towns_edit_towns_edit_component__WEBPACK_IMPORTED_MODULE_2__["TownsEditComponent"], _towns_list_towns_list_component__WEBPACK_IMPORTED_MODULE_3__["TownsListComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
            _partials_partials_module__WEBPACK_IMPORTED_MODULE_11__["PartialsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__["TranslateModule"].forChild(),
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatNativeDateModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressSpinnerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogModule"]
        ],
        providers: [_core_town_resolvers_town__WEBPACK_IMPORTED_MODULE_5__["TownEditResolver"],
            {
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
                useClass: _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_7__["JwtInterceptor"],
                multi: true
            },],
    })
], TownsModule);



/***/ })

}]);
//# sourceMappingURL=app-views-pages-towns-towns-module-es2015.js.map