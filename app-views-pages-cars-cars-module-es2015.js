(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-cars-cars-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/cars/cars-edit/cars-edit.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/cars/cars-edit/cars-edit.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"kt-portlet\">\r\n\t<kt-portlet-header [title]=\"getComponentTitle()\" [class]=\"'kt-portlet__head--lg'\" [viewLoading$]=\"loading$\">\r\n\t\t<ng-container ktPortletTools>\r\n\t\t\t<a [routerLink]=\"['../']\" class=\"btn btn-secondary kt-margin-r-10\" mat-raised-button matTooltip=\"Назад к списку\">\r\n\t\t\t\t<i class=\"la la-arrow-left\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Назад</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" [disabled]=\"buttonDisabled\" class=\"btn btn-primary kt-margin-r-10\" color=\"primary\" (click)=\"onSubmit(true)\" mat-raised-button matTooltip=\"Сохранить и продолжить\">\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сохранить</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-secondary kt-margin-r-10\" (click)=\"reset()\" mat-raised-button matTooltip=\"Отменить изменения\">\r\n\t\t\t\t<i class=\"la la-cog\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сбросить</span>\r\n\t\t\t</a>\r\n\t\t\t<button mat-icon-button [matMenuTriggerFor]=\"menu\" color=\"primary\">\r\n\t\t\t\t<mat-icon>more_vert</mat-icon>\r\n\t\t\t</button>\r\n\t\t\t<mat-menu #menu=\"matMenu\">\r\n\t\t\t\t<button mat-menu-item  [disabled]=\"buttonDisabled\" color=\"primary\" (click)=\"onSubmit(true)\">Сохранить & Выйти</button>\r\n\t\t\t\t<!--<button mat-menu-item color=\"primary\">Save & Duplicate</button>-->\r\n\t\t\t\t<button mat-menu-item  [disabled]=\"buttonDisabled\" color=\"primary\" (click)=\"onSubmit(false)\">Сохранить & Продолжить</button>\r\n\t\t\t</mat-menu>\r\n\t\t</ng-container>\r\n\t</kt-portlet-header>\r\n\t<br>\r\n\t<!--begin::Form-->\r\n\t<kt-portlet-body>\r\n\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\t\t\t<kt-alert *ngIf=\"hasFormErrors\" type=\"warn\" [showCloseButton]=\"true\" [duration]=\"10000\" (close)=\"onAlertClose($event)\">\r\n\t\t\t\tОшибка! Форма не прошла валидацию.\r\n\t\t\t</kt-alert>\r\n\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t<ng-container [ngTemplateOutlet]=\"formTemplate\"></ng-container>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</kt-portlet-body>\r\n</div>\r\n\r\n<ng-template #formTemplate>\r\n\t<form class=\"kt-form kt-form--label-right\" [formGroup]=\"modelForm\" >\r\n\t\t<ng-container *ngFor=\"let control of formConfig(); trackBy:trackByFn\">\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'horizontal' && isFieldVisible(control.permission)\">\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'text' || control['type'] == 'password'  || control['type'] == 'email'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<input [type]=\"control['type']\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\">\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'file'\" class=\"form-group row validated\" [formGroupName]=\"control.field\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<input [type]=\"control['type']\" (change)=\"onFileChange($event,control.field)\" formControlName=\"value\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\">\r\n\t\t\t\t\t\t<img style=\"width:250px\" [src]=\"getImageSource(control.field)\" [id]=\"control.field+'_preview'\" [ngClass]=\"{'hidden':!modelForm.controls[control.field]}\">\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n<!--\t\t\t\t\t\t<img *ngIf=\"modelForm.controls[control.field].value\" [alt]=\"control.label\" [id]=\"control.field+'_preview'\" src=\"\">-->\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'select'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<select  formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\" class=\"form-control\">\r\n\t\t\t\t\t\t\t<option *ngIf=\"control.nullValue\" value=\"\">-- Выберите --</option>\r\n\t\t\t\t\t\t\t<option *ngFor=\"let option of control.options\" [value]=\"option.value\">{{option.label}}</option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'vertical'\"></ng-container>\r\n\t\t</ng-container>\r\n\t</form>\r\n</ng-template>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/cars/cars-list/cars-list.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/cars/cars-list/cars-list.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<kt-portlet>\r\n\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n\t\t<div class=\"kt-portlet__head-label\">\r\n\t\t\t<h3 class=\"kt-portlet__head-title\">\r\n\t\t\t\t{{title}}\r\n\t\t\t</h3>\r\n\t\t</div>\r\n\t\t<div class=\"kt-portlet__head-toolbar\">\r\n\t\t\t<div class=\"kt-portlet__head-wrapper\">\r\n\t\t\t\t<div class=\"dropdown dropdown-inline\">\r\n\t\t\t\t\t<button  (click)=\"redirectCreateModel();\" mat-raised-button>Добавить</button>&nbsp;\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body\">\r\n\t\t<div class=\"kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10\">\r\n\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-xl-8 order-2 order-xl-1\">\r\n\t\t\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t\t\t<div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--left\">\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Поиск...\" id=\"generalSearch\">\r\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--left\">\r\n\t\t\t\t\t\t\t<span><i class=\"la la-search\"></i></span>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body kt-portlet__body--fit\">\r\n\t\t<div class=\"my_datatable\" id=\"car\"></div>\r\n\t</div>\r\n</kt-portlet>\r\n");

/***/ }),

/***/ "./src/app/core/car/resolvers/car.ts":
/*!*******************************************!*\
  !*** ./src/app/core/car/resolvers/car.ts ***!
  \*******************************************/
/*! exports provided: CarEditResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarEditResolver", function() { return CarEditResolver; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
/**
 * Created by Алексей on 09.02.2018.
 */



let CarEditResolver = class CarEditResolver {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    resolve(route, state) {
        if (route.queryParams.id) {
            return this.http.get('/api/car/' + route.queryParams.id);
        }
        else {
            return;
        }
    }
};
CarEditResolver.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] }
];
CarEditResolver = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
], CarEditResolver);



/***/ }),

/***/ "./src/app/views/pages/cars/cars-edit/cars-edit.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/views/pages/cars/cars-edit/cars-edit.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2NhcnMvY2Fycy1lZGl0L2NhcnMtZWRpdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/pages/cars/cars-edit/cars-edit.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/views/pages/cars/cars-edit/cars-edit.component.ts ***!
  \*******************************************************************/
/*! exports provided: CarsEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarsEditComponent", function() { return CarsEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _core_car_models_car__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../core/car/_models/car */ "./src/app/core/car/_models/car.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};








let CarsEditComponent = class CarsEditComponent {
    constructor(subheader, userModel, router, titleService, http, Fb, cdr, route) {
        this.subheader = subheader;
        this.userModel = userModel;
        this.router = router;
        this.titleService = titleService;
        this.http = http;
        this.Fb = Fb;
        this.cdr = cdr;
        this.route = route;
        this.config = {
            orientation: 'horizontal'
        };
        this.buttonDisabled = false;
        this.title = '';
        this.baseUrl = 'car';
        this.hasFormErrors = false;
        this.model = new _core_car_models_car__WEBPACK_IMPORTED_MODULE_7__["Car"]();
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_5__["User"]();
        this.roles = {};
        this.errors = [];
        this.submitted = false;
    }
    ngOnInit() {
        this.createForm();
        this.initFormValues();
        console.log(this.modelForm);
    }
    ngAfterViewInit() {
        this.setInputMasks();
    }
    setTitles() {
        let breadcrumbs = [{
                title: 'Список автомобилей',
                page: '/' + this.baseUrl + 's'
            }];
        this.subheader.setTitle('Услуги');
        if (this.state === 'edit') {
            this.title = 'Редактирование автомобилей';
            breadcrumbs.push({
                title: 'Редактирование',
                page: '/' + this.baseUrl + 's/edit?id=' + this.modelForm.controls['id'].value,
            });
        }
        else {
            this.title = 'Добавление автомобилей';
            breadcrumbs.push({
                title: 'Добавление',
                page: '/' + this.baseUrl + 's/edit'
            });
        }
        this.subheader.appendBreadcrumbs(breadcrumbs);
        this.titleService.setTitle(this.title);
    }
    getComponentTitle() {
        return this.title;
    }
    reset() {
        this.modelForm.reset();
        this.setFormValues();
    }
    setInputMasks() {
        // $('#number').inputmask('mask', {
        //     'mask': 'A 999 AA',
        //     oncomplete: (e) => {
        //         console.log(e);
        //         this.modelForm.controls['number'].setValue(e.target.value);
        //     },
        // });
        $('#year').inputmask('mask', {
            'mask': '9999',
            oncomplete: (e) => {
                console.log(e);
                this.modelForm.controls['year'].setValue(e.target.value);
            },
        });
    }
    setFormValues() {
        this.route.data
            .subscribe((data) => {
            if (data['data']) {
                this.modelForm.patchValue(data['data'], {
                    emitEvent: false,
                    onlySelf: true
                });
                data['data']['images'].forEach((item) => {
                    this.modelForm.controls[item['type']]['name'] = item['name'];
                });
            }
        });
    }
    onFileChange(event, field) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                let control = this.modelForm.controls[field];
                let result = '' + reader.result;
                control.controls['base64'].setValue(result.split(',')[1]);
                $('#' + field + '_preview')
                    .attr('src', e.target.result)
                    .width(250);
            };
        }
    }
    onSubmit(redirect) {
        let request;
        this.cdr.detectChanges();
        if (this.state === 'edit') {
            request = this.http.put('api/' + this.baseUrl, this.modelForm.value);
        }
        else {
            request = this.http.post('api/' + this.baseUrl, this.modelForm.value);
        }
        this.buttonDisabled = true;
        request.subscribe((data) => {
            if (redirect) {
                this.router.navigate(['/' + this.baseUrl + 's']);
            }
            this.hasFormErrors = false;
            this.errors = [];
            this.buttonDisabled = false;
        }, (data) => {
            this.addErrors(data.error);
            this.hasFormErrors = true;
            this.buttonDisabled = false;
            this.cdr.detectChanges();
        });
    }
    initFormValues() {
        this.route.queryParams.subscribe((params) => {
            const id = params['id'];
            if (id > 0) {
                this.state = 'edit';
                this.setFormValues();
            }
            else {
                this.state = 'create';
            }
            this.setTitles();
        });
    }
    createForm() {
        this.modelForm = this.Fb.group({
            id: [this.model.id, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mark: [this.model.mark, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            model: [this.model.model, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            number: [this.model.number, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            color: [this.model.color, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            year: [this.model.year, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            seat: [this.model.seat, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            conditioner: [this.model.conditioner, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            bank_card: [this.model.bank_card, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            baby_seat: [this.model.baby_seat, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            baggage_count: [this.model.baggage_count, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            blocked: [this.model.blocked, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            forward: this.createImageForm(),
            // right: this.createImageForm(),
            salon: this.createImageForm(),
            baggage: this.createImageForm(),
        });
    }
    createImageForm() {
        return this.Fb.group({
            name: '',
            value: '',
            base64: '',
            uploaded: false
        });
    }
    getImageSource(name) {
        if (this.modelForm.controls[name]['uploaded'] == true) {
            return false;
        }
        else {
            return 'api/public/images/car_images/' + this.modelForm.controls['id'].value + '/' + this.modelForm.controls[name]['name'];
        }
    }
    formConfig() {
        return [
            {
                field: 'forward',
                label: 'Фото перед - правый бок',
                type: 'file',
                permission: 'manage_' + this.baseUrl + 's'
            },
            // {
            //     field: 'right',
            //     label: 'Фото справа',
            //     type: 'file',
            //     permission: 'manage_' + this.baseUrl + 's'
            // },
            {
                field: 'salon',
                label: 'Фото салона',
                type: 'file',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'baggage',
                label: 'Фото багажника',
                type: 'file',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'mark',
                label: 'Марка',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'model',
                label: 'Модель',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'number',
                label: 'Гос.номер',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'color',
                label: 'Цвет',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'year',
                label: 'Год',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'seat',
                label: 'Кол-во пассажирских мест',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'baggage_count',
                label: 'Кол-во багажных мест',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'bank_card',
                label: 'Оплата банковской картой',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            }, {
                field: 'baby_seat',
                label: 'Детское сиденье',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            }, {
                field: 'conditioner',
                label: 'Кондиционер',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            }, {
                field: 'blocked',
                label: 'Блокировка',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            },
        ];
    }
    addErrors(errors) {
        this.errors = errors;
        // errors.forEach((item) => {
        // 	this.errors.push(item.split('.')[0]);
        // });
    }
    isFieldVisible(permission) {
        return this.userModel.hasPermissionTo(permission);
    }
    trackByFn(index, item) {
        return index;
    }
    getErrorMessage(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
        else {
            return '';
        }
    }
    hasError(field) {
        return {
            'is-invalid': this.errors[field],
            'form-control': true,
        };
    }
    onAlertClose() {
        this.hasFormErrors = false;
    }
};
CarsEditComponent.ctorParameters = () => [
    { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"] },
    { type: _core_auth__WEBPACK_IMPORTED_MODULE_5__["User"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
CarsEditComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'kt-cars-edit',
        template: __importDefault(__webpack_require__(/*! raw-loader!./cars-edit.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/cars/cars-edit/cars-edit.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./cars-edit.component.scss */ "./src/app/views/pages/cars/cars-edit/cars-edit.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"],
        _core_auth__WEBPACK_IMPORTED_MODULE_5__["User"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], CarsEditComponent);



/***/ }),

/***/ "./src/app/views/pages/cars/cars-list/cars-list.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/views/pages/cars/cars-list/cars-list.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2NhcnMvY2Fycy1saXN0L2NhcnMtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/pages/cars/cars-list/cars-list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/views/pages/cars/cars-list/cars-list.component.ts ***!
  \*******************************************************************/
/*! exports provided: CarsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarsListComponent", function() { return CarsListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _core_auth_services__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../core/auth/_services */ "./src/app/core/auth/_services/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};








let CarsListComponent = class CarsListComponent {
    constructor(subheader, cdr, auth, router, titleService, store, http) {
        this.subheader = subheader;
        this.cdr = cdr;
        this.auth = auth;
        this.router = router;
        this.titleService = titleService;
        this.store = store;
        this.http = http;
        this.title = 'Автомобили';
        this.baseUrl = 'car';
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_6__["User"]();
    }
    ngOnInit() {
        this.subheader.setBreadcrumbs([
            {
                title: 'Список автомобилей',
                url: 'services'
            }
        ]);
        console.log(this.subheader.breadcrumbs$);
        this.subheader.setTitle(this.title);
        this.titleService.setTitle(this.title);
        this.initDatatable();
        this.cdr.detectChanges();
    }
    redirectCreateModel() {
        this.router.navigate(['/' + this.baseUrl + 's/edit']);
    }
    initDatatable() {
        const token = this.auth.getUserToken();
        this.datatable = $('#car').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: 'api/' + this.baseUrl + 's',
                        headers: {
                            'Authorization': token,
                        },
                        map: (raw) => {
                            // sample data mapping
                            var dataSet = raw;
                            console.log(raw);
                            if (typeof raw.models !== 'undefined') {
                                dataSet = raw.models;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },
            // column sorting
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            // columns definition
            columns: this.getColumnsByRole(),
        });
        this.datatable.on('kt-datatable--on-layout-updated', () => {
            setTimeout(() => {
                $('.editModel').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    this.edit(id);
                });
                $('.deleteModel').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    if (confirm('Вы уверены что хотите удалть?')) {
                        this.delete(id);
                    }
                });
                $('.toggleStatus').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    this.toggleStatus(id);
                });
            });
        });
        this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
            this.router.navigate(['/auth/login']);
        });
    }
    toggleStatus(id) {
        this.http.post('/api/' + this.baseUrl + '/status/' + id, {}).subscribe(() => {
            this.datatable.reload();
        });
    }
    edit(id) {
        this.router.navigate(['/' + this.baseUrl + 's/edit',], { queryParams: { id: id } });
    }
    delete(id) {
        this.http.delete('/api/' + this.baseUrl + '/' + id).subscribe(() => {
            this.datatable.reload();
        });
    }
    getColumnsByRole() {
        var config = [];
        if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
            config = [{
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    width: 40,
                    type: 'number',
                    selector: false,
                    textAlign: 'center',
                    autoHide: !1,
                }, {
                    field: 'mark',
                    title: 'Марка',
                    autoHide: !1
                }, {
                    field: 'model',
                    title: 'Модель',
                    autoHide: !1
                }, {
                    field: 'number',
                    title: 'Номер',
                    autoHide: !1
                }, {
                    field: 'color',
                    title: 'Цвет',
                    autoHide: !1
                },];
        }
        if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
            config.push({
                field: 'blocked',
                title: 'Статус',
                width: 140,
                template: function (row, index, datatable) {
                    if (row['blocked'] == 1) {
                        var html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-danger btn-sm btn-pill">Заблокирован</button>';
                    }
                    else {
                        var html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-success btn-sm btn-pill">Активен</button>';
                    }
                    return html;
                }
            });
        }
        config.push({
            field: 'Actions',
            title: 'Действия',
            sortable: false,
            overflow: 'visible',
            textAlign: 'center',
            autoHide: !1,
            template: (row, index, datatable) => {
                let visible = this.user.hasPermissionTo('manage_' + this.baseUrl + 's');
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                let result = '<button href="javascript:;" data-id=' + row['id'] + ' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
                        <i class="la la-edit"></i>\
                    </button>';
                if (visible) {
                    result +=
                        '<button href="javascript:;" data-id=' + row['id'] + ' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
                        <i class="la la-trash"></i>\
                    </button>';
                }
                return result;
            },
        });
        return config;
    }
};
CarsListComponent.ctorParameters = () => [
    { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] },
    { type: _core_auth_services__WEBPACK_IMPORTED_MODULE_1__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"] }
];
CarsListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'kt-cars-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./cars-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/cars/cars-list/cars-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./cars-list.component.scss */ "./src/app/views/pages/cars/cars-list/cars-list.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
        _core_auth_services__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
        _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"]])
], CarsListComponent);



/***/ }),

/***/ "./src/app/views/pages/cars/cars.module.ts":
/*!*************************************************!*\
  !*** ./src/app/views/pages/cars/cars.module.ts ***!
  \*************************************************/
/*! exports provided: CarsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarsModule", function() { return CarsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _cars_list_cars_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cars-list/cars-list.component */ "./src/app/views/pages/cars/cars-list/cars-list.component.ts");
/* harmony import */ var _cars_edit_cars_edit_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cars-edit/cars-edit.component */ "./src/app/views/pages/cars/cars-edit/cars-edit.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../core/auth/_interceptor/interceptor */ "./src/app/core/auth/_interceptor/interceptor.ts");
/* harmony import */ var _partials_partials_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../partials/partials.module */ "./src/app/views/partials/partials.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _core_car_resolvers_car__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../core/car/resolvers/car */ "./src/app/core/car/resolvers/car.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};












const routes = [
    { path: '', component: _cars_list_cars_list_component__WEBPACK_IMPORTED_MODULE_2__["CarsListComponent"] },
    { path: 'edit', component: _cars_edit_cars_edit_component__WEBPACK_IMPORTED_MODULE_3__["CarsEditComponent"], resolve: { data: _core_car_resolvers_car__WEBPACK_IMPORTED_MODULE_11__["CarEditResolver"] } }
];
let CarsModule = class CarsModule {
};
CarsModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        declarations: [_cars_list_cars_list_component__WEBPACK_IMPORTED_MODULE_2__["CarsListComponent"], _cars_edit_cars_edit_component__WEBPACK_IMPORTED_MODULE_3__["CarsEditComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _partials_partials_module__WEBPACK_IMPORTED_MODULE_8__["PartialsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(routes),
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__["TranslateModule"].forChild(),
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginatorModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"]
        ],
        providers: [_core_car_resolvers_car__WEBPACK_IMPORTED_MODULE_11__["CarEditResolver"],
            {
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"],
                useClass: _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_7__["JwtInterceptor"],
                multi: true
            },],
    })
], CarsModule);



/***/ })

}]);
//# sourceMappingURL=app-views-pages-cars-cars-module-es2015.js.map