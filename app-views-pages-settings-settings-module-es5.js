function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-settings-settings-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/settings/settings.component.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/settings/settings.component.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPagesSettingsSettingsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"kt-portlet\">\r\n\t<kt-portlet-header [title]=\"getComponentTitle()\" [class]=\"'kt-portlet__head--lg'\" [viewLoading$]=\"loading$\">\r\n\t\t<ng-container ktPortletTools>\r\n\t\t\t<a [routerLink]=\"['../']\" class=\"btn btn-secondary kt-margin-r-10\" mat-raised-button matTooltip=\"Назад к списку\">\r\n\t\t\t\t<i class=\"la la-arrow-left\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Назад</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-primary kt-margin-r-10\" color=\"primary\" (click)=\"onSubmit(false)\" mat-raised-button matTooltip=\"Сохранить и продолжить\">\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сохранить</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-secondary kt-margin-r-10\" (click)=\"reset()\" mat-raised-button matTooltip=\"Отменить изменения\">\r\n\t\t\t\t<i class=\"la la-cog\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сбросить</span>\r\n\t\t\t</a>\r\n\t\t\t<button mat-icon-button [matMenuTriggerFor]=\"menu\" color=\"primary\">\r\n\t\t\t\t<mat-icon>more_vert</mat-icon>\r\n\t\t\t</button>\r\n\t\t\t<mat-menu #menu=\"matMenu\">\r\n\t\t\t\t<button mat-menu-item color=\"primary\" (click)=\"onSubmit(true)\">Сохранить & Выйти</button>\r\n\t\t\t\t<!--<button mat-menu-item color=\"primary\">Save & Duplicate</button>-->\r\n\t\t\t\t<button mat-menu-item color=\"primary\" (click)=\"onSubmit(false)\">Сохранить & Продолжить</button>\r\n\t\t\t</mat-menu>\r\n\t\t</ng-container>\r\n\t</kt-portlet-header>\r\n\t<br>\r\n\t<!--begin::Form-->\r\n\t<kt-portlet-body>\r\n\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\t\t\t<kt-alert *ngIf=\"hasFormErrors\" type=\"warn\" [showCloseButton]=\"true\" [duration]=\"10000\" (close)=\"onAlertClose($event)\">\r\n\t\t\t\tОшибка! Форма не прошла валидацию.\r\n\t\t\t</kt-alert>\r\n\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t<ng-container [ngTemplateOutlet]=\"formTemplate\"></ng-container>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</kt-portlet-body>\r\n</div>\r\n\r\n<ng-template #formTemplate>\r\n\t<form class=\"kt-form kt-form--label-right\" [formGroup]=\"modelForm\" >\r\n\t\t<ng-container *ngFor=\"let control of formConfig(); trackBy:trackByFn\">\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'horizontal' && isFieldVisible(control.permission)\">\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'text' || control['type'] == 'password'  || control['type'] == 'email'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<input [type]=\"control['type']\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\">\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'select'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<select  formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\" class=\"form-control\">\r\n\t\t\t\t\t\t\t<option *ngIf=\"control.nullValue\" value=\"\">-- Выберите --</option>\r\n\t\t\t\t\t\t\t<option *ngFor=\"let option of control.options\" [value]=\"option.value\">{{option.label}}</option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'vertical'\"></ng-container>\r\n\t\t</ng-container>\r\n\t</form>\r\n</ng-template>\r\n";
    /***/
  },

  /***/
  "./src/app/core/settings/resolvers/settings.ts":
  /*!*****************************************************!*\
    !*** ./src/app/core/settings/resolvers/settings.ts ***!
    \*****************************************************/

  /*! exports provided: SettingsEditResolver */

  /***/
  function srcAppCoreSettingsResolversSettingsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsEditResolver", function () {
      return SettingsEditResolver;
    });
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };
    /**
     * Created by Алексей on 09.02.2018.
     */


    var SettingsEditResolver = /*#__PURE__*/function () {
      function SettingsEditResolver(http, activatedRoute) {
        _classCallCheck(this, SettingsEditResolver);

        this.http = http;
        this.activatedRoute = activatedRoute;
      }

      _createClass(SettingsEditResolver, [{
        key: "resolve",
        value: function resolve(route, state) {
          return this.http.get('/api/setting');
        }
      }]);

      return SettingsEditResolver;
    }();

    SettingsEditResolver.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]
      }];
    };

    SettingsEditResolver = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])], SettingsEditResolver);
    /***/
  },

  /***/
  "./src/app/views/pages/settings/settings.component.scss":
  /*!**************************************************************!*\
    !*** ./src/app/views/pages/settings/settings.component.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPagesSettingsSettingsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/pages/settings/settings.component.ts":
  /*!************************************************************!*\
    !*** ./src/app/views/pages/settings/settings.component.ts ***!
    \************************************************************/

  /*! exports provided: SettingsComponent */

  /***/
  function srcAppViewsPagesSettingsSettingsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsComponent", function () {
      return SettingsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _core_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../core/auth */
    "./src/app/core/auth/index.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _core_base_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../core/_base/layout */
    "./src/app/core/_base/layout/index.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SettingsComponent = /*#__PURE__*/function () {
      function SettingsComponent(subheader, userModel, router, titleService, http, Fb, cdr, route) {
        _classCallCheck(this, SettingsComponent);

        this.subheader = subheader;
        this.userModel = userModel;
        this.router = router;
        this.titleService = titleService;
        this.http = http;
        this.Fb = Fb;
        this.cdr = cdr;
        this.route = route;
        this.config = {
          orientation: 'horizontal'
        };
        this.title = "";
        this.baseUrl = 'setting';
        this.hasFormErrors = false;
        this.model = {};
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.roles = {};
        this.errors = [];
        this.submitted = false;
      }

      _createClass(SettingsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.createForm();
          this.initFormValues();
        }
      }, {
        key: "setTitles",
        value: function setTitles() {
          var breadcrumbs = [{
            title: 'Параметры',
            page: '/' + this.baseUrl + 's'
          }];
          this.subheader.setTitle("Параметры");
          this.title = "Редактирование параметров";
          breadcrumbs.push({
            title: 'Редактирование',
            page: '/' + this.baseUrl + 's/edit'
          });
          this.subheader.setBreadcrumbs(breadcrumbs);
          this.titleService.setTitle(this.title);
        }
      }, {
        key: "getComponentTitle",
        value: function getComponentTitle() {
          return this.title;
        }
      }, {
        key: "reset",
        value: function reset() {
          this.modelForm.reset();
          this.setFormValues();
        }
      }, {
        key: "setFormValues",
        value: function setFormValues() {
          var _this = this;

          this.route.data.subscribe(function (data) {
            if (data['data']) {
              _this.modelForm.patchValue(data['data'], {
                emitEvent: false,
                onlySelf: true
              });
            }
          });
        }
      }, {
        key: "onSubmit",
        value: function onSubmit(redirect) {
          var _this2 = this;

          var request;

          if (this.state == 'edit') {
            request = this.http.put('api/' + this.baseUrl, this.modelForm.value);
          } else {
            request = this.http.post('api/' + this.baseUrl, this.modelForm.value);
          }

          request.subscribe(function (data) {
            if (redirect) _this2.router.navigate(['/' + _this2.baseUrl + 's']);
            _this2.hasFormErrors = false;
          }, function (data) {
            _this2.errors = data.error;
            _this2.hasFormErrors = true;

            _this2.cdr.detectChanges();
          });
        }
      }, {
        key: "initFormValues",
        value: function initFormValues() {
          var _this3 = this;

          this.route.queryParams.subscribe(function (params) {
            _this3.state = 'edit';

            _this3.setFormValues();

            _this3.setTitles();
          });
        }
      }, {
        key: "createForm",
        value: function createForm() {
          this.modelForm = this.Fb.group({
            order_percent: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            order_time: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]
          });
        }
      }, {
        key: "formConfig",
        value: function formConfig() {
          return [{
            field: 'order_percent',
            label: 'Процент от заказа',
            type: 'text',
            permission: 'manage_settings'
          }, {
            field: 'order_time',
            label: 'Время на заказ',
            type: 'text',
            permission: 'manage_settings'
          }];
        }
      }, {
        key: "isFieldVisible",
        value: function isFieldVisible(permission) {
          return this.userModel.hasPermissionTo(permission);
        }
      }, {
        key: "trackByFn",
        value: function trackByFn(index, item) {
          return index;
        }
      }, {
        key: "getErrorMessage",
        value: function getErrorMessage(field) {
          if (this.errors[field]) {
            return this.errors[field][0];
          } else {
            return '';
          }
        }
      }, {
        key: "hasError",
        value: function hasError(field) {
          var classList = {
            'is-invalid': this.errors[field],
            'form-control': true
          };
          return classList;
        }
      }, {
        key: "onAlertClose",
        value: function onAlertClose($event) {
          this.hasFormErrors = false;
        }
      }]);

      return SettingsComponent;
    }();

    SettingsComponent.ctorParameters = function () {
      return [{
        type: _core_base_layout__WEBPACK_IMPORTED_MODULE_6__["SubheaderService"]
      }, {
        type: _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    SettingsComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'kt-settings',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./settings.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/settings/settings.component.html"))["default"],
      styles: [__importDefault(__webpack_require__(
      /*! ./settings.component.scss */
      "./src/app/views/pages/settings/settings.component.scss"))["default"]]
    }), __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_6__["SubheaderService"], _core_auth__WEBPACK_IMPORTED_MODULE_1__["User"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], SettingsComponent);
    /***/
  },

  /***/
  "./src/app/views/pages/settings/settings.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/views/pages/settings/settings.module.ts ***!
    \*********************************************************/

  /*! exports provided: SettingsModule */

  /***/
  function srcAppViewsPagesSettingsSettingsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsModule", function () {
      return SettingsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _settings_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./settings.component */
    "./src/app/views/pages/settings/settings.component.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _core_settings_resolvers_settings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../core/settings/resolvers/settings */
    "./src/app/core/settings/resolvers/settings.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _partials_partials_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../partials/partials.module */
    "./src/app/views/partials/partials.module.ts");
    /* harmony import */


    var _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../core/auth/_interceptor/interceptor */
    "./src/app/core/auth/_interceptor/interceptor.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var routes = [{
      path: '',
      component: _settings_component__WEBPACK_IMPORTED_MODULE_2__["SettingsComponent"],
      resolve: {
        data: _core_settings_resolvers_settings__WEBPACK_IMPORTED_MODULE_4__["SettingsEditResolver"]
      }
    }];

    var SettingsModule = function SettingsModule() {
      _classCallCheck(this, SettingsModule);
    };

    SettingsModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      declarations: [_settings_component__WEBPACK_IMPORTED_MODULE_2__["SettingsComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], _partials_partials_module__WEBPACK_IMPORTED_MODULE_9__["PartialsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateModule"].forChild(), _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressSpinnerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTabsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"]],
      providers: [_core_settings_resolvers_settings__WEBPACK_IMPORTED_MODULE_4__["SettingsEditResolver"], {
        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
        useClass: _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_10__["JwtInterceptor"],
        multi: true
      }]
    })], SettingsModule);
    /***/
  }
}]);
//# sourceMappingURL=app-views-pages-settings-settings-module-es5.js.map