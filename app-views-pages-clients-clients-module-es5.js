function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-clients-clients-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-balance/clients-balance.component.html":
  /*!**************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-balance/clients-balance.component.html ***!
    \**************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPagesClientsClientsBalanceClientsBalanceComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<h3>\r\n\tТекущий баланс : {{getTotal()}}\r\n</h3>\r\n<a href=\"javascript:;\" class=\"btn btn-success kt-margin-r-10\" (click)=\"openDialog('1')\" mat-raised-button matTooltip=\"Пополнить баланс\">\r\n\t<i class=\"la la-save\"></i>\r\n\t<span class=\"kt-hidden-mobile\">Пополнить</span>\r\n</a>\r\n<a href=\"javascript:;\" class=\"btn btn-warning kt-margin-r-10\" (click)=\"openDialog('0')\" mat-raised-button matTooltip=\"Списать с баланса\">\r\n\t<i class=\"la la-save\"></i>\r\n\t<span class=\"kt-hidden-mobile\">Списать</span>\r\n</a>\r\n<div class=\"kt-portlet__body kt-portlet__body--fit\">\r\n\t<div class=\"my_datatable\" id=\"client_balance\"></div>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.html":
  /*!**************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.html ***!
    \**************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPagesClientsClientsBalanceModalClientBalanceDialogComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<h1 mat-dialog-title>{{getTitle()}}</h1>\r\n<div mat-dialog-content>\r\n\t<p>Сумма</p>\r\n\t<mat-form-field>\r\n\t\t<input matInput [(ngModel)]=\"form.value\">\r\n\t</mat-form-field>\r\n\t<p>Комментарий</p>\r\n\t<mat-form-field>\r\n\t\t<input matInput [(ngModel)]=\"form.comment\">\r\n\t</mat-form-field>\r\n</div>\r\n<div *ngIf=\"errors\">\r\n\t<!--<div *ngFor=\"let error of errors\">-->\r\n\t\t{{errors[0]}}\r\n\t<!--</div>-->\r\n</div>\r\n<div mat-dialog-actions>\r\n\t<button mat-button (click)=\"closeDialog()\">Отмена</button>\r\n\t<button mat-button (click)=\"submitDialog()\" cdkFocusInitial>Ок</button>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-edit/clients-edit.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-edit/clients-edit.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPagesClientsClientsEditClientsEditComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"kt-portlet\">\r\n\t<kt-portlet-header [title]=\"getComponentTitle()\" [class]=\"'kt-portlet__head--lg'\" [viewLoading$]=\"loading$\">\r\n\t\t<ng-container ktPortletTools>\r\n\t\t\t<a [routerLink]=\"['../../']\" class=\"btn btn-secondary kt-margin-r-10\" mat-raised-button matTooltip=\"Назад к списку\">\r\n\t\t\t\t<i class=\"la la-arrow-left\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Назад</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-primary kt-margin-r-10\" color=\"primary\" (click)=\"onSubmit(true)\" mat-raised-button matTooltip=\"Сохранить и продолжить\">\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сохранить</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-secondary kt-margin-r-10\" (click)=\"reset()\" mat-raised-button matTooltip=\"Отменить изменения\">\r\n\t\t\t\t<i class=\"la la-cog\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сбросить</span>\r\n\t\t\t</a>\r\n\t\t</ng-container>\r\n\t</kt-portlet-header>\r\n\t<br>\r\n\t<!--begin::Form-->\r\n\t<kt-portlet-body>\r\n\r\n\t\t<mat-tab-group [(selectedIndex)]=\"selectedTab\">\r\n\t\t\t<mat-tab>\r\n\t\t\t\t<ng-template mat-tab-label>\r\n\t\t\t\t\t<i class=\"mat-tab-label-icon fa fa-user\"></i>\r\n\t\t\t\t\tБазовая информация\r\n\t\t\t\t</ng-template>\r\n\t\t\t\t<ng-template matTabContent>\r\n\t\t\t\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\t\t\t\t\t\t<kt-alert *ngIf=\"hasFormErrors\" type=\"warn\" [showCloseButton]=\"true\" [duration]=\"10000\" (close)=\"onAlertClose($event)\">\r\n\t\t\t\t\t\t\tОшибка! Форма не прошла валидацию.\r\n\t\t\t\t\t\t</kt-alert>\r\n\t\t\t\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t\t\t\t<ng-container [ngTemplateOutlet]=\"formTemplate\"></ng-container>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ng-template>\r\n\t\t\t</mat-tab>\r\n\t\t\t<mat-tab [disabled]=\"!modelForm.controls['id'].value\">\r\n\t\t\t\t<ng-template mat-tab-label>\r\n\t\t\t\t\t<i class=\"mat-tab-label-icon fa fa-user\"></i>\r\n\t\t\t\t\tБаланс клиента\r\n\t\t\t\t</ng-template>\r\n\t\t\t\t<ng-template matTabContent>\r\n\t\t\t\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\t\t\t\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t\t\t\t<kt-clients-balance [data]=\"balanceData\"></kt-clients-balance>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</ng-template>\r\n\t\t\t</mat-tab>\r\n\t\t</mat-tab-group>\r\n\t</kt-portlet-body>\r\n</div>\r\n\r\n<ng-template #formTemplate>\r\n\t<form class=\"kt-form kt-form--label-right\" [formGroup]=\"modelForm\" >\r\n\t\t<ng-container *ngFor=\"let control of formConfig(); trackBy:trackByFn\">\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'horizontal' && isFieldVisible(control.permission)\">\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'text' || control['type'] == 'password'  || control['type'] == 'email'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<input [type]=\"control['type']\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\">\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div *ngIf=\"control['type'] == 'select'\" class=\"form-group row validated\">\r\n\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t<select  formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\" class=\"form-control\">\r\n\t\t\t\t\t\t\t<option [value]=\"null\">-- Выберите --</option>\r\n\t\t\t\t\t\t\t<option *ngFor=\"let option of control.options\" [value]=\"option.value\">{{option.label}}</option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage('role')}}</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngIf=\"config.orientation == 'vertical'\"></ng-container>\r\n\t\t</ng-container>\r\n\t</form>\r\n</ng-template>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-list/clients-list.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-list/clients-list.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPagesClientsClientsListClientsListComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<kt-portlet>\r\n\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n\t\t<div class=\"kt-portlet__head-label\">\r\n\t\t\t<h3 class=\"kt-portlet__head-title\">\r\n\t\t\t\t{{title}}\r\n\t\t\t</h3>\r\n\t\t</div>\r\n\t\t<div class=\"kt-portlet__head-toolbar\">\r\n\t\t\t<div class=\"kt-portlet__head-wrapper\">\r\n\t\t\t\t<div class=\"dropdown dropdown-inline\">\r\n\t\t\t\t\t<button  (click)=\"redirectCreateModel();\" mat-raised-button>Добавить</button>&nbsp;\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body\">\r\n\t\t<div class=\"kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10\">\r\n\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-xl-8 order-2 order-xl-1\">\r\n\t\t\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t\t\t<div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--left\">\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Поиск...\" id=\"generalSearch\">\r\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--left\">\r\n\t\t\t\t\t\t\t<span><i class=\"la la-search\"></i></span>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body kt-portlet__body--fit\">\r\n\t\t<div class=\"my_datatable\" [id]=\"baseUrl\"></div>\r\n\t</div>\r\n</kt-portlet>\r\n";
    /***/
  },

  /***/
  "./src/app/core/client/_resolvers/clients.ts":
  /*!***************************************************!*\
    !*** ./src/app/core/client/_resolvers/clients.ts ***!
    \***************************************************/

  /*! exports provided: ClientEditResolver */

  /***/
  function srcAppCoreClient_resolversClientsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientEditResolver", function () {
      return ClientEditResolver;
    });
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };
    /**
     * Created by Алексей on 09.02.2018.
     */


    var ClientEditResolver = /*#__PURE__*/function () {
      function ClientEditResolver(http, activatedRoute) {
        _classCallCheck(this, ClientEditResolver);

        this.http = http;
        this.activatedRoute = activatedRoute;
      }

      _createClass(ClientEditResolver, [{
        key: "resolve",
        value: function resolve(route, state) {
          if (route.queryParams.id) {
            return this.http.get('/api/client/' + route.queryParams.id);
          } else return;
        }
      }]);

      return ClientEditResolver;
    }();

    ClientEditResolver.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]
      }];
    };

    ClientEditResolver = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])], ClientEditResolver);
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-balance/clients-balance.component.scss":
  /*!************************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-balance/clients-balance.component.scss ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPagesClientsClientsBalanceClientsBalanceComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2NsaWVudHMvY2xpZW50cy1iYWxhbmNlL2NsaWVudHMtYmFsYW5jZS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-balance/clients-balance.component.ts":
  /*!**********************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-balance/clients-balance.component.ts ***!
    \**********************************************************************************/

  /*! exports provided: ClientsBalanceComponent */

  /***/
  function srcAppViewsPagesClientsClientsBalanceClientsBalanceComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientsBalanceComponent", function () {
      return ClientsBalanceComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _core_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../core/auth */
    "./src/app/core/auth/index.ts");
    /* harmony import */


    var _core_auth_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../core/auth/_services */
    "./src/app/core/auth/_services/index.ts");
    /* harmony import */


    var _modal_client_balance_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./modal/client-balance-dialog.component */
    "./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ClientsBalanceComponent = /*#__PURE__*/function () {
      function ClientsBalanceComponent(auth, http, user, cdr, dialog) {
        _classCallCheck(this, ClientsBalanceComponent);

        this.auth = auth;
        this.http = http;
        this.user = user;
        this.cdr = cdr;
        this.dialog = dialog;
        this.total = '0';
        this.baseUrl = 'client_balance';
      }

      _createClass(ClientsBalanceComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initDatatable();
        }
      }, {
        key: "getTotal",
        value: function getTotal() {
          return this.data.total + ' руб.';
        }
      }, {
        key: "openDialog",
        value: function openDialog(type) {
          var _this = this;

          var dialogRef = this.dialog.open(_modal_client_balance_dialog_component__WEBPACK_IMPORTED_MODULE_5__["ClientBalanceDialogComponent"], {
            width: '350px',
            data: {
              type: type,
              client_id: this.data.id
            }
          });
          dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
              _this.setBalance();

              _this.datatable.reload();
            }
          });
        }
      }, {
        key: "setBalance",
        value: function setBalance() {
          var _this2 = this;

          this.http.get('api/client/balance/' + this.data.id).subscribe(function (data) {
            _this2.data.total = data['total'];

            _this2.cdr.detectChanges();
          });
        }
      }, {
        key: "initDatatable",
        value: function initDatatable() {
          var token = this.auth.getUserToken();
          this.datatable = $('#' + this.baseUrl).KTDatatable({
            data: {
              // autoColumns: true,
              type: 'remote',
              source: {
                read: {
                  method: 'GET',
                  url: 'api/' + this.baseUrl + 's/' + this.data.id,
                  headers: {
                    'Authorization': token
                  },
                  map: function map(raw) {
                    // sample data mapping
                    var dataSet = raw;
                    console.log(raw);

                    if (typeof raw.models !== 'undefined') {
                      dataSet = raw.models;
                    }

                    return dataSet;
                  }
                }
              },
              pageSize: 10,
              serverPaging: true,
              serverFiltering: true,
              serverSorting: true
            },
            // layout definition
            layout: {
              scroll: false,
              footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            search: {
              input: $('#generalSearch')
            },
            // columns definition
            columns: this.getColumnsByRole()
          });
        }
      }, {
        key: "getColumnsByRole",
        value: function getColumnsByRole() {
          var config = [];

          if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
            config = [{
              field: 'id',
              title: '#',
              sortable: 'asc',
              width: 40,
              type: 'number',
              selector: false,
              textAlign: 'center',
              autoHide: !1
            }, {
              field: 'type',
              title: 'Тип',
              autoHide: !1,
              width: 100,
              textAlign: 'center',
              template: function template(row, index, datatable) {
                if (row['type'] == 1) {
                  return 'Пополнение';
                } else if (row['type'] == 0) {
                  return 'Списание';
                } else {
                  return 'Неизвестный статус';
                }
              }
            }, {
              field: 'value',
              title: 'Сумма',
              autoHide: !1,
              width: 100,
              textAlign: 'center',
              template: function template(row, index, datatable) {
                return row['value'] + ' руб.';
              }
            }, {
              field: 'created_at',
              title: 'Дата',
              autoHide: !1,
              width: 100,
              textAlign: 'center',
              template: function template(row, index, datatable) {
                return moment.utc(row['created_at']).local().format('DD.MM.YYYY HH:mm');
              }
            }, {
              field: 'comment',
              title: 'Комментарий',
              autoHide: !1,
              width: 100,
              textAlign: 'center'
            }];
          }

          return config;
        }
      }]);

      return ClientsBalanceComponent;
    }();

    ClientsBalanceComponent.ctorParameters = function () {
      return [{
        type: _core_auth_services__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _core_auth__WEBPACK_IMPORTED_MODULE_3__["User"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Object)], ClientsBalanceComponent.prototype, "data", void 0);

    ClientsBalanceComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'kt-clients-balance',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./clients-balance.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-balance/clients-balance.component.html"))["default"],
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
      styles: [__importDefault(__webpack_require__(
      /*! ./clients-balance.component.scss */
      "./src/app/views/pages/clients/clients-balance/clients-balance.component.scss"))["default"]]
    }), __metadata("design:paramtypes", [_core_auth_services__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _core_auth__WEBPACK_IMPORTED_MODULE_3__["User"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])], ClientsBalanceComponent);
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.ts":
  /*!**********************************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.ts ***!
    \**********************************************************************************************/

  /*! exports provided: ClientBalanceDialogComponent */

  /***/
  function srcAppViewsPagesClientsClientsBalanceModalClientBalanceDialogComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientBalanceDialogComponent", function () {
      return ClientBalanceDialogComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/esm2015/dialog.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ClientBalanceDialogComponent = /*#__PURE__*/function () {
      function ClientBalanceDialogComponent(dialogRef, data, cdr, htpp) {
        _classCallCheck(this, ClientBalanceDialogComponent);

        this.dialogRef = dialogRef;
        this.data = data;
        this.cdr = cdr;
        this.htpp = htpp;
        this.form = {
          value: '0',
          comment: '',
          client_id: '',
          type: ''
        };
      }

      _createClass(ClientBalanceDialogComponent, [{
        key: "getTitle",
        value: function getTitle() {
          if (this.data.type == '0') {
            return "Списывание баланса";
          } else if (this.data.type == '1') {
            return "Пополнение баланса";
          } else {
            return "Неихвестно";
          }
        }
      }, {
        key: "submitDialog",
        value: function submitDialog() {
          var _this3 = this;

          this.form.client_id = this.data.client_id;
          this.form.type = this.data.type;
          this.htpp.post('api/client_balance', this.form).subscribe(function () {
            _this3.dialogRef.close(true);
          }, function (error) {
            _this3.cdr.detectChanges();

            _this3.errors = error['error'];
          });
        }
      }, {
        key: "closeDialog",
        value: function closeDialog() {
          this.dialogRef.close(false);
        }
      }]);

      return ClientBalanceDialogComponent;
    }();

    ClientBalanceDialogComponent.ctorParameters = function () {
      return [{
        type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]]
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    ClientBalanceDialogComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'kt-client-balance-dialog',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./client-balance-dialog.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.html"))["default"],
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush
    }), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])), __metadata("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])], ClientBalanceDialogComponent);
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-edit/clients-edit.component.scss":
  /*!******************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-edit/clients-edit.component.scss ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPagesClientsClientsEditClientsEditComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2NsaWVudHMvY2xpZW50cy1lZGl0L2NsaWVudHMtZWRpdC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-edit/clients-edit.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-edit/clients-edit.component.ts ***!
    \****************************************************************************/

  /*! exports provided: ClientsEditComponent */

  /***/
  function srcAppViewsPagesClientsClientsEditClientsEditComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientsEditComponent", function () {
      return ClientsEditComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _core_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../core/auth */
    "./src/app/core/auth/index.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../core/_base/layout */
    "./src/app/core/_base/layout/index.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ClientsEditComponent = /*#__PURE__*/function () {
      function ClientsEditComponent(subheader, userModel, router, titleService, http, Fb, cdr, route) {
        _classCallCheck(this, ClientsEditComponent);

        this.subheader = subheader;
        this.userModel = userModel;
        this.router = router;
        this.titleService = titleService;
        this.http = http;
        this.Fb = Fb;
        this.cdr = cdr;
        this.route = route;
        this.config = {
          orientation: 'horizontal'
        };
        this.title = "";
        this.selectedTab = 0;
        this.baseUrl = 'client';
        this.hasFormErrors = false;
        this.model = {};
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_2__["User"]();
        this.roles = {};
        this.errors = [];
        this.submitted = false;
      }

      _createClass(ClientsEditComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.createForm();
          this.initFormValues();
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          this.setInputMasks();
        }
      }, {
        key: "setInputMasks",
        value: function setInputMasks() {
          var _this4 = this;

          $('#phone').inputmask('mask', {
            'mask': '+7 (999) 999-9999',
            oncomplete: function oncomplete(e) {
              console.log(e);

              _this4.modelForm.controls['phone'].setValue(e.target.value);
            }
          });
          $("#phone").change(function (e) {
            console.log(e);

            _this4.modelForm.controls['phone'].setValue(e.target.value);
          });
        }
      }, {
        key: "setTitles",
        value: function setTitles() {
          var breadcrumbs = [{
            title: 'Список клиентов',
            page: '/users'
          }];
          this.subheader.setTitle("Клиенты");

          if (this.state == 'edit') {
            this.title = "Редактирование клиента";
            breadcrumbs.push({
              title: 'Редактирование',
              page: '/clients/edit?id=' + this.modelForm.controls['id'].value
            });
          } else {
            this.title = "Добавление клиента";
            breadcrumbs.push({
              title: 'Добавление',
              page: '/clients/create'
            });
          }

          this.subheader.setBreadcrumbs(breadcrumbs);
          this.titleService.setTitle(this.title);
        }
      }, {
        key: "getComponentTitle",
        value: function getComponentTitle() {
          return this.title;
        }
      }, {
        key: "reset",
        value: function reset() {
          this.modelForm.reset();
        }
      }, {
        key: "onSubmit",
        value: function onSubmit(redirect) {
          var _this5 = this;

          if (this.state == 'edit') {
            this.http.put('api/client', this.modelForm.value).subscribe(function (data) {
              if (redirect) _this5.router.navigate(['/clients']);
              _this5.hasFormErrors = false;
            }, function (data) {
              _this5.errors = data.error;
              _this5.hasFormErrors = true;

              _this5.cdr.detectChanges();
            });
          } else {
            this.http.post('api/client', this.modelForm.value).subscribe(function (data) {
              if (redirect) _this5.router.navigate(['/clients']);
              _this5.hasFormErrors = false;
            }, function (data) {
              _this5.errors = data.error;
              _this5.hasFormErrors = true;

              _this5.cdr.detectChanges();
            });
          }
        }
      }, {
        key: "initFormValues",
        value: function initFormValues() {
          var _this6 = this;

          this.route.queryParams.subscribe(function (params) {
            var id = params['id'];

            if (id > 0) {
              _this6.state = 'edit';

              _this6.route.data.subscribe(function (data) {
                if (data['model']) {
                  _this6.balanceData = {
                    id: data['model'].id,
                    total: data['model'].total
                  };

                  _this6.modelForm.patchValue(data['model'], {
                    emitEvent: false,
                    onlySelf: true
                  });
                }
              });
            } else {
              _this6.state = 'create';
            }

            _this6.setTitles();
          });
        }
      }, {
        key: "createForm",
        value: function createForm() {
          this.modelForm = this.Fb.group({
            id: '',
            first_name: [''],
            last_name: [''],
            phone: [''],
            password: [''],
            blocked: [0]
          });
        }
      }, {
        key: "formConfig",
        value: function formConfig() {
          return [{
            field: 'first_name',
            label: 'Имя',
            type: 'text',
            permission: 'manage_' + this.baseUrl + 's'
          }, {
            field: 'last_name',
            label: 'Фамилия',
            type: 'text',
            permission: 'manage_' + this.baseUrl + 's'
          }, {
            field: 'phone',
            label: 'Телефон',
            type: 'text',
            permission: 'manage_' + this.baseUrl + 's'
          }, {
            field: 'password',
            label: 'Пароль',
            type: 'password',
            permission: 'manage_' + this.baseUrl + 's'
          }, {
            field: 'blocked',
            label: 'Блокировка',
            type: 'select',
            permission: 'manage_' + this.baseUrl + 's',
            options: [{
              value: 0,
              label: 'Нет'
            }, {
              value: 1,
              label: 'Да'
            }],
            nullValue: false
          }];
        }
      }, {
        key: "isFieldVisible",
        value: function isFieldVisible(permission) {
          return this.userModel.hasPermissionTo(permission);
        }
      }, {
        key: "trackByFn",
        value: function trackByFn(index, item) {
          return index;
        }
      }, {
        key: "getErrorMessage",
        value: function getErrorMessage(field) {
          if (this.errors[field]) {
            return this.errors[field][0];
          } else {
            return '';
          }
        }
      }, {
        key: "hasError",
        value: function hasError(field) {
          var classList = {
            'is-invalid': this.errors[field],
            'form-control': true
          };
          return classList;
        }
      }]);

      return ClientsEditComponent;
    }();

    ClientsEditComponent.ctorParameters = function () {
      return [{
        type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"]
      }, {
        type: _core_auth__WEBPACK_IMPORTED_MODULE_2__["User"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
      }];
    };

    ClientsEditComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'kt-clients-edit',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./clients-edit.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-edit/clients-edit.component.html"))["default"],
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
      styles: [__importDefault(__webpack_require__(
      /*! ./clients-edit.component.scss */
      "./src/app/views/pages/clients/clients-edit/clients-edit.component.scss"))["default"]]
    }), __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"], _core_auth__WEBPACK_IMPORTED_MODULE_2__["User"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])], ClientsEditComponent);
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-list/clients-list.component.scss":
  /*!******************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-list/clients-list.component.scss ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPagesClientsClientsListClientsListComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2NsaWVudHMvY2xpZW50cy1saXN0L2NsaWVudHMtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients-list/clients-list.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/views/pages/clients/clients-list/clients-list.component.ts ***!
    \****************************************************************************/

  /*! exports provided: ClientsListComponent */

  /***/
  function srcAppViewsPagesClientsClientsListClientsListComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientsListComponent", function () {
      return ClientsListComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ngrx/store */
    "./node_modules/@ngrx/store/fesm2015/store.js");
    /* harmony import */


    var _core_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../core/auth */
    "./src/app/core/auth/index.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../core/_base/layout */
    "./src/app/core/_base/layout/index.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _core_auth_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../../core/auth/_services */
    "./src/app/core/auth/_services/index.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ClientsListComponent = /*#__PURE__*/function () {
      function ClientsListComponent(subheader, cdr, auth, router, titleService, store, http) {
        _classCallCheck(this, ClientsListComponent);

        this.subheader = subheader;
        this.cdr = cdr;
        this.auth = auth;
        this.router = router;
        this.titleService = titleService;
        this.store = store;
        this.http = http;
        this.title = 'Клиенты';
        this.baseUrl = 'client';
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_2__["User"]();
      }

      _createClass(ClientsListComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subheader.setBreadcrumbs([{
            title: 'Список клиентов',
            url: 'users'
          }]);
          this.subheader.setTitle('Клиенты');
          this.cdr.detectChanges();
          this.titleService.setTitle('Клиенты');
          this.initDatatable();
        }
      }, {
        key: "redirectCreateModel",
        value: function redirectCreateModel() {
          this.router.navigate(['/clients/edit']);
        }
      }, {
        key: "initDatatable",
        value: function initDatatable() {
          var _this7 = this;

          var token = this.auth.getUserToken();
          this.datatable = $('#' + this.baseUrl).KTDatatable({
            data: {
              type: 'remote',
              source: {
                read: {
                  method: 'GET',
                  url: 'api/' + this.baseUrl + 's',
                  headers: {
                    'Authorization': token
                  },
                  map: function map(raw) {
                    // sample data mapping
                    var dataSet = raw;
                    console.log(raw);

                    if (typeof raw.models !== 'undefined') {
                      dataSet = raw.models;
                    }

                    return dataSet;
                  }
                }
              },
              pageSize: 10,
              serverPaging: true,
              serverFiltering: true,
              serverSorting: true
            },
            // layout definition
            layout: {
              scroll: false,
              footer: false
            },
            // column sorting
            sortable: true,
            pagination: true,
            search: {
              input: $('#generalSearch')
            },
            // columns definition
            columns: this.getColumnsByRole()
          });
          this.datatable.on('kt-datatable--on-layout-updated', function () {
            setTimeout(function () {
              $('.editModel').click(function (e) {
                var id = $(e.currentTarget).attr('data-id');

                _this7.edit(id);
              });
              $('.deleteModel').click(function (e) {
                var id = $(e.currentTarget).attr('data-id');

                if (confirm('Вы уверены что хотите удалть?')) {
                  _this7["delete"](id);
                }
              });
              $('.toggleStatus').click(function (e) {
                var id = $(e.currentTarget).attr('data-id');

                _this7.toggleStatus(id);
              });
            });
          });
          this.datatable.on('kt-datatable--on-ajax-fail', function ($param) {
            _this7.router.navigate(['/auth/login']);
          });
        }
      }, {
        key: "toggleStatus",
        value: function toggleStatus(id) {
          var _this8 = this;

          this.http.post('/api/' + this.baseUrl + '/status/' + id, {}).subscribe(function () {
            _this8.datatable.reload();
          });
        }
      }, {
        key: "edit",
        value: function edit(id) {
          this.router.navigate(['/' + this.baseUrl + 's/edit'], {
            queryParams: {
              id: id
            }
          });
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          var _this9 = this;

          this.http["delete"]('/api/' + this.baseUrl + '/' + id).subscribe(function () {
            _this9.datatable.reload();
          });
        }
      }, {
        key: "getColumnsByRole",
        value: function getColumnsByRole() {
          var _this10 = this;

          var config = [];

          if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
            config = [{
              field: 'id',
              title: '#',
              sortable: 'asc',
              width: 40,
              type: 'number',
              selector: false,
              textAlign: 'center',
              autoHide: !1
            }, {
              field: 'first_name',
              title: 'Имя, фамилия',
              autoHide: !1,
              template: function template(row, index, datatable) {
                return row['first_name'] + ' ' + row['last_name'];
              }
            }, {
              field: 'phone',
              title: 'Тел.номер',
              autoHide: !1
            }];
          }

          if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
            config.push({
              field: 'blocked',
              title: 'Статус',
              width: 140,
              template: function template(row, index, datatable) {
                var html = '';

                if (row['blocked'] === 1) {
                  html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-danger btn-sm btn-pill">Заблокирован</button>';
                } else {
                  html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-success btn-sm btn-pill">Активен</button>';
                }

                return html;
              }
            });
          }

          config.push({
            field: 'Actions',
            title: 'Действия',
            sortable: false,
            overflow: 'visible',
            textAlign: 'center',
            autoHide: !1,
            template: function template(row, index, datatable) {
              var visible = _this10.user.hasPermissionTo('manage_' + _this10.baseUrl + 's');

              var dropup = datatable.getPageSize() - index <= 4 ? 'dropup' : '';
              var result = '<button href="javascript:;" data-id=' + row['id'] + ' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
                        <i class="la la-edit"></i>\
                    </button>';

              if (visible) {
                result += '<button href="javascript:;" data-id=' + row['id'] + ' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
                        <i class="la la-trash"></i>\
                    </button>';
              }

              return result;
            }
          });
          return config;
        }
      }]);

      return ClientsListComponent;
    }();

    ClientsListComponent.ctorParameters = function () {
      return [{
        type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: _core_auth_services__WEBPACK_IMPORTED_MODULE_7__["AuthService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"]
      }, {
        type: _ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }];
    };

    ClientsListComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'kt-clients-list',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./clients-list.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/clients/clients-list/clients-list.component.html"))["default"],
      styles: [__importDefault(__webpack_require__(
      /*! ./clients-list.component.scss */
      "./src/app/views/pages/clients/clients-list/clients-list.component.scss"))["default"]]
    }), __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], _core_auth_services__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["Title"], _ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])], ClientsListComponent);
    /***/
  },

  /***/
  "./src/app/views/pages/clients/clients.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/views/pages/clients/clients.module.ts ***!
    \*******************************************************/

  /*! exports provided: ClientsModule */

  /***/
  function srcAppViewsPagesClientsClientsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientsModule", function () {
      return ClientsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _clients_list_clients_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./clients-list/clients-list.component */
    "./src/app/views/pages/clients/clients-list/clients-list.component.ts");
    /* harmony import */


    var _clients_edit_clients_edit_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./clients-edit/clients-edit.component */
    "./src/app/views/pages/clients/clients-edit/clients-edit.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _partials_partials_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../partials/partials.module */
    "./src/app/views/partials/partials.module.ts");
    /* harmony import */


    var _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../core/auth/_interceptor/interceptor */
    "./src/app/core/auth/_interceptor/interceptor.ts");
    /* harmony import */


    var _core_client_resolvers_clients__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../core/client/_resolvers/clients */
    "./src/app/core/client/_resolvers/clients.ts");
    /* harmony import */


    var _clients_balance_clients_balance_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./clients-balance/clients-balance.component */
    "./src/app/views/pages/clients/clients-balance/clients-balance.component.ts");
    /* harmony import */


    var _clients_balance_modal_client_balance_dialog_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./clients-balance/modal/client-balance-dialog.component */
    "./src/app/views/pages/clients/clients-balance/modal/client-balance-dialog.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var routes = [// { path: '', redirectTo: 'list'},
    {
      path: '',
      component: _clients_list_clients_list_component__WEBPACK_IMPORTED_MODULE_2__["ClientsListComponent"]
    }, {
      path: 'edit',
      component: _clients_edit_clients_edit_component__WEBPACK_IMPORTED_MODULE_3__["ClientsEditComponent"],
      resolve: {
        model: _core_client_resolvers_clients__WEBPACK_IMPORTED_MODULE_11__["ClientEditResolver"]
      }
    }];

    var ClientsModule = function ClientsModule() {
      _classCallCheck(this, ClientsModule);
    };

    ClientsModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      declarations: [_clients_list_clients_list_component__WEBPACK_IMPORTED_MODULE_2__["ClientsListComponent"], _clients_edit_clients_edit_component__WEBPACK_IMPORTED_MODULE_3__["ClientsEditComponent"], _clients_balance_clients_balance_component__WEBPACK_IMPORTED_MODULE_12__["ClientsBalanceComponent"], _clients_balance_modal_client_balance_dialog_component__WEBPACK_IMPORTED_MODULE_13__["ClientBalanceDialogComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], _partials_partials_module__WEBPACK_IMPORTED_MODULE_9__["PartialsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateModule"].forChild(), _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressSpinnerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTabsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"]],
      entryComponents: [_clients_balance_modal_client_balance_dialog_component__WEBPACK_IMPORTED_MODULE_13__["ClientBalanceDialogComponent"]],
      providers: [_core_client_resolvers_clients__WEBPACK_IMPORTED_MODULE_11__["ClientEditResolver"], {
        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
        useClass: _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_10__["JwtInterceptor"],
        multi: true
      }]
    })], ClientsModule);
    /***/
  }
}]);
//# sourceMappingURL=app-views-pages-clients-clients-module-es5.js.map