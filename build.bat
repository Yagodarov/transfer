@echo off
echo.
del *.txt
del *.woff
del *.woff2
del *.eot
del *.ttf
del *.svg
del *.css
del *.js
del *.png
del *.html
del assets /s /q
call ng build --prod --source-map=false
ROBOCOPY dist/metronic/ . /s /e /it
rmdir /s /q dist

GOTO DONE

:UNKNOWN
Echo Error no commit comment
goto:eof

:DONE
ECHO Done!
goto:eof
