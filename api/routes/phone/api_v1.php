<?php
	/**
	 * Created by PhpStorm.
	 * User: Acer
	 * Date: 19.04.2018
	 * Time: 0:34
	 */
	Route::group(['middleware' => ['uid']
	], function () {
		Route::post('client/order', 'ApiControllers\AndroidOrderController@store')->middleware();
		Route::post('client/order_confirm', 'ApiControllers\AndroidOrderController@update')->middleware();
		Route::get('client/orders', 'ApiControllers\AndroidOrderController@index')->middleware();
	});
	
	
	Route::group(['middleware' => []
	], function () {
		Route::post('driver/authenticate', 'ApiControllers\Auth\AndroidLoginController@authenticateDriver')->middleware();
	});
	
	Route::group(['middleware' => ['auth:driver', 'jwt.auth']
	], function () {
		Route::post('driver/update', 'ApiControllers\Auth\LoginController@updateDriver')->middleware();
		
		Route::get('driver/orders', 'ApiControllers\AndroidOrderController@driverOrders')->middleware();
		Route::post('driver/order_bid', 'OrderBidController@store');
		Route::post('driver/order_bid_change', 'OrderBidController@update');
		Route::post('driver/order_bid/status/{id}', 'OrderBidController@toggleStatus');
		Route::get('driver/order_bids', 'OrderBidController@index');
	});
