<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authenticate', 'Auth\LoginController@authenticate');

Route::group(['middleware' => ['jwt.auth']],
	function () {
	Route::get('authenticate', 'Auth\LoginController@getUser');
    Route::get('users-list', 'UserController@index')->middleware(['permission:manage_users']);
    Route::get('users', 'UserController@index')->middleware(['permission:manage_users']);
    Route::post('user', 'UserController@store')->middleware(['permission:manage_users']);
    Route::post('user/status/{id}', 'UserController@toggleStatus')->middleware(['permission:manage_users']);
    Route::get('user/{id}', 'UserController@get')->middleware(['permission:manage_users']);
    Route::put('user', 'UserController@update')->middleware(['permission:manage_users']);
    Route::put('user/block/{id}', 'UserController@block')->middleware(['permission:block_user']);
    Route::delete('user/{id}', 'UserController@delete')->middleware(['permission:delete_user']);

    Route::get('towns', 'TownController@index')->middleware(['permission:manage_towns']);
    Route::post('town', 'TownController@store')->middleware(['permission:manage_towns']);
    Route::post('town/status/{id}', 'TownController@toggleStatus')->middleware(['permission:manage_towns']);
    Route::get('town/{id}', 'TownController@get')->middleware(['permission:manage_towns']);
    Route::put('town', 'TownController@update')->middleware(['permission:manage_towns']);
    Route::delete('town/{id}', 'TownController@delete')->middleware(['permission:manage_towns']);

    Route::get('services', 'ServiceController@index')->middleware(['permission:manage_services']);
    Route::post('service', 'ServiceController@store')->middleware(['permission:manage_services']);
    Route::post('service/status/{id}', 'ServiceController@toggleStatus')->middleware(['permission:manage_services']);
    Route::get('service/{id}', 'ServiceController@get')->middleware(['permission:manage_services']);
    Route::put('service', 'ServiceController@update')->middleware(['permission:manage_services']);
    Route::delete('service/{id}', 'ServiceController@delete')->middleware(['permission:manage_services']);

//    Route::get('towns', 'TownController@index')->middleware(['permission:manage_towns']);
//    Route::post('town', 'TownController@store')->middleware(['permission:manage_towns']);
//    Route::post('town/status/{id}', 'TownController@toggleStatus')->middleware(['permission:manage_towns']);
//    Route::get('town/{id}', 'TownController@get')->middleware(['permission:manage_towns']);
//    Route::put('town', 'TownController@update')->middleware(['permission:manage_towns']);
//    Route::delete('town/{id}', 'TownController@delete')->middleware(['permission:manage_towns']);

    Route::get('drivers', 'DriverController@index')->middleware(['permission:manage_drivers']);
    Route::post('driver', 'DriverController@store')->middleware(['permission:manage_drivers']);
    Route::post('driver/status/{id}', 'DriverController@toggleStatus')->middleware(['permission:manage_drivers']);
    Route::get('driver/{id}', 'DriverController@get')->middleware(['permission:manage_drivers']);
    Route::get('driver/balance/{id}', 'DriverController@getBalance')->middleware(['permission:manage_drivers']);
    Route::put('driver', 'DriverController@update')->middleware(['permission:manage_drivers']);
    Route::delete('driver/{id}', 'DriverController@delete')->middleware(['permission:manage_drivers']);


//	Route::get('drivers', 'DriverController@index')->middleware(['permission:manage_drivers']);
	Route::post('driver_balance', 'DriverBalanceController@store')->middleware(['permission:manage_drivers']);
//	Route::post('driver-balance/status/{id}', 'DriverController@toggleStatus')->middleware(['permission:manage_drivers']);
	Route::get('driver_balances/{id}', 'DriverBalanceController@index')->middleware(['permission:manage_drivers']);
	Route::get('driver_balance/{id}', 'DriverBalanceController@get')->middleware(['permission:manage_drivers']);

	Route::post('client_balance', 'ClientPaymentController@store')->middleware(['permission:manage_clients']);
//	Route::post('driver-balance/status/{id}', 'DriverController@toggleStatus')->middleware(['permission:manage_drivers']);
	Route::get('client_balances/{id}', 'ClientPaymentController@index')->middleware(['permission:manage_clients']);
	Route::get('client_balance/{id}', 'ClientPaymentController@get')->middleware(['permission:manage_clients']);
//	Route::put('driver_balance', 'DriverBalanceController@update')->middleware(['permission:manage_drivers']);
//	Route::delete('driver_balance/{id}', 'DriverBalanceController@delete')->middleware(['permission:manage_drivers']);

    Route::get('cars', 'CarController@index')->middleware(['permission:manage_cars']);
    Route::post('car', 'CarController@store')->middleware(['permission:manage_cars']);
    Route::post('car/status/{id}', 'CarController@toggleStatus')->middleware(['permission:manage_cars']);
    Route::get('car/{id}', 'CarController@get')->middleware(['permission:manage_cars']);
    Route::put('car', 'CarController@update')->middleware(['permission:manage_cars']);
    Route::delete('car/{id}', 'CarController@delete')->middleware(['permission:manage_cars']);

    Route::get('clients', 'ClientController@index')->middleware(['permission:manage_clients']);
    Route::post('client', 'ClientController@store')->middleware(['permission:manage_clients']);
    Route::post('client/status/{id}', 'ClientController@toggleStatus')->middleware(['permission:manage_clients']);
    Route::get('client/{id}', 'ClientController@get')->middleware(['permission:manage_clients']);
    Route::put('client', 'ClientController@update')->middleware(['permission:manage_clients']);
    Route::delete('client/{id}', 'ClientController@delete')->middleware(['permission:manage_clients']);

    Route::get('orders', 'OrderController@index')->middleware(['permission:manage_orders']);
    Route::post('order', 'OrderController@store')->middleware(['permission:manage_orders']);
    Route::get('order/{id}', 'OrderController@get')->middleware(['permission:manage_orders']);
    Route::put('order', 'OrderController@update')->middleware(['permission:manage_orders']);
    Route::delete('order/{id}', 'OrderController@delete')->middleware(['permission:manage_orders']);

	Route::get('setting', 'SettingController@get')->middleware(['permission:manage_settings']);
	Route::post('setting', 'SettingController@store')->middleware(['permission:manage_settings']);
	Route::put('setting', 'SettingController@store')->middleware(['permission:manage_settings']);
});

Route::group([
	'prefix' => 'v1',
], function () {
	require base_path('routes/phone/api_v1.php');
});
