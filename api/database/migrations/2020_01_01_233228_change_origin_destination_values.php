<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOriginDestinationValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_destinations', function (Blueprint $table) {
        	$table->renameColumn('address', 'value');
        });
        Schema::table('order_origins', function (Blueprint $table) {
        	$table->renameColumn('origin', 'value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('origin_destinations', function (Blueprint $table) {
		    $table->renameColumn('value', 'address');
	    });
	    Schema::table('order_origins', function (Blueprint $table) {
		    $table->renameColumn('value', 'origin');
	    });
    }
}
