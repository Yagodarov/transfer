<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValueNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
        	$table->string('phone')->nullable()->change();
        	$table->string('password')->nullable()->change();
        	$table->integer('blocked')->default(0)->change();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('clients', function (Blueprint $table) {
			$table->string('phone')->nullable(false)->change();
			$table->string('password')->nullable(false)->change();
			$table->integer('blocked')->nullable(false)->change();
		});
    }
}
