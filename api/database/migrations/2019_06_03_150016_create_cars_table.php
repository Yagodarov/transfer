<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mark')->default('');
            $table->string('model')->default('');
            $table->string('number')->default('');
            $table->string('color')->default('');
            $table->integer('year')->nullable();
            $table->integer('seat')->nullable();
            $table->integer('conditioner')->nullable();
            $table->integer('bank_card')->nullable();
            $table->integer('baby_seat')->nullable();
            $table->integer('blocked')->defatult(0);
            $table->timestampsTz();
        });

        Schema::create('car_images', function (Blueprint $table) {
        	$table->integer('car_id')->unsigned();
        	$table->string('type');
        	$table->string('name');
        	$table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('car_images');
        Schema::dropIfExists('cars');
    }
}
