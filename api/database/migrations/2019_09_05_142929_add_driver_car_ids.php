<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverCarIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
			$table->integer('car_id')->unsigned()->nullable();
			$table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
			$table->integer('driver_id')->unsigned()->nullable();
			$table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('orders', function (Blueprint $table) {
			$table->dropForeign('orders_driver_id_foreign');
			$table->dropForeign('orders_car_id_foreign');
			$table->dropColumn('driver_id');
			$table->dropColumn('car_id');
		});
    }
}
