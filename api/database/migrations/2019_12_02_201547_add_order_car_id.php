<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderCarId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_bids', function (Blueprint $t) {
        	$t->integer('car_id')->unsigned()->nullable();
        	$t->foreign('car_id')->references('id')->on('cars')->onDelete('restrict');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('order_bids', function (Blueprint $t) {
			$t->dropForeign('order_bids_car_id_foreign');
			$t->dropColumn('car_id');
		});
    }
}
