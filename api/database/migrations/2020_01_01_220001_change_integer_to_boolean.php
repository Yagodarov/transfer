<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIntegerToBoolean extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table((new \App\Models\Android\Order())->getTable(), function (Blueprint $table) {
          $table->boolean('conditioner')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table((new \App\Models\Android\Order())->getTable(), function (Blueprint $table) {
        $table->integer('conditioner')->change();
      });
    }
}
