<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_seats', function (Blueprint $table) {
        	$table->string('weight')->change();
        	$table->string('age')->change();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('order_seats', function (Blueprint $table) {
			$table->integer('weight')->change();
			$table->integer('age')->change();
		});
    }
}
