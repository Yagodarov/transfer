<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create((new \App\Models\Android\Notification\Notification())->getTable(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 255);
			$table->string('push_token', 255);
            $table->string('body');
            $table->string('title');
            $table->string('icon');
			$table->string('type');

            $table->integer('car_id')->unsigned()->nullable();
			$table->foreign('car_id')->references('id')->on('cars')->onDelete('restrict');

            $table->integer('driver_id')->unsigned()->nullable();
			$table->foreign('driver_id')->references('id')->on('drivers')->onDelete('restrict');

            $table->integer('order_id')->unsigned()->nullable();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('restrict');

            $table->integer('order_bid_id')->unsigned()->nullable();
            $table->foreign('order_bid_id')->references('id')->on('order_bids')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists((new \App\Models\Android\Notification\Notification())->getTable());
    }
}
