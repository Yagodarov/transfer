<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTimeTz('departure_time')->nullable();
            $table->string('origin')->nullable();
            $table->string('comment')->nullable();
            $table->integer('passengers_count')->nullable();
            $table->integer('baggage_count')->nullable();
            $table->integer('conditioner')->nullable();
            $table->integer('bank_card_payment')->nullable();
            $table->integer('client_id')->unsigned()->nullable();
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->timestampsTz();
        });

        Schema::create('order_destinations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('address');
			$table->integer('order_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
			$table->timestampsTz();
		});

        Schema::create('order_seats', function (Blueprint $table) {
			$table->increments('id');
			$table->decimal('weight', 8,2);
			$table->decimal('age', 8, 2);
			$table->integer('order_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
			$table->timestampsTz();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('order_seats');
		Schema::dropIfExists('order_destinations');
        Schema::dropIfExists('orders');
    }
}
