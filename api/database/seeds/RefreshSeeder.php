<?php

use Illuminate\Database\Seeder;

class RefreshSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(RPSeeder::class);
    }
}
