<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RPSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();

        $admin = Role::create(['name' => 'admin', 'title' => 'Администратор']);
        $manager = Role::create(['name' => 'manager', 'title' => 'Менеджер']);

		$view_users = Permission::create(['name' => 'view_users']);
		$manage_services = Permission::create(['name' => 'manage_services']);
		$manage_drivers = Permission::create(['name' => 'manage_drivers']);
		$manage_driver_balances = Permission::create(['name' => 'manage_driver_balances']);
		$manage_settings = Permission::create(['name' => 'manage_settings']);
		$manage_users = Permission::create(['name' => 'manage_users']);
		$manage_cars = Permission::create(['name' => 'manage_cars']);
		$manage_clients = Permission::create(['name' => 'manage_clients']);
		$manage_client_balances = Permission::create(['name' => 'manage_client_balances']);
		$manage_orders = Permission::create(['name' => 'manage_orders']);

        $change_password = Permission::create(['name' => 'change_password']);
        $block_user = Permission::create(['name' => 'block_user']);
        $delete_user = Permission::create(['name' => 'delete_user']);

        $manage_towns = Permission::create(['name' => 'manage_towns']);

		$manage_towns->assignRole([$admin, $manager]);

		$manage_services->assignRole([$admin, $manager]);

		$manage_drivers->assignRole([$admin, $manager]);

		$manage_client_balances->assignRole([$admin]);

		$manage_driver_balances->assignRole([$admin]);

		$manage_cars->assignRole([$admin, $manager]);

		$manage_clients->assignRole([$admin, $manager]);

		$manage_orders->assignRole([$admin, $manager]);

		$manage_settings->assignRole($admin);

        $view_users->assignRole([$admin, $manager]);
		$manage_users->assignRole($admin);
		$block_user->assignRole($admin);
		$delete_user->assignRole($admin);
		$change_password->assignRole($admin);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
