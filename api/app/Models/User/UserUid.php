<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 08.04.2019
 * Time: 0:05
 */

namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;

class UserUid extends Model
{
	public $table='uid';


	protected $fillable = [
		'id',
		'uid',
		'client_id',
		'driver_id',
	];

}
