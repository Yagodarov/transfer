<?php

namespace App\Models;

use App\Models\User\Profile;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

/**
 * @property int $id
 * @property int $town_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 * @property Town $town
 */
class Driver extends Authenticatable implements JWTSubject
{
	public $total = 0;
    /**
     * @var array
     */
    protected $fillable = ['town_id', 'first_name', 'last_name', 'phone', 'password'];

	protected $hidden = ['password'];

    public function rules(Request $request) {
    	if ($request->isMethod('post')) {
			return [
				'town_id' => 'required|exists:towns,id',
				'first_name' => 'required|string|min:4|max:64',
				'last_name' => 'required|string|min:4|max:64',
				'phone' => ['required','string',function($attribute, $value, $fail) {
					$search = preg_replace('/[^0-9]/', '', $value);
					if (Driver::where([
						['phone', 'LIKE', "%{$search}%"],
					])->first()) {
						$fail('Поле Телефон должен быть уникальным');
					}
				}, 'regex:/^[+]7[\s][(][0-9]{3}[)][\s][0-9]{3}[-][0-9]{4}$|^[+]7[0-9]{10}$/'],
				'password' => 'required|string|min:4|max:64',
				'cars' => [function($attribute, $values, $fail) {
					$max_count = 1;
					if (count($values) > 1) {
//						return $fail(count($values));
						return $fail('Слишком много автомобилей');
					} else if (count($values) < 1) {
						return $fail('Необходимо выбрать автомобиль');
					}
				}],
			];
		} else {
			return [
				'town_id' => 'required|exists:towns,id',
				'first_name' => 'required|string|min:4|max:64',
				'last_name' => 'required|string|min:4|max:64',
				'phone' => ['required','string',function($attribute, $value, $fail) {
					$search = preg_replace('/[^0-9]/', '', $value);
					if (Driver::where([
						['phone', 'LIKE', "%{$search}%"],
						['id', '!=', $this->id]
					])->first()) {
						return $fail('Поле Телефон должен быть уникальным');
					}
				}, 'regex:/^[+]7[\s][(][0-9]{3}[)][\s][0-9]{3}[-][0-9]{4}$|^[+]7[0-9]{10}$/'],
				'password' => 'sometimes|string|min:4|max:64',
				'cars' => [function($attribute, $values, $fail) {
					$max_count = 1;
					if (count($values) > 1) {
//						return $fail(count($values));
						return $fail('Слишком много автомобилей');
					} else if (count($values) < 1) {
						return $fail('Необходимо выбрать автомобиль');
					}
				}],
			];
		}
	}

	public function setPhoneAttribute($value) {
		$this->attributes['phone'] = '+'.preg_replace("/[^0-9]/", "",  $value);
	}

	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = in_array($request->get('sort')['field'], (new self())->fillable) ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models =  self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->with('town')
			->select('*')
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});

		if (isset($request->get('query')['generalSearch'])) {
			$globalSearch = $request->get('query')['generalSearch'];
			$models
				->orWhere('drivers.first_name', 'LIKE', "%{$globalSearch}%")
				->orWhere('drivers.last_name', 'LIKE', "%{$globalSearch}%")
				->orWhere('drivers.phone', 'LIKE', "%{$globalSearch}%")
				->orWhere('drivers.id', '=', $globalSearch);
		}

		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

	public function storeCars(Request $request) {
    	$cars = $request->get('cars');
    	DriverCar::where('driver_id', $this->id)->delete();
    	foreach ($cars as $key => $value) {
			$driverCar = new DriverCar([
				'car_id' => $value,
				'driver_id' => $this->id
			]);
			$driverCar->save();
		}
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			$this->setPassword($request);
			if ($result = $this->save())
			{
				$this->storeCars($request);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function setPassword(Request $request) {
		// Edit
		if ($this->id) {
			if (!$request->password)
			{
				$this->password = $this->getOriginal('password');
			}
			if ($request->password){
				$this->password = bcrypt($request->password);
			}
		} else {
			$this->password = bcrypt($request->password);
		}
	}

	public function getTotal() {
    	$payments = $this->payments;
    	$total = 0;
    	foreach ($payments as $payment) {
    		if ($payment->type == 1) {
    			$total += $payment->value;
			} elseif ($payment->type == 0) {
				$total -= $payment->value;
			}
		}
		$this->total = $total;
		return $total;
	}

	public function saveToken(Request $request) {
		if (!$token = Token::where([
			['token', '=', $request->get('token')],
			['driver_id', '=', $this->getAttribute('id')],
		])->first()) {
			$token = new Token();
			$token->driver_id = Auth::user()->id;
			if ($result = $token->store($request))
				return true;
			else
			{
				return false;
			}
		} else {
			return true;
		}
	}
	
	public function getErrorMessages($request) {
		$validator = Validator::make(Input::all(), $this->rules($request));
    	return $validator->messages()->all();
	}

	public function getToken() {
		$token = Token::where([
			'driver_id' => Auth::user()->id
		])->first();
		return $token->token;
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function town()
    {
        return $this->belongsTo('App\Models\Town');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\DriverPayment');
    }

    public function cars()
    {
        return $this->hasMany('App\Models\DriverCar');
    }

	public function getJWTIdentifier()
	{
		return $this->getKey();
	}
	/**
	 * Return a key value array, containing any custom claims to be added to the JWT.
	 *
	 * @return array
	 */
	public function getJWTCustomClaims()
	{
		return [];
	}
}
