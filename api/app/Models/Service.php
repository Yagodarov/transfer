<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
/**
 * @property int $id
 * @property string $label
 * @property integer $blocked
 * @property string $created_at
 * @property string $updated_at
 */
class Service extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['label', 'id', 'blocked'];

	public function rules(Request $request) {
		return [
			'id' => 'sometimes|numeric|nullable',
			'label' => 'required|max:64',
			'blocked' => 'required|numeric',
		];
	}


	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = in_array($request->get('sort')['field'], (new self())->fillable) ? $request->get('sort')['field'] : 'callstart';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models =  self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->select('*')
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});

		if (isset($request->get('query')['generalSearch'])) {
			$globalSearch = $request->get('query')['generalSearch'];
			$models
				->orWhere('services.label', 'LIKE', "%{$globalSearch}%")
				->orWhere('services.id', '=', $globalSearch);
		}

		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}
}
