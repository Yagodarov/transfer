<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property int $id
 * @property string $label
 * @property integer $blocked
 * @property string $created_at
 * @property string $updated_at
 */
class Client extends Authenticatable implements JWTSubject
{
	use Notifiable;
    /**
     * @var array
     */
    protected $table = 'clients';
    protected $fillable = ['blocked', 'phone', 'first_name', 'last_name', 'password'];
	protected $hidden = ['password'];

    public function rules(Request $request) {
		if ($request->isMethod('post')) {
			return [
				'id' => 'sometimes|numeric|nullable',
				'first_name' => 'required|max:64',
				'last_name' => 'required|max:64',
				'phone' => ['sometimes','string', function($attribute, $value, $fail) {
					$search = preg_replace('/[^0-9]/', '', $value);
					if (Client::where([
						['phone', 'LIKE', "%{$search}%"],
					])->first()) {
						$fail('Поле Телефон должен быть уникальным');
					}
				}, 'regex:/^[+]7[\s][(][0-9]{3}[)][\s][0-9]{3}[-][0-9]{4}$|^[+]7[0-9]{10}$/'],
				'password' => 'sometimes|max:64',
				'blocked' => 'sometimes|numeric',
			];
		} else {
			return [
				'id' => 'sometimes|numeric|nullable',
				'first_name' => 'required|max:64',
				'last_name' => 'required|max:64',
				'phone' => ['sometimes','string', function($attribute, $value, $fail) {
					$search = preg_replace('/[^0-9]/', '', $value);
					if (Client::where([
						['phone', 'LIKE', "%{$search}%"],
						['id', '!=', $this->id]
					])->first()) {
						$fail('Поле Телефон должен быть уникальным');
					}
				}, 'regex:/^[+]7[\s][(][0-9]{3}[)][\s][0-9]{3}[-][0-9]{4}$|^[+]7[0-9]{10}$/'],
				'password' => 'sometimes|max:64',
				'blocked' => 'sometimes|numeric',
			];
		}
	}

	public function setPhoneAttribute($value) {
		$this->attributes['phone'] = '+'.preg_replace("/[^0-9]/", "",  $value);
	}

	public function saveToken(Request $request) {
		$token = Token::where([
			['token', '=', $request->get('pushToken')],
			['client_id', '=', $this->getAttribute('id')]
		])->first();
		if (!$token) {
			$token = new Token();
			$token->client_id = $this->getAttribute('id');
			if ($token->store($request))
				return true;
			else
				return false;
		}
	}

	public function getTotal() {
		$payments = $this->payments;
		$total = 0;
		foreach ($payments as $payment) {
			if ($payment->type == 1) {
				$total += $payment->value;
			} elseif ($payment->type == 0) {
				$total -= $payment->value;
			}
		}
		$this->total = $total;
		return $total;
	}

	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = in_array($request->get('sort')['field'], (new self())->fillable) ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$table_name = (new self())->getTable();
		$models =  self
			::select('*')
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});

		if (isset($request->get('query')['generalSearch'])) {
			$globalSearch = $request->get('query')['generalSearch'];
			$models
				->orWhere("{$table_name}.first_name", 'LIKE', "%{$globalSearch}%")
				->orWhere("{$table_name}.last_name", 'LIKE', "%{$globalSearch}%")
				->orWhere("{$table_name}.phone", 'LIKE', "%{$globalSearch}%")
				->orWhere("{$table_name}.id", '=', $globalSearch);
		}

		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			$this->setPassword($request);
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function payments()
	{
		return $this->hasMany('App\Models\ClientPayment');
	}
	public function uid()
	{
		return $this->hasMany('App\Models\User\UserUid');
	}

	public function setPassword(Request $request) {
    	// Edit
    	if ($this->id) {
			if (!$request->password)
			{
				$this->password = $this->getOriginal('password');
			}
			if ($request->password){
				$this->password = bcrypt($request->password);
			}
		} else {
			$this->password = bcrypt($request->password);
		}
	}

	public function getJWTIdentifier()
	{
		return $this->getKey();
	}
	/**
	 * Return a key value array, containing any custom claims to be added to the JWT.
	 *
	 * @return array
	 */
	public function getJWTCustomClaims()
	{
		return [];
	}
}
