<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
/**
 * @property int $id
 * @property string $label
 * @property integer $blocked
 * @property string $created_at
 * @property string $updated_at
 */
class ClientPayment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id', 'client_id', 'comment', 'value', 'type'];

    public function rules(Request $request) {
		return [
			'id' => 'sometimes|numeric|nullable',
			'client_id' => 'required|numeric|exists:clients,id',
			'comment' => 'required|min:4|max:64',
			'value' => 'required|numeric|min:1|digits_between:1,10',
			'type' => 'required|numeric',
		];
	}


	public static function search($id, Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = in_array($request->get('sort')['field'], (new self())->fillable) ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models =  self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->select('*')
			->where("client_id", '=', "{$id}")
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});

//		if (isset($request->get('query')['generalSearch'])) {
//			$globalSearch = $request->get('query')['generalSearch'];
//			$models
//		}

		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request, $perpage, $page){
				return $models->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages()->all(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function driver()
	{
		return $this->belongsTo('App\Models\Client');
	}
}
