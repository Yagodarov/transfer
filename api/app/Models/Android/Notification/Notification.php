<?php


namespace App\Models\Android\Notification;


use App\Models\Car;
use App\Models\Driver;
use App\Models\Order;
use App\Models\OrderBid;
use App\Models\Token;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Notification extends Model
{
	protected $fillable = ['key', 'type', 'push_token', 'body', 'title', 'icon', 'order_bid_id', 'car_id', 'driver_id', 'order_id'];
	protected $hidden = [];
	protected function rules() {
		return [];
	}

	public function getOrderBidData() {
		$result = [
			'order' => $this->order,
			'driver' => $this->driver,
			'car' => $this->car
		];
		return $result;
	}

	public function notifyOrderBid(OrderBid $orderBid, $type) {
		$attributes = [
			'key' => $this->getServerKey(),
			'type' => $type,
			'push_token' => Auth::user()->getToken(),
			'body' => $this->getBodyMesssage($type),
			'title' => $this->getTitleMesssage($type),
			'icon' => $this->getIcon($type),
			'order_bid_id' => $orderBid->getAttribute('id'),
			'click_action' => 'FLUTTER_NOTIFICATION_CLICK'
		];
		$this->fill($orderBid->getAttributes());
		$this->fill($attributes);
		$this->setClientToken();
		$this->save();
		$this->send();
	}

	public function setClientToken() {
		$order_id = $this->getAttribute('order_id');
		$order = Order::find($order_id);
		$client_id = $order->getAttribute('client_id');
		$token = Token::where('client_id', $client_id)->latest('id')->first();
		if (!$token) {
			$this->setAttribute('push_token', 'client not have token');
		} else {
			$this->setAttribute('push_token', $token->getAttribute('token'));
		}
	}

	public function getServerKey() {
		return env('FCM_KEY');
	}

	public function getIcon($id) {
		$values = [
			'orderBidCreated' => 'ic_launcher',
			'orderBidConfirmedByDriver' => 'ic_launcher',
		];
		return $values[$id];
	}

	public function getTitleMesssage($id) {
		$values = [
			'orderBidCreated' => 'orderBidCreatedTitle',
			'orderBidConfirmedByDriver' => 'orderBidConfirmedByDriverTitle'
		];
		return $values[$id];
	}

	public function getBodyMesssage($id) {
		$values = [
			'orderBidCreated' => 'orderBidCreatedBody',
			'orderBidConfirmedByDriver' => 'orderBidConfirmedByDriverBody'
		];
		return $values[$id];
	}

	public function getSendableData() {
		return [
			'to' => $this->getAttribute('push_token'),
			"notification" => [
				"body" => $this->getAttribute('body'),
				"title" => $this->getAttribute('title'),
				"icon" =>  $this->getAttribute('icon') // ic_launcher
			],
			"data" => $this->getOrderBidData()
		];
	}

	public function send() {
		if ($this->getAttribute('pushToken')) {
			$data = json_encode($this->getSendableData());
			//FCM API end-point
			$url = 'https://fcm.googleapis.com/fcm/send';
			//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key

			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization:key='.$this->server_key
			);
			//CURL request to route notification to FCM connection server (provided by Google)
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('Oops! FCM Send Error: ' . curl_error($ch));
			}
			curl_close($ch);
		}
	}

	public function driver() {
		return $this->belongsTo(Driver::class);
	}

	public function order() {
		return $this->belongsTo(Order::class);
	}

	public function car() {
		return $this->belongsTo(Car::class);
	}
}
