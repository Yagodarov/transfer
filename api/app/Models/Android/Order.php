<?php

namespace App\Models\Android;

use App\Models\OrderDestination;
use App\Models\OrderSeat;
use App\Models\Origin;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

/**
 * Class Order
 * @package App\Models\Android
 */
class Order extends \App\Models\Order
{
	protected $table = 'orders';
	protected $fillable = [
		'id',
		'departure_time',
		'adults_count',
		'baggage_count',
		'conditioner',
		'payment_type',
		'client_id',
		'car_id',
		'driver_id',
		'comment',
		'status',
		'lat',
		'lng'
	];
	
	protected static function boot()
	{
		parent::boot(); // TODO: Change the autogenerated stub
		self::creating(function ($model) {
			$model->setAttribute('status', '0');
			$model->setAttribute('client_id', Auth::user()->id);
		});
	}
	
	public function rules(Request $request)
	{
		if ($request->isMethod('post')) {
			return [
				'id' => 'sometimes|numeric|nullable',
				'departure_time' => ['required', function ($attribute, $value, $fail) {
					$now = Carbon::now();
					$datetime = new Carbon($value);
					$settings = Settings::where('id', 1)->first();
					if (!$datetime) {
						return $fail('Неправильная дата');
					}
					if ($datetime->diffInHours($now) < $settings->order_time) {
						$fail('На указанное время нельзя создать заказ');
					}
				}],
				'adults_count' => 'required|digits_between:1,10',
				'baggage_count' => 'required|numeric',
				'conditioner' => 'required|boolean',
				'payment_type' => 'required|numeric',
				'status' => 'sometimes|numeric|between:1,13',
				'client_id' => ['sometimes', 'exists:clients,id', function ($attribute, $value, $fail) {
					if (Auth::guard('api')) {
						return $fail('Заполните клиента');
					}
				}],
				'origin' => ['required', function ($attribute, $value, $fail) use ($request) {
					$origin = new Origin();
					$result = $origin->validate($request->get('origin'));
					if ($result['status'] == false) {
						return $fail($result['messages'][0]);
					}
				}],
				'comment' => ['max:64'],
			];
		} else {
			return [
				'id' => 'required|nullable',
				'departure_time' => ['required', function ($attribute, $value, $fail) {
					$now = Carbon::now();
					$datetime = new Carbon($value);
					$settings = Settings::where('id', 1)->first();
					if (!$datetime) {
						return $fail('Неправильная дата');
					}
				}],
				'origin' => ['required', function ($attribute, $value, $fail) use ($request) {
					$origin = new Origin();
					$result = $origin->validate($value);
					if ($result['status'] == false) {
						return $fail($result['messages'][0]);
					}
				}],
				'adults_count' => 'required|digits_between:1,10',
				'baggage_count' => 'required|numeric',
				'conditioner' => 'required|boolean',
				'payment_type' => 'required|numeric',
				'status' => 'sometimes|numeric|between:1,13',
				'client_id' => ['sometimes', 'exists:clients,id', function ($attribute, $value, $fail) {
					if (Auth::guard('api')) {
						return $fail('Заполните клиента');
					}
				}],
				'comment' => ['max:64'],
			];
		}
	}
	
	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = $request->get('sort')['field'] ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models = self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->with('destinations')
			->with('car')
			->with('origin')
			->with('childrens')
			->with('driver')
			->with('bid.driver')
			->select('*')
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});
		
		if (isset($request->get('query')['generalSearch'])) {
			$globalSearch = $request->get('query')['generalSearch'];
			$models
				->orWhere('services.label', 'LIKE', "%{$globalSearch}%")
				->orWhere('services.id', '=', $globalSearch);
		}
		
		if (Auth::guard('client')->check()) {
			$models->where('orders.client_id', Auth::user()->id);
		}
		if (Auth::guard('driver')->check()) {
			$models->whereNull('orders.driver_id');
		}
		
		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page) {
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' => $page,
				'pages' => ceil($count / 10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}
	
	public static function driverOrders(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = $request->get('sort')['field'] ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models = self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->with('destinations')
			->with('car')
			->with('origin')
			->with('childrens')
			->with('driver')
			->with('bid')
			->with('bid.driver')
			->select('*')
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});
		
		if (Auth::guard('driver')) {
			$models
				->where('orders.driver_id', Auth::user()->id)
				->orWhere('orders.driver_id', null);
		}
		
		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page) {
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' => $page,
				'pages' => ceil($count / 10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}
	
	/**
	 * Fills order from request and saves it
	 * @param Request|null $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request = null)
	{
		if ($request == null) {
			$validator = Validator::make($this->attributes, $this->rules($request));
			if ($validator->fails()) {
				return response()->json($validator->messages()->all(), 403);
			} else {
				$this->fill($request->all());
				if ($result = $this->save()) {
					return response()->json($this, 200);
				} else
					return response()->json($result, 403);
			}
		}
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages()->all(), 403);
		} else {
			$this->fill($request->all());
			if ($result = $this->save()) {
				$this->saveDestinations($request);
				$this->saveSeats($request);
				$this->saveOrigin($request);
				$returnModel = $this;
				$returnModel->origin;
				$this->destinations;
				$this->childrens;
				return response()->json($this, 200);
			} else
				return response()->json($result, 403);
		}
	}
	
	/**
	 * @param Request $request
	 * @return bool|void
	 */
	public function saveOrigin(Request $request)
	{
		if (!$origin = Origin::where('order_id', $this->getAttribute('order_id'))->first()) {
			$origin = new Origin();
		}
		$origin->fill($request->get('origin'));
		$origin->setAttribute('order_id', $this->getAttribute('id'));
		return $origin->save();
	}
	
	public function saveSeats(Request $request)
	{
		if ($this->id) {
			OrderSeat::where('order_id', $this->id)->delete();
		}
		if ($request->get('children_age')) {
			$seats = $request->get('children_age');
			foreach ($seats as $item) {
				$model = new OrderSeat([
					'order_id' => $this->id,
					'age' => $item,
					'weight' => ''
				]);
				try {
					$model->save();
				} catch (\Exception $e) {
				
				}
			}
		}
	}
	
	public function saveDestinations(Request $request)
	{
		if ($this->id) {
			OrderDestination::where('order_id', $this->id)->delete();
		}
		if ($request->get('destinations')) {
			$destinations = $request->get('destinations');
			foreach ($destinations as $item) {
				$item = (array)$item;
				$model = new OrderDestination([
					'order_id' => $this->id,
					'value' => $item['value'],
					'lat' => isset($item['lat']) ? $item['lat'] : '',
					'lng' => isset($item['lng']) ? $item['lng'] : ''
				]);
				try {
					$model->save();
				} catch (\Exception $e) {
				
				}
			}
		}
	}
	
	public function childrens()
	{
		return $this->hasMany(OrderSeat::class);
	}
	
	public function car()
	{
		return $this->belongsTo('App\Models\Car');
	}
	
	public function bid() {
		return $this->hasMany('App\Models\OrderBid');
	}
	
	public function driver()
	{
		return $this->belongsTo('App\Models\Driver');
	}
	
	public function seats()
	{
		return $this->hasMany('App\Models\OrderSeat');
	}
	
	public function destinations()
	{
		return $this->hasMany('App\Models\OrderDestination');
	}
	
	public function origin()
	{
		return $this->hasOne(Origin::class);
	}
}
