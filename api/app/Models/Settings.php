<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 26.04.2019
 * Time: 20:23
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Settings extends Model
{
	protected $fillable = [
		'order_percent', 'order_time'
	];

	public function rules(Request $request) {
		return [
			'order_percent' => 'required|numeric',
			'order_time' => 'required|numeric',
		];
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}
}
