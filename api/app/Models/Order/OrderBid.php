<?php

namespace App\Models;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class OrderBid extends Model
{
	protected $fillable = [
		'driver_id',
		'order_id',
		'car_id',
		'price',
		'status',
	];

	protected static function boot()
	{
		parent::boot(); // TODO: Change the autogenerated stub
		self::creating(function (OrderBid $orderBid) {
			$orderBid->status = 1;
		});
		self::created(function (OrderBid $orderBid) {
			$orderBid->notify('orderBidCreated');
		});
		self::updated(function (OrderBid $orderBid) {
			if ($orderBid->getAttribute('status') == '3' && $orderBid->isDirty('status')) {
				$orderBid->notify('orderBidConfirmedByDriver');
			}
		});
	}

	public function notify($type) {
		(new Android\Notification\Notification())->notifyOrderBid($this, $type);
	}

	public function rules(Request $request) {
		if (!$this->getAttribute('id')) {
			return [
				'order_id' => ['required', 'exists:orders,id', function($attribute, $value, $fail){
					if (OrderBid::where([
						['order_id', '=', $value],
						['driver_id', '=', Auth::user()->id]
					])->first()) {
						$fail("Водитель уже сделал ставку");
					}
				}],
				'price' => 'required|numeric',
//				'status' => 'sometimes|numeric|digits_between:1'
			];
		} else {
			return [
				'order_id' => 'required|exists:orders,id',
				'price' => 'required|numeric',
				'status' => ['sometimes','numeric', 'digits_between:0,3', function($attribute, $value, $fail){
					if ($this->getOriginal('status') == 0) {

					}
					if ($this->getOriginal('status') == 3) {

					}
				}]
			];
		}
	}


	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = in_array($request->get('sort')['field'], (new self())->fillable) ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models =  self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->select('*')
			->with('order')
			->with('order.destinations')
			->with('order.car')
			->with('order.origin')
			->with('order.childrens')
			->with('order.driver')
			->where('order_bids.driver_id', '=', Auth::user()->id)
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});

		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules($request));
		$params = $request->all();
		if ($validator->fails()) {
			return response()->json($validator->messages()->all(), 403);
		}
		else
		{
			$this->fill($request->post());
			$this->driver_id = Auth::user()->id;
			$this->car_id = isset(Auth::user()->cars[0]) ? Auth::user()->cars[0]->car_id : null;
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}
	
	public function order() {
		return $this->belongsTo(\App\Models\Android\Order::class);
	}

	public function driver()
	{
		return $this->belongsTo('App\Models\Driver');
	}
}
