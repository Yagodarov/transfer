<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
/**
 * @property int $id
 * @property string $label
 * @property integer $blocked
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = [
		'id',
		'departure_time',
		'origin',
		'adults_count',
		'baggage_count',
		'conditioner',
		'payment_type',
		'client_id',
		'car_id',
		'driver_id',
		'comment',
		'status',
	];

	public function setDepartureTimeAttribute($value)
	{
		$this->attributes['departure_time'] =  Carbon::parse($value)->toDateTimeString();
	}

	public function rules(Request $request) {
		if ($request->isMethod('post')) {
			return [
				'id' => 'sometimes|numeric|nullable',
				'departure_time' => ['required', function($attribute, $value, $fail) {
					$now = Carbon::now();
					$datetime = new Carbon($value);
					$settings = Settings::where('id', 1)->first();
					if (!$datetime) {
						return $fail('Неправильная дата');
					}
					if ($datetime->diffInHours($now) < $settings->order_time) {
						$fail('На указанное время нельзя создать заказ');
					}
				}],
				'origin' => ['required', 'min:3', 'max:64', function($attribute, $value, $fail) use ($request) {
					$origin = new Origin();
					$result = $origin->validate($request->all());
					if ($result['status'] == false) {
						return $fail($result['messages'][0]);
					}
				}],
				'adults_count' => 'required|digits_between:1,10',
				'baggage_count' => 'required|numeric',
				'conditioner' => 'required|boolean',
				'payment_type' => 'required|numeric',
				'status' => 'required|numeric|between:0,13',
				'client_id' => ['sometimes', 'exists:clients,id', function($attribute, $value, $fail) {

				}],
				'comment' => ['max:64'],
			];
		} else {
			return [
				'id' => 'required|nullable',
				'departure_time' => ['required', function($attribute, $value, $fail) {
					$now = Carbon::now();
					$datetime = new Carbon($value);
					$settings = Settings::where('id', 1)->first();
					if (!$datetime) {
						return $fail('Неправильная дата');
					}
				}],
				'origin' => ['required', 'min:3', 'max:64', function($attribute, $value, $fail) use ($request) {
					$origin = new Origin();
					$result = $origin->validate($request);
					if ($result['status'] == false) {
						return $fail($result['messages'][0]);
					}
				}],
				'adults_count' => 'required|digits_between:1,10',
				'baggage_count' => 'required|numeric',
				'conditioner' => 'required|boolean',
				'payment_type' => 'required|numeric',
				'status' => 'required|numeric|between:0,13',
				'client_id' => ['sometimes', 'exists:clients,id'],
				'comment' => ['max:64'],
			];
		}
	}


	public static function search(Request $request)
	{
		$perpage = $request->get('pagination')['perpage'] ?? 10;
		$page = $request->get('pagination')['page'] ?? 1;
		$field = $request->get('sort')['field'] ? $request->get('sort')['field'] : 'id';
		$sort = $request->get('sort')['sort'] ?? 'desc';
		$models =  self
			::when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->with('destinations')
			->with('car')
			->with('driver')
			->select('*')
			->when(!$request->get('orderBy'), function ($models) use ($request, $field, $sort) {
				return $models
					->orderBy($field, $sort);
			});


		if (isset($request->get('query')['generalSearch'])) {
			$globalSearch = $request->get('query')['generalSearch'];
			$models
				->orWhere('services.label', 'LIKE', "%{$globalSearch}%")
				->orWhere('services.id', '=', $globalSearch);
		}


		$count = $models->get()->count();
		$models = $models
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request, $perpage, $page){
				return $users->skip(($page - 1) * $perpage)->take($perpage);
			})
			->get();
		return response()->json([
			'models' => $models,
			'meta' => [
				'page' =>  $page,
				'pages' => ceil($count/10),
				'perpage' => $perpage,
				'total' => $count,
			]
		]);
	}

	public function saveDestinations(Request $request) {
		if ($this->id) {
			OrderDestination::where('order_id', $this->id)->delete();
		}
		if ($request->get('destination')) {
			foreach ($request->get('destination') as $item) {
				$model = new OrderDestination([
					'order_id' => $this->id,
					'value' => $item['value']
				]);
				try{
					$model->save();
				} catch (\Exception $e) {

				}
			}
		}
	}

	public function saveSeats(Request $request) {
		if ($this->id) {
			OrderSeat::where('order_id', $this->id)->delete();
		}
		if ($request->get('seat')) {
			foreach ($request->get('seat') as $item) {
				$model = new OrderSeat([
					'order_id' => $this->id,
					'age' => $item['age'],
					'weight' => $item['weight']
				]);
				try{
					$model->save();
				} catch (\Exception $e) {

				}
			}
		}
	}

	public function store(Request $request = null)
	{
		if ($request == null) {
			$validator = Validator::make($this->attributes, $this->rules($request));
			if ($validator->fails()) {
				return response()->json($validator->messages(), 403);
			}
			else
			{
				$this->fill($request->all());
				if ($result = $this->save())
				{
					return response()->json($result, 200);
				}
				else
					return response()->json($result, 403);
			}
		}
		$validator = Validator::make(Input::all(), $this->rules($request));
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				$this->saveDestinations($request);
				$this->saveSeats($request);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function childrens()
	{
		return $this->hasMany(OrderSeat::class);
	}

	public function car()
	{
		return $this->belongsTo('App\Models\Car');
	}

	public function bid() {
		return $this->hasMany('App\Models\OrderBid');
	}

	public function driver()
	{
		return $this->belongsTo('App\Models\Driver');
	}

	public function seats()
	{
		return $this->hasMany('App\Models\OrderSeat');
	}

	public function destinations()
	{
		return $this->hasMany('App\Models\OrderDestination');
	}

	public function origin()
	{
		return $this->hasOne(Origin::class);
	}
}
