<?php

namespace App\Console\Commands;

use App\Models\Call;
use App\Models\Contact;
use App\Models\Roistat;
use App\Models\Zadarma;
use Illuminate\Console\Command;

class GetStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$zd = new Zadarma();
    	$zd->fillCalls();

    	Contact::fillContactInfoAll();
    }
}
