<?php

namespace App\Http\Controllers;

use App\Models\DriverPayment as BaseModel;
use Illuminate\Http\Request;

class DriverBalanceController extends Controller
{
	public function index($id, Request $request)
	{
		return BaseModel::search($id, $request);
	}

	public function get($id)
	{
		$model = BaseModel::find($id);
		return response()->json($model);
	}

	public function store(Request $request)
	{
		return (new BaseModel())->store($request);
	}

	public function update(Request $request)
	{
		return (BaseModel::find($request->get('id'))->store($request));
	}

	public function delete($id)
	{
		return response()->json(BaseModel::find($id)->delete());
	}


	public function toggleStatus($id) {
		$model = BaseModel::find($id);
		$model->blocked = !$model->blocked;
		return response()->json($model->save());
	}
}
