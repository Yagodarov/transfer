<?php

namespace App\Http\Controllers;

use App\Models\Service as BaseModel;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
	public function index(Request $request)
	{
		return BaseModel::search($request);
	}

	public function get($id)
	{
		$model = BaseModel::find($id);
		return response()->json($model);
	}

	public function store(Request $request)
	{
		return (new BaseModel())->store($request);
	}

	public function update(Request $request)
	{
		return (BaseModel::find($request->get('id'))->store($request));
	}

	public function delete($id)
	{
		return response()->json(BaseModel::find($id)->delete());
	}


	public function toggleStatus($id) {
		$model = BaseModel::find($id);
		$model->blocked = !$model->blocked;
		return response()->json($model->save());
	}
}
