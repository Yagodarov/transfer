<?php

namespace App\Http\Controllers;

use App\Models\Order as BaseModel;
use Illuminate\Http\Request;

class OrderController extends Controller
{
	public function index(Request $request)
	{
		return BaseModel::search($request);
	}

	public function driverOrders(Request $request)
	{
		return BaseModel::searchByDriver($request);
	}

	public function get($id)
	{
		$model = BaseModel::find($id);
		$model->seats;
		$model->destinations;
		return response()->json($model);
	}

	public function store(Request $request)
	{
		return (new BaseModel())->store($request);
	}

	public function update(Request $request)
	{
		return (BaseModel::find($request->get('id'))->store($request));
	}
	public function changeStatus(Request $request)
	{
		$model = BaseModel::find($request->get('id'));
		return $model->store($request);
	}

	public function delete($id)
	{
		return response()->json(BaseModel::find($id)->delete());
	}
}
