<?php

namespace App\Http\Controllers\ApiControllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Driver;
use App\Models\User\User;
use App\Models\User\UserUid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AndroidLoginController extends Controller
{

    public function getClient()
	{
		$user = Client::where('id', Auth::user()->id)->first();
		$data = [];
		$data['user'] = $user;
//		$user['permissions_list'] = $user->getAllPermissions();
		return response()->json($data, 200);
	}

    public function updateClient(Request $request)
	{
		return (Client::find('uid', $request->get('uid'))->store($request));
	}

    public function updateDriver(Request $request)
	{
		return (Driver::find('uid', $request->get('uid'))->store($request));
	}

    public function getDriver()
	{
		$user = Driver::where('id', Auth::user()->id)->first();
		$data = [];
		$data['user'] = $user;
//		$user['permissions_list'] = $user->getAllPermissions();
		return response()->json($data, 200);
	}

	public function registerClient(Request $request) {
		try {
			$client = Client::with(['uid' => function ($query) use ($request) {
				$query->where('uid', '=', $request->get('uid'));
			}])->first();
			if (!$client)
			{
				$client = (new Client());
			}
			$validator = Validator::make($request->all(), $client->rules($request));
			if ($validator->fails()) {
				return response()->json($validator->messages()->all(), 403);
			}
			else {
				$client->fill($request->all());
			}
			if ($result = $client->save()) {
				return response()->json([
					'message' => 'Успешно'
				], 403);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json([
				'message' => 'Ошибка сервера'
			], 500);
		}
	}

	public function authenticateClient(Request $request)
	{
		try {
			$client = Client::whereHas('uid', function ($query) use ($request) {
				$query->where('uid', '=', $request->get('uid'));
			})->first();
			if ($client)
			{
				if ($client->blocked) {
					return response()->json([
						'message' => 'Аккаунт заблокирован'
					], 401);
				} else {
					$token = auth()->login($client);
				}
			} elseif(!$client) {
				return response()->json([
					'message' => 'Профиль не заполнен'
				], 403);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json([
				'message' => 'Ошибка сервера'
			], 500);
		}

		// all good so return the token
		$result = $client;
		$result['accessToken'] = $token;
		$uid = $request->get('uid');
		if ($uid) {
			UserUid::where('client_id', $client->id)->delete();
			$userId = new UserUid([
				'client_id' => $client->id,
				'uid' => $uid,
			]);
			$userId->save();
		} else {
			return response()->json([
				'message' => 'Отсутствует uid'
			], 401);
		}
		$result->uid;
		return response()->json($result);
	}

	public function authenticateDriver(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('phone', 'password');
		try {
			$model = Driver::where([
				['phone', '=', $request->get('phone')],
			])->first();
			$res = Hash::check($request->get('password'), $model['password']);
			if ($res)
			{
				if ( $model->blocked) {
					return response()->json([
						'message' => 'Аккаунт заблокирован'
						], 401);
				} else {
					$token = auth()->login($model);
				}
			} else {
				return response()->json([
					'message' => 'Неверные данные'
				], 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json([
				'message' => 'Ошибка сервера'
			], 500);
		}

		// all good so return the token
		if (!($request->get('token') && $model->saveToken($request))) {
			return response()->json([
				'message' => 'Не удалось сохранить токен'
			]);
		}
		$result = $model;
		$result['accessToken'] = $token;
		return response()->json($result);
	}
}
