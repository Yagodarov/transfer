<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Models\Android\Order as BaseModel;
use Illuminate\Http\Request;

class AndroidOrderController extends Controller
{
	/**
	 * Search orders
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function index(Request $request)
	{
		return BaseModel::search($request);
	}
	
	/**
	 * Get instance of Order Model
	 *
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function get($id)
	{
		$model = BaseModel::find($id);
		$model->seats;
		$model->origin;
		$model->destinations;
		return response()->json($model);
	}
	
	/**
	 * Search driver orders
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	
	public function driverOrders(Request $request)
	{
		return BaseModel::driverOrders($request);
	}
	
	/**
	 * Creates and saves new order
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{
		return (new BaseModel())->store($request);
	}
	
	/**
	 * Updates and saves order
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request)
	{
		$model = BaseModel::find($request->get('id'));
		if (!$model) {
			return response()->json([
				'error' => 'Не найден такой заказ'
			], 403);
		}
		$model->status = $request->get('status');
		if ($model->save()) {
			return $model;
		} else {
			return response()->json([
				'error' => 'Не удалось обновить'
			], 403);
		}
	}
}
