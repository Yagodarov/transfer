<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Settings as BaseModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class SettingController extends Controller
{
	public function store(Request $request)
	{
		$model = BaseModel::where('id', '=', 1)->first() ?? new BaseModel(['id' => 1]);
		return $model->store($request);
	}


	public function get()
	{
		if (!$model = BaseModel::find(1)) {
			$model = new BaseModel(['id' => 1]);
			$model->save();
		};
		return response()->json($model);
	}
}
