<?php

namespace App\Http\Controllers;

use App\Models\OrderBid as BaseModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderBidController extends Controller
{
	public function index(Request $request)
	{
		return BaseModel::search($request);
	}

	public function store(Request $request)
	{
		return (new BaseModel())->store($request);
	}

	public function update(Request $request)
	{
		$orderBid = BaseModel::where([
			['driver_id', Auth::user()->id],
			['order_id', $request->get('order_id')],
		])->first();
		return ($orderBid->store($request));
	}

	public function delete($id)
	{
		return response()->json(BaseModel::find($id)->delete());
	}

	public function toggleStatus(Request $request) {
		$model = BaseModel::find([
			['driver_id', Auth::user()->id],
			['order_id', $request->get('order_id')],
		]);
		$model->status = $request->get('status');
		return response()->json($model->save());
	}
}
