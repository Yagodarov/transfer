<?php

namespace App\Http\Middleware;

use App\Models\Client;
use App\Models\Driver;
use Closure;
use Illuminate\Support\Facades\Auth;

class AndroidId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (!$request->get('uid')) {
			return response()->json([
				'message' => 'Отстутствует uid'
			], 403);
		}
		$user = Client::with(['uid' => function ($query) use ($request) {
			$query->where('uid', '=', $request->get('uid'));
		}])->first();
		if (!$user) {
			$user = new Client();
			$user->fill($request->all());
			$user->save();
			$user->saveToken($request);
		} else {
			$user->fill($request->all());
			$user->save();
			$user->saveToken($request);
		}
		Auth::login($user);
		return $next($request);
    }
}
