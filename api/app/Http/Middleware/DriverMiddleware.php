<?php

namespace App\Http\Middleware;

use App\Models\Driver;
use Closure;
use Illuminate\Support\Facades\Auth;

class DriverMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $user = Driver::find(Auth::user()->id);
	    $result = $user->saveToken($request);
	    if ($result != true) {
		    return response()->json([
			    'message' => 'Не удалось сохранить токен'
		    ], 403);
	    }
	    Auth::login($user);
	    return $next($request);
    }
}
