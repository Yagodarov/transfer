(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-orders-orders-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/orders/orders-edit/orders-edit.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/orders/orders-edit/orders-edit.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"kt-portlet\">\r\n\t<kt-portlet-header [title]=\"getComponentTitle()\" [class]=\"'kt-portlet__head--lg'\" [viewLoading$]=\"loading$\">\r\n\t\t<ng-container ktPortletTools>\r\n\t\t\t<a [routerLink]=\"['../']\" class=\"btn btn-secondary kt-margin-r-10\" mat-raised-button matTooltip=\"Назад к списку\">\r\n\t\t\t\t<i class=\"la la-arrow-left\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Назад</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-primary kt-margin-r-10\" color=\"primary\" (click)=\"onSubmit(true)\" mat-raised-button matTooltip=\"Сохранить и продолжить\">\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сохранить</span>\r\n\t\t\t</a>\r\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-secondary kt-margin-r-10\" (click)=\"reset()\" mat-raised-button matTooltip=\"Отменить изменения\">\r\n\t\t\t\t<i class=\"la la-cog\"></i>\r\n\t\t\t\t<span class=\"kt-hidden-mobile\">Сбросить</span>\r\n\t\t\t</a>\r\n\t\t</ng-container>\r\n\t</kt-portlet-header>\r\n\t<br>\r\n\t<!--begin::Form-->\r\n\t<kt-portlet-body>\r\n\t\t<form class=\"kt-form kt-form--label-right\" [formGroup]=\"modelForm\" >\r\n\t\t\t<mat-tab-group [(selectedIndex)]=\"selectedTab\" (selectedTabChange)=\"onTabChange($event)\">\r\n\t\t\t\t<mat-tab ngClass=\"{'mat-tab-error' : hasFormErrors}\">\r\n\t\t\t\t\t<ng-template mat-tab-label>\r\n\t\t\t\t\t\t<i class=\"mat-tab-label-icon fa fa-user\"></i>\r\n\t\t\t\t\t\tБазовая информация\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<ng-template matTabContent>\r\n\t\t\t\t\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\t\t\t\t\t\t\t<kt-alert *ngIf=\"hasFormErrors\" type=\"warn\" [showCloseButton]=\"true\" [duration]=\"10000\">\r\n\t\t\t\t\t\t\t\tОшибка! Форма не прошла валидацию.\r\n\t\t\t\t\t\t\t</kt-alert>\r\n\t\t\t\t\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let control of formConfig(); trackBy:trackByFn\">\r\n\t\t\t\t\t\t\t\t\t<ng-container *ngIf=\"config.orientation == 'horizontal' && isFieldVisible(control.permission)\">\r\n\t\t\t\t\t\t\t\t\t\t<div *ngIf=\"control['type'] == 'text'\r\n\t\t|| control['type'] == 'datetime'\r\n\t\t|| control['type'] == 'datetime-local'\r\n\t\t|| control['type'] == 'password'\r\n\t\t|| control['type'] == 'email'\" class=\"form-group row validated\">\r\n\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<input autocomplete=\"off\" [type]=\"control['type']\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t<div *ngIf=\"control['type'] == 'select'\" class=\"form-group row validated\">\r\n\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<select autocomplete=\"off\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\" class=\"form-control\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<option *ngIf=\"control.nullValue\" [value]=\"null\">-- Выберите --</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<option *ngFor=\"let option of control.options\" [value]=\"option.value\">{{option.label}}</option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t<div *ngIf=\"control['type'] == 'textarea'\" class=\"form-group row validated\">\r\n\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-form-label col-lg-2\" [for]=\"control.field\">{{control.label}}</label>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<textarea autocomplete=\"off\" formControlName=\"{{control.field}}\" [ngClass]=\"hasError(control.field)\" [id]=\"control.field\"></textarea>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"invalid-feedback\">{{getErrorMessage(control.field)}}</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t<ng-container *ngIf=\"config.orientation == 'vertical'\"></ng-container>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</mat-tab>\r\n\r\n\t\t\t\t<mat-tab>\r\n\t\t\t\t\t<ng-template mat-tab-label>\r\n\t\t\t\t\t\t<i class=\"mat-tab-label-icon fa fa-user\"></i>\r\n\t\t\t\t\t\tПункты назначения\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<ng-template matTabContent>\r\n\t\t\t\t\t\t<ng-container>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<div class=\"col-lg-8 mx-auto ng-star-inserted\">\r\n\r\n\t\t\t\t\t\t\t\t<div class=\"form-group  m-form__group row\"\r\n\t\t\t\t\t\t\t\t\t formArrayName=\"destination\"\r\n\t\t\t\t\t\t\t\t\t *ngFor=\"let item of modelForm.controls['destination']['controls']; let i = index;\">\r\n\t\t\t\t\t\t\t\t\t<ng-container\r\n\t\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t\t\t<ng-container\r\n\t\t\t\t\t\t\t\t\t\t\t[formGroupName]=\"i\"\r\n\t\t\t\t\t\t\t\t\t\t>\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-lg-2 col-form-label\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tПункт назначения {{i+1}}\r\n\t\t\t\t\t\t\t\t\t\t\t</label>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-8\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" min=\"1\" placeholder=\"Адрес\" formControlName=\"value\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t   data-id=\"test\">\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-2\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"javascrtipt:;\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t   (click)=\"removeItem('destination', item[i])\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t   class=\"btn btn-danger m-btn m-btn--icon m-btn--icon-only\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"la la-remove\"></i>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"col-lg-3\"></div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"col\">\r\n\t\t\t\t\t\t\t\t\t\t<div\r\n\t\t\t\t\t\t\t\t\t\t\t(click)=\"addItem('destination')\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"btn btn btn-primary m-btn m-btn--icon\">\r\n\t\t\t\t\t\t\t\t\t<span>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"la la-plus\"></i>\r\n\t\t\t\t\t\t\t\t\t\t<span>\r\n\t\t\t\t\t\t\t\t\t\t\tДобавить пункт\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</mat-tab>\r\n\r\n\t\t\t\t<mat-tab>\r\n\t\t\t\t\t<ng-template mat-tab-label>\r\n\t\t\t\t\t\t<i class=\"mat-tab-label-icon fa fa-user\"></i>\r\n\t\t\t\t\t\tДетские кресла\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<ng-template matTabContent>\r\n\t\t\t\t\t\t<ng-container>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<div class=\"form-group form-group-last row\" id=\"kt_repeater_1\">\r\n\r\n\t\t\t\t\t\t\t\t<label class=\"col-lg-2 col-form-label\">\r\n\t\t\t\t\t\t\t\t\tСиденья\r\n\t\t\t\t\t\t\t\t</label>\r\n\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-10\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"form-group row align-items-center\"\r\n\t\t\t\t\t\t\t\t\t\t formArrayName=\"seat\"\r\n\t\t\t\t\t\t\t\t\t\t *ngFor=\"let item of modelForm.controls['seat']['controls']; let i = index;\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-container\r\n\t\t\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t\t\t\t<ng-container\r\n\t\t\t\t\t\t\t\t\t\t\t\t[formGroupName]=\"i\"\r\n\t\t\t\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"kt-form__group--inline\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"kt-form__label\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label>Возраст:</label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"kt-form__control\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" min=\"1\" placeholder=\"Возраст\" formControlName=\"age\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-id=\"test\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"kt-form__group--inline\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"kt-form__label\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label>Вес:</label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"kt-form__control\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" min=\"1\" placeholder=\"Вес\" formControlName=\"weight\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t   data-id=\"test\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"javascrtipt:;\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t   (click)=\"removeItem('seat', item[i])\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t   class=\"btn-sm btn btn-label-danger btn-bold\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"la la-trash-o\"></i>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tУдалить\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t<div class=\"form-group form-group-last row\">\r\n\t\t\t\t\t\t\t\t<label class=\"col-lg-2 col-form-label\"></label>\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-4\">\r\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\"  (click)=\"addItem('seat')\" data-repeater-create=\"\" class=\"btn btn-bold btn-sm btn-label-brand\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"la la-plus\"></i> Добавить\r\n\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</mat-tab>\r\n\t\t\t</mat-tab-group>\r\n\t\t</form>\r\n\t</kt-portlet-body>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/orders/orders-list/orders-list.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/orders/orders-list/orders-list.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<kt-portlet>\r\n\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n\t\t<div class=\"kt-portlet__head-label\">\r\n\t\t\t<h3 class=\"kt-portlet__head-title\">\r\n\t\t\t\t{{title}}\r\n\t\t\t</h3>\r\n\t\t</div>\r\n\t\t<div class=\"kt-portlet__head-toolbar\">\r\n\t\t\t<div class=\"kt-portlet__head-wrapper\">\r\n\t\t\t\t<div class=\"dropdown dropdown-inline\">\r\n\t\t\t\t\t<button  (click)=\"redirectCreateModel();\" mat-raised-button>Добавить</button>&nbsp;\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body\">\r\n\t\t<div class=\"kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10\">\r\n\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-xl-8 order-2 order-xl-1\">\r\n\t\t\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t\t\t<div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--left\">\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Поиск...\" id=\"generalSearch\">\r\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--left\">\r\n\t\t\t\t\t\t\t<span><i class=\"la la-search\"></i></span>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body kt-portlet__body--fit\">\r\n\t\t<div class=\"my_datatable\" [id]=\"baseUrl\"></div>\r\n\t</div>\r\n</kt-portlet>\r\n");

/***/ }),

/***/ "./src/app/core/car/resolvers/car_list.ts":
/*!************************************************!*\
  !*** ./src/app/core/car/resolvers/car_list.ts ***!
  \************************************************/
/*! exports provided: CarListResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarListResolver", function() { return CarListResolver; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _models_car__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_models/car */ "./src/app/core/car/_models/car.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
/**
 * Created by Алексей on 09.02.2018.
 */





let CarListResolver = class CarListResolver {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    resolve(route, state) {
        return this.http.get('/api/cars?all=true').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((res) => res['models'].map((car) => {
            let car2 = new _models_car__WEBPACK_IMPORTED_MODULE_4__["Car"](car);
            return car2;
        })));
    }
};
CarListResolver.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] }
];
CarListResolver = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
], CarListResolver);



/***/ }),

/***/ "./src/app/core/client/_resolvers/clients-list.ts":
/*!********************************************************!*\
  !*** ./src/app/core/client/_resolvers/clients-list.ts ***!
  \********************************************************/
/*! exports provided: ClientsListResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientsListResolver", function() { return ClientsListResolver; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _models_client__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/client */ "./src/app/core/client/models/client.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
/**
 * Created by Алексей on 09.02.2018.
 */





let ClientsListResolver = class ClientsListResolver {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    resolve(route, state) {
        return this.http.get('/api/clients').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((res) => res['models'].map((model) => new _models_client__WEBPACK_IMPORTED_MODULE_4__["Client"](model))));
    }
};
ClientsListResolver.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] }
];
ClientsListResolver = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
], ClientsListResolver);



/***/ }),

/***/ "./src/app/core/client/models/client.ts":
/*!**********************************************!*\
  !*** ./src/app/core/client/models/client.ts ***!
  \**********************************************/
/*! exports provided: Client */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Client", function() { return Client; });
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
class Client {
    constructor(Client) {
        if (Client) {
            Object.assign(this, Client);
            this.label = this.first_name + ' ' + this.last_name;
            this.value = this.id;
        }
    }
    getLabel() {
        return this.first_name + ' ' + this.last_name;
    }
}


/***/ }),

/***/ "./src/app/core/driver/_models/driver.ts":
/*!***********************************************!*\
  !*** ./src/app/core/driver/_models/driver.ts ***!
  \***********************************************/
/*! exports provided: Driver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Driver", function() { return Driver; });
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
class Driver {
    getParam(param) {
        return this[param];
    }
    getLabel() {
        return this.first_name + ' ' + this.last_name;
    }
    constructor(Driver) {
        if (Driver) {
            Object.assign(this, Driver);
            this.label = this.first_name + ' ' + this.last_name;
            this.value = this.id;
        }
    }
}


/***/ }),

/***/ "./src/app/core/driver/resolvers/driver_list.ts":
/*!******************************************************!*\
  !*** ./src/app/core/driver/resolvers/driver_list.ts ***!
  \******************************************************/
/*! exports provided: DriverListResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DriverListResolver", function() { return DriverListResolver; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _models_driver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_models/driver */ "./src/app/core/driver/_models/driver.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
/**
 * Created by Алексей on 09.02.2018.
 */





let DriverListResolver = class DriverListResolver {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    resolve(route, state) {
        return this.http.get('/api/drivers?all=true').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((res) => res['models'].map((driver) => {
            let models = new _models_driver__WEBPACK_IMPORTED_MODULE_4__["Driver"](driver);
            return models;
        })));
    }
};
DriverListResolver.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] }
];
DriverListResolver = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
], DriverListResolver);



/***/ }),

/***/ "./src/app/core/order/_resolvers/orders.ts":
/*!*************************************************!*\
  !*** ./src/app/core/order/_resolvers/orders.ts ***!
  \*************************************************/
/*! exports provided: OrderEditResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderEditResolver", function() { return OrderEditResolver; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
/**
 * Created by Алексей on 09.02.2018.
 */



let OrderEditResolver = class OrderEditResolver {
    constructor(http, activatedRoute) {
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    resolve(route, state) {
        if (route.queryParams.id) {
            return this.http.get('/api/order/' + route.queryParams.id);
        }
        else {
            return;
        }
    }
};
OrderEditResolver.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"] }
];
OrderEditResolver = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"]])
], OrderEditResolver);



/***/ }),

/***/ "./src/app/views/pages/orders/orders-edit/orders-edit.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/views/pages/orders/orders-edit/orders-edit.component.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mat-tab-error {\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcGFnZXMvb3JkZXJzL29yZGVycy1lZGl0L0M6XFxVc2Vyc1xcQWxleGV5XFxJZGVhUHJvamVjdHNcXHRyYW5zZmVyL3NyY1xcYXBwXFx2aWV3c1xccGFnZXNcXG9yZGVyc1xcb3JkZXJzLWVkaXRcXG9yZGVycy1lZGl0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC92aWV3cy9wYWdlcy9vcmRlcnMvb3JkZXJzLWVkaXQvb3JkZXJzLWVkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxVQUFBO0FDQ0QiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9wYWdlcy9vcmRlcnMvb3JkZXJzLWVkaXQvb3JkZXJzLWVkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LXRhYi1lcnJvciB7XHJcblx0Y29sb3I6IHJlZDtcclxufVxyXG4iLCIubWF0LXRhYi1lcnJvciB7XG4gIGNvbG9yOiByZWQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/views/pages/orders/orders-edit/orders-edit.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/views/pages/orders/orders-edit/orders-edit.component.ts ***!
  \*************************************************************************/
/*! exports provided: OrdersEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersEditComponent", function() { return OrdersEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







let OrdersEditComponent = class OrdersEditComponent {
    constructor(subheader, userModel, router, titleService, http, Fb, cdr, route) {
        this.subheader = subheader;
        this.userModel = userModel;
        this.router = router;
        this.titleService = titleService;
        this.http = http;
        this.Fb = Fb;
        this.cdr = cdr;
        this.route = route;
        this.config = {
            orientation: 'horizontal'
        };
        this.selectedTab = 0;
        this.title = "";
        this.baseUrl = 'order';
        this.hasFormErrors = false;
        this.model = {};
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_5__["User"]();
        this.roles = {};
        this.errors = [];
        this.clientsList = [];
        this.driverList = [];
        this.carList = [];
        this.destinations = [];
        this.seats = [];
        this.hasFormError = false;
        this.submitted = false;
    }
    ngOnInit() {
        this.createForm();
        this.initFormValues();
    }
    ngAfterViewInit() {
        this.setInputMasks();
    }
    onTabChange(e) {
        this.cdr.detectChanges();
    }
    setInputMasks() {
        $('#phone').inputmask('mask', {
            'mask': '+7 (999) 999-9999',
            oncomplete: (e) => {
                console.log(e);
                this.modelForm.controls['phone'].setValue(e.target.value);
            },
        });
    }
    setTitles() {
        let breadcrumbs = [{
                title: 'Список заказов',
                page: '/orders'
            }];
        this.subheader.setTitle("Заказы");
        if (this.state == 'edit') {
            this.title = "Редактирование заказа";
            breadcrumbs.push({
                title: 'Редактирование',
                page: '/orders/edit?id=' + this.modelForm.controls['id'].value,
            });
        }
        else {
            this.title = "Добавление заказа";
            breadcrumbs.push({
                title: 'Добавление',
                page: '/orders/create'
            });
        }
        this.subheader.setBreadcrumbs(breadcrumbs);
        this.titleService.setTitle(this.title);
    }
    getComponentTitle() {
        return this.title;
    }
    reset() {
        this.modelForm.reset();
    }
    onSubmit(redirect) {
        let submitModel = this.modelForm.value;
        submitModel.departure_time = moment(submitModel.departure_time).toISOString();
        if (this.state == 'edit') {
            this.http.put('api/order', this.modelForm.value).subscribe((data) => {
                if (redirect)
                    this.router.navigate(['/orders']);
                this.hasFormErrors = false;
            }, (data) => {
                this.errors = data.error;
                this.hasFormErrors = true;
                this.cdr.detectChanges();
            });
        }
        else {
            this.http.post('api/order', this.modelForm.value).subscribe((data) => {
                if (redirect)
                    this.router.navigate(['/orders']);
                this.hasFormErrors = false;
            }, (data) => {
                this.errors = data.error;
                this.hasFormErrors = true;
                this.cdr.detectChanges();
            });
        }
    }
    initFormValues() {
        this.route.queryParams.subscribe((params) => {
            const id = params['id'];
            if (id > 0) {
                this.state = 'edit';
            }
            else {
                this.state = 'create';
            }
            this.route.data
                .subscribe((data) => {
                if (id > 0) {
                    if (data['model']) {
                        this.modelForm.patchValue(data['model'], {
                            emitEvent: false,
                            onlySelf: true
                        });
                        this.setUtcDate(data);
                        this.setValues(data);
                    }
                }
                else {
                    this.modelForm.addControl('destination', this.Fb.array([this.createItem('destination')]));
                    this.modelForm.addControl('seat', this.Fb.array([this.createItem('seat')]));
                }
                if (data['clients']) {
                    this.clientsList = data['clients'];
                }
                if (data['drivers']) {
                    this.driverList = data['drivers'];
                }
                if (data['cars']) {
                    this.carList = data['cars'];
                }
                console.log(data);
            });
            this.setTitles();
        });
    }
    setValues(data) {
        let seats = [];
        let destinations = [];
        if (data['model']['seats'].length) {
            let ad = seats = data['model']['seats'];
            data['model']['seats'].forEach((item) => {
                this.addItem('seat', item);
            });
        }
        else {
            this.addItem('seat');
        }
        if (data['model']['destinations'].length) {
            data['model']['destinations'].forEach((item) => {
                this.addItem('destination', item);
            });
        }
        else {
            this.addItem('destination');
        }
    }
    setUtcDate(data) {
        var stillUtc = moment.utc(data['model']['departure_time']).toDate();
        var local = moment(stillUtc).local().format('YYYY-MM-DD[T]HH:mm');
        this.modelForm.controls['departure_time'].setValue(local);
    }
    createForm() {
        this.modelForm = this.Fb.group({
            id: '',
            departure_time: [''],
            origin: [''],
            passengers_count: [1],
            baggage_count: [0],
            payment_type: [0],
            conditioner: [0],
            bank_card_payment: [0],
            client_id: [null],
            driver_id: [null],
            car_id: [null],
            comment: [''],
            status: [0],
        });
    }
    createItem(name, data = null) {
        let thisFormGroup;
        if (name === 'destination') {
            thisFormGroup = this.Fb.group({
                value: data ? data['address'] + '' : ''
            });
        }
        else if (name === 'seat') {
            thisFormGroup = this.Fb.group({
                weight: data ? data['weight'] + '' : '',
                age: data ? data['age'] + '' : ''
            });
        }
        return thisFormGroup;
    }
    addItem(name, data = null) {
        if (!this.modelForm.controls[name]) {
            this.modelForm.addControl(name, this.Fb.array([this.createItem(name, data)]));
        }
        else {
            let items = this.modelForm.get(name);
            items.push(this.createItem(name, data));
        }
    }
    removeItem(name, id) {
        let items = this.modelForm.get(name);
        if (items.length > 1)
            items.removeAt(id);
    }
    formConfig() {
        return [
            {
                field: 'departure_time',
                label: 'Дата и время выезда',
                type: 'datetime-local',
                permission: 'manage_' + this.baseUrl + 's',
            },
            {
                field: 'origin',
                label: 'Место отправления',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's',
            },
            {
                field: 'passengers_count',
                label: 'Кол-во пассажиров',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's',
            },
            {
                field: 'baggage_count',
                label: 'Кол-во багажа',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's',
            },
            {
                field: 'payment_type',
                label: 'Способ оплаты',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [{
                        value: 0,
                        label: 'Наличные',
                    }, {
                        value: 1,
                        label: 'Банковская карта'
                    }, {
                        value: 2,
                        label: 'Баланс'
                    },
                ],
                nullValue: false
            },
            {
                field: 'conditioner',
                label: 'Кондиционер',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [{
                        value: 0,
                        label: 'Нет',
                    }, {
                        value: 1,
                        label: 'Да'
                    }
                ],
                nullValue: false
            },
            {
                field: 'client_id',
                label: 'Клиент',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: this.clientsList,
                nullValue: true
            },
            {
                field: 'driver_id',
                label: 'Водитель',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: this.driverList,
                nullValue: true
            },
            {
                field: 'car_id',
                label: 'Автомобиль',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: this.carList,
                nullValue: true
            },
            {
                field: 'comment',
                label: 'Комментарий',
                type: 'textarea',
                permission: 'manage_' + this.baseUrl + 's',
            },
            {
                field: 'status',
                label: 'Статус',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [{
                        value: 0,
                        label: 'В обработке',
                    }, {
                        value: 1,
                        label: 'Поиск такси'
                    }, {
                        value: 2,
                        label: 'Принят'
                    }, {
                        value: 3,
                        label: 'Едет к клиенту'
                    }, {
                        value: 4,
                        label: 'Ожидает клиента'
                    }, {
                        value: 5,
                        label: 'Такси задерживается'
                    }, {
                        value: 6,
                        label: 'Нет клиента'
                    }, {
                        value: 7,
                        label: 'Посадка'
                    }, {
                        value: 8,
                        label: 'В рейсе'
                    }, {
                        value: 9,
                        label: 'Отказ клиента'
                    }, {
                        value: 10,
                        label: 'Предварительный заказ'
                    }, {
                        value: 11,
                        label: 'Отказ водителя'
                    }, {
                        value: 12,
                        label: 'Выполнен'
                    }, {
                        value: 13,
                        label: 'Отменен'
                    }
                ],
                nullValue: false
            },
        ];
    }
    isFieldVisible(permission) {
        return this.userModel.hasPermissionTo(permission);
    }
    trackByFn(index, item) {
        return index;
    }
    getErrorMessage(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
        else {
            return '';
        }
    }
    hasError(field) {
        let classList = {
            'is-invalid': this.errors[field],
            'form-control': true,
        };
        return classList;
    }
};
OrdersEditComponent.ctorParameters = () => [
    { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"] },
    { type: _core_auth__WEBPACK_IMPORTED_MODULE_5__["User"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
OrdersEditComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'kt-orders-edit',
        template: __importDefault(__webpack_require__(/*! raw-loader!./orders-edit.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/orders/orders-edit/orders-edit.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./orders-edit.component.scss */ "./src/app/views/pages/orders/orders-edit/orders-edit.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"],
        _core_auth__WEBPACK_IMPORTED_MODULE_5__["User"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], OrdersEditComponent);



/***/ }),

/***/ "./src/app/views/pages/orders/orders-list/orders-list.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/views/pages/orders/orders-list/orders-list.component.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL29yZGVycy9vcmRlcnMtbGlzdC9vcmRlcnMtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/pages/orders/orders-list/orders-list.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/views/pages/orders/orders-list/orders-list.component.ts ***!
  \*************************************************************************/
/*! exports provided: OrdersListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersListComponent", function() { return OrdersListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _core_auth_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../core/auth/_services */ "./src/app/core/auth/_services/index.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};








let OrdersListComponent = class OrdersListComponent {
    constructor(subheader, cdr, auth, router, titleService, store, http) {
        this.subheader = subheader;
        this.cdr = cdr;
        this.auth = auth;
        this.router = router;
        this.titleService = titleService;
        this.store = store;
        this.http = http;
        this.title = 'Заказы';
        this.baseUrl = 'order';
        this.user = new _core_auth__WEBPACK_IMPORTED_MODULE_6__["User"]();
    }
    ngOnInit() {
        this.subheader.setBreadcrumbs([
            {
                title: 'Список заказов',
                url: 'users'
            }
        ]);
        this.subheader.setTitle("Заказы");
        this.cdr.detectChanges();
        this.titleService.setTitle("Заказы");
        this.initDatatable();
    }
    redirectCreateModel() {
        this.router.navigate(['/orders/edit']);
    }
    initDatatable() {
        const token = this.auth.getUserToken();
        this.datatable = $('#' + this.baseUrl).KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: 'api/' + this.baseUrl + 's',
                        headers: {
                            'Authorization': token,
                        },
                        map: (raw) => {
                            // sample data mapping
                            var dataSet = raw;
                            console.log(raw);
                            if (typeof raw.models !== 'undefined') {
                                dataSet = raw.models;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },
            // column sorting
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            // columns definition
            columns: this.getColumnsByRole(),
        });
        this.datatable.on('kt-datatable--on-layout-updated', () => {
            setTimeout(() => {
                $('.editModel').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    this.edit(id);
                });
                $('.deleteModel').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    if (confirm('Вы уверены что хотите удалть?')) {
                        this.delete(id);
                    }
                });
                $('.toggleStatus').click((e) => {
                    var id = $(e.currentTarget).attr('data-id');
                    this.toggleStatus(id);
                });
            });
        });
        this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
            this.router.navigate(['/auth/login']);
        });
    }
    toggleStatus(id) {
        this.http.post('/api/' + this.baseUrl + '/status/' + id, {}).subscribe(() => {
            this.datatable.reload();
        });
    }
    edit(id) {
        this.router.navigate(['/' + this.baseUrl + 's/edit',], { queryParams: { id: id } });
    }
    delete(id) {
        this.http.delete('/api/' + this.baseUrl + '/' + id).subscribe(() => {
            this.datatable.reload();
        });
    }
    getColumnsByRole() {
        var config = [];
        if (this.user.hasPermissionTo('manage_users')) {
            config = [{
                    field: 'id',
                    title: '#',
                    sortable: 'asc',
                    width: 40,
                    type: 'number',
                    selector: false,
                    textAlign: 'center',
                    autoHide: !1,
                }, {
                    field: 'departure_time',
                    title: 'Дата отправления',
                    autoHide: !1,
                    template: function (row, index, datatable) {
                        return moment.utc(row['departure_time']).local().format('DD.MM.YYYY HH:mm');
                    }
                }, {
                    field: 'origin',
                    title: 'Место отправления',
                    autoHide: !1
                }, {
                    field: 'destination',
                    title: 'Место назначения',
                    autoHide: !1,
                    template: function (row, index, datatable) {
                        var destination = '';
                        var count = 0;
                        if (row['destinations']) {
                            row['destinations'].forEach((item) => {
                                if (count > 0) {
                                    destination += ', ';
                                }
                                destination += item['address'];
                                count++;
                            });
                        }
                        return destination;
                    }
                }, {
                    field: 'car',
                    title: 'Автомобиль',
                    autoHide: !1,
                    template: function (row, index, datatable) {
                        if (row.car)
                            return row.car.mark + ' ' + row.car.model + ' ' + row.car.number + ' ' + row.car.year + ' ' + row.car.color;
                        else
                            return;
                    }
                }, {
                    field: 'driver',
                    title: 'Водитель',
                    autoHide: !1,
                    template: function (row, index, datatable) {
                        if (row.driver)
                            return row.driver.first_name + ' ' + row.driver.last_name + ' ' + row.driver.phone;
                        else
                            return;
                    }
                }, {
                    field: 'status',
                    title: 'Статус',
                    autoHide: !1,
                    template: function (row, index, datatable) {
                        if (row.status != undefined) {
                            var labels = [{
                                    value: 0,
                                    label: 'В обработке',
                                }, {
                                    value: 1,
                                    label: 'Поиск такси'
                                }, {
                                    value: 2,
                                    label: 'Принят'
                                }, {
                                    value: 3,
                                    label: 'Едет к клиенту'
                                }, {
                                    value: 4,
                                    label: 'Ожидает клиента'
                                }, {
                                    value: 5,
                                    label: 'Такси задерживается'
                                }, {
                                    value: 6,
                                    label: 'Нет клиента'
                                }, {
                                    value: 7,
                                    label: 'Посадка'
                                }, {
                                    value: 8,
                                    label: 'В рейсе'
                                }, {
                                    value: 9,
                                    label: 'Отказ клиента'
                                }, {
                                    value: 10,
                                    label: 'Предварительный заказ'
                                }, {
                                    value: 11,
                                    label: 'Отказ водителя'
                                }, {
                                    value: 12,
                                    label: 'Выполнен'
                                }, {
                                    value: 13,
                                    label: 'Отменен'
                                }
                            ];
                            return labels.find(element => element.value === row.status).label;
                        }
                        else
                            return "Статус неизвестен";
                    }
                }, {
                    field: 'comment',
                    title: 'Комментарий',
                    autoHide: !1
                },];
        }
        config.push({
            field: 'Actions',
            title: 'Действия',
            sortable: false,
            overflow: 'visible',
            textAlign: 'center',
            autoHide: !1,
            template: (row, index, datatable) => {
                let visible = this.user.hasPermissionTo('manage_' + this.baseUrl + 's');
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                let result = '<button href="javascript:;" data-id=' + row['id'] + ' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
                        <i class="la la-edit"></i>\
                    </button>';
                if (visible) {
                    result +=
                        '<button href="javascript:;" data-id=' + row['id'] + ' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
                        <i class="la la-trash"></i>\
                    </button>';
                }
                return result;
            },
        });
        return config;
    }
};
OrdersListComponent.ctorParameters = () => [
    { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] },
    { type: _core_auth_services__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"] }
];
OrdersListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'kt-orders-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./orders-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/pages/orders/orders-list/orders-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./orders-list.component.scss */ "./src/app/views/pages/orders/orders-list/orders-list.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
        _core_auth_services__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["Title"],
        _ngrx_store__WEBPACK_IMPORTED_MODULE_1__["Store"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"]])
], OrdersListComponent);



/***/ }),

/***/ "./src/app/views/pages/orders/orders.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/pages/orders/orders.module.ts ***!
  \*****************************************************/
/*! exports provided: OrdersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersModule", function() { return OrdersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _orders_list_orders_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./orders-list/orders-list.component */ "./src/app/views/pages/orders/orders-list/orders-list.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/auth/_interceptor/interceptor */ "./src/app/core/auth/_interceptor/interceptor.ts");
/* harmony import */ var _core_order_resolvers_orders__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/order/_resolvers/orders */ "./src/app/core/order/_resolvers/orders.ts");
/* harmony import */ var _orders_edit_orders_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./orders-edit/orders-edit.component */ "./src/app/views/pages/orders/orders-edit/orders-edit.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _partials_partials_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../partials/partials.module */ "./src/app/views/partials/partials.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _core_client_resolvers_clients_list__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../core/client/_resolvers/clients-list */ "./src/app/core/client/_resolvers/clients-list.ts");
/* harmony import */ var _core_driver_resolvers_driver_list__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../core/driver/resolvers/driver_list */ "./src/app/core/driver/resolvers/driver_list.ts");
/* harmony import */ var _core_car_resolvers_car_list__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../core/car/resolvers/car_list */ "./src/app/core/car/resolvers/car_list.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};















const routes = [
    // { path: '', redirectTo: 'list'},
    { path: '', component: _orders_list_orders_list_component__WEBPACK_IMPORTED_MODULE_2__["OrdersListComponent"] },
    { path: 'edit', component: _orders_edit_orders_edit_component__WEBPACK_IMPORTED_MODULE_7__["OrdersEditComponent"], resolve: {
            model: _core_order_resolvers_orders__WEBPACK_IMPORTED_MODULE_6__["OrderEditResolver"],
            clients: _core_client_resolvers_clients_list__WEBPACK_IMPORTED_MODULE_12__["ClientsListResolver"],
            drivers: _core_driver_resolvers_driver_list__WEBPACK_IMPORTED_MODULE_13__["DriverListResolver"],
            cars: _core_car_resolvers_car_list__WEBPACK_IMPORTED_MODULE_14__["CarListResolver"]
        } }
];
let OrdersModule = class OrdersModule {
};
OrdersModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        declarations: [_orders_list_orders_list_component__WEBPACK_IMPORTED_MODULE_2__["OrdersListComponent"], _orders_edit_orders_edit_component__WEBPACK_IMPORTED_MODULE_7__["OrdersEditComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _partials_partials_module__WEBPACK_IMPORTED_MODULE_10__["PartialsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_11__["TranslateModule"].forChild(),
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatNativeDateModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressSpinnerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogModule"]
        ],
        providers: [
            _core_order_resolvers_orders__WEBPACK_IMPORTED_MODULE_6__["OrderEditResolver"],
            _core_client_resolvers_clients_list__WEBPACK_IMPORTED_MODULE_12__["ClientsListResolver"],
            _core_driver_resolvers_driver_list__WEBPACK_IMPORTED_MODULE_13__["DriverListResolver"],
            _core_car_resolvers_car_list__WEBPACK_IMPORTED_MODULE_14__["CarListResolver"],
            {
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                useClass: _core_auth_interceptor_interceptor__WEBPACK_IMPORTED_MODULE_5__["JwtInterceptor"],
                multi: true
            },
        ]
    })
], OrdersModule);



/***/ })

}]);
//# sourceMappingURL=app-views-pages-orders-orders-module-es2015.js.map