# PHP 7.1, MYSQL 5.7б ANGULAR 7, LARAVEL 5.6 

# Композер и миграции

Перейти в папку api/  
0. Настроить данные для базы данных в файле .env пример - .env.example
1. Выполнить "php composer.phar install" или "composer install"  
2. Выполнить "php artisan migrate"
3. Выполнить "php artisan db:seed"
4. Прописать ключи в файле api/config/api.php
5. При каждом обновлении выполнять php artisan migrate, php artisan db:seed --class=RefreshSeeder
