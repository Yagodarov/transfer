#!/usr/bin/env bash
rm -f ./*.woff
rm -f ./*.woff2
rm -f ./*.eot
rm -f ./*.ttf
rm -f ./*.svg
rm -f ./*.css
rm -f ./*.js
rm -f ./*.png
rm -f ./*.html
rm -f ./*.txt
nice -n 20 ng build --sourceMap=false --optimization=false
cp -R dist/metronic/* .
rm -rf dist
echo 'Done!'
