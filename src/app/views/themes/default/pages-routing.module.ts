// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../../core/auth';
import { NgxPermissionsGuard } from 'ngx-permissions';
import {MyPageComponent} from "../../pages/my-page/my-page.component";
import {SettingsComponent} from "../../pages/settings/settings.component";
// My Component

const routes: Routes = [
	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'my-page', // <= Page URL
				component: MyPageComponent // <= Page component registration
			},{
				path: 'users', // <= Page URL
				loadChildren: () => import('app/views/pages/users/users.module').then(m => m.UsersModule)
		   	},{
				path: 'clients', // <= Page URL
				loadChildren: () => import('app/views/pages/clients/clients.module').then(m => m.ClientsModule)
		   	},{
				path: 'orders', // <= Page URL
				loadChildren: () => import('app/views/pages/orders/orders.module').then(m => m.OrdersModule)
		   	},{
				path: 'towns', // <= Page URL
				loadChildren: () => import('app/views/pages/towns/towns.module').then(m => m.TownsModule)
		   	},{
				path: 'cars', // <= Page URL
				loadChildren: () => import('app/views/pages/cars/cars.module').then(m => m.CarsModule)
		   	},{
				path: 'drivers', // <= Page URL
				loadChildren: () => import('app/views/pages/drivers/drivers.module').then(m => m.DriversModule)
		   	},{
				path: 'services', // <= Page URL
				loadChildren: () => import('app/views/pages/services/services.module').then(m => m.ServicesModule)
		   	},{
				path: 'settings', // <= Page URL
				loadChildren: () => import('app/views/pages/settings/settings.module').then(m => m.SettingsModule)
		   	},
			{
				path: 'material',
				loadChildren: () => import('app/views/pages/material/material.module').then(m => m.MaterialModule)
			},
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					'type': 'error-v6',
					'code': 403,
					'title': '403... Access forbidden',
					'desc': 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator'
				}
			},
			{path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'users', pathMatch: 'full'},
			{path: '**', redirectTo: 'users', pathMatch: 'full'}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
