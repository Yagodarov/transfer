// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
// NgBootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { MyPageComponent } from './my-page/my-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UsersModule} from "./users/users.module";
import {DashboardModule} from "./dashboard/dashboard.module";
import {DriversModule} from "./drivers/drivers.module";
@NgModule({
	declarations: [MyPageComponent],
	exports: [],
	imports: [
		ReactiveFormsModule,
		FormsModule,

		CommonModule,
		HttpClientModule,
		NgbModule,
		CoreModule,
		PartialsModule,
		DriversModule,
		UsersModule,
		DashboardModule
	],
	providers: []
})
export class PagesModule {
}
