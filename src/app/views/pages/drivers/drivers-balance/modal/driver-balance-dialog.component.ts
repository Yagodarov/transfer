import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, ɵEMPTY_ARRAY} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpClient} from "@angular/common/http";
export interface DialogData {
	type: string;
	driver_id: string;
}
@Component({
	selector: 'kt-driver-balance-dialog',
	templateUrl: './driver-balance-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DriverBalanceDialogComponent {
	form = {
		value : '0',
		comment : '',
		driver_id : '',
		type : ''
	};
	errors : any;
	constructor(public dialogRef: MatDialogRef<DriverBalanceDialogComponent>,
				@Inject(MAT_DIALOG_DATA) public data: DialogData,
				private cdr : ChangeDetectorRef,
				private htpp: HttpClient) {
	}

	getTitle() {
		if (this.data.type == '0') {
			return "Спиисывание баланса";
		} else if (this.data.type == '1'){
			return "Пополнение баланса";
		} else {
			return "Неихвестно";
		}
	}

	submitDialog() {
		this.form.driver_id = this.data.driver_id;
		this.form.type = this.data.type;
		this.htpp.post('api/driver_balance', this.form).subscribe(() => {
			this.dialogRef.close(true);
		}, (error) => {
			this.cdr.detectChanges();
			this.errors = error['error'];
		});
	}

	closeDialog() {
		this.dialogRef.close(false);
	}
}
