import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {AuthService} from "../../../../core/auth/_services";
import {User} from "../../../../core/auth";
import {MatDialog} from "@angular/material";
import {DriverBalanceDialogComponent} from "./modal/driver-balance-dialog.component";
import {HttpClient} from "@angular/common/http";

declare var moment : any;
@Component({
  selector: 'kt-driver-balance',
  templateUrl: './driver-balance.component.html',
  styleUrls: ['./driver-balance.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DriverBalanceComponent implements OnInit {
	@Input() data : any;
	total : string = '0';
	baseUrl = 'driver_balance';
	datatable : any;
  constructor(
  	private auth : AuthService,
  	private http : HttpClient,
	private user : User,
	private cdr : ChangeDetectorRef,
	public dialog: MatDialog
  ) { }

  ngOnInit() {
  	this.initDatatable();
  }

  getTotal() {
	  return this.data.total + ' руб.';
  }

  openDialog(type) {
	  const dialogRef = this.dialog.open(DriverBalanceDialogComponent, {
		  width: '350px',
		  data: {
			  type: type,
			  driver_id: this.data.id
		  }
	  });

	  dialogRef.afterClosed().subscribe(result => {
	  	if (result) {
	  		this.setBalance();
	  		this.datatable.reload();
		}
	  });
  }

  setBalance() {
  	this.http.get('api/driver/balance/'+this.data.id).subscribe((data) => {
		this.data.total = data['total'];
		this.cdr.detectChanges();
	});
  }

	initDatatable() {
		const token = this.auth.getUserToken();
		this.datatable = $('#driver_balance').KTDatatable({
			data: {
				// autoColumns: true,
				type: 'remote',
				source: {
					read: {
						method: 'GET',
						url: 'api/'+this.baseUrl+'s/'+this.data.id,
						headers: {
							'Authorization': token,
						},
						map: (raw) => {
							// sample data mapping
							var dataSet = raw;
							console.log(raw);
							if (typeof raw.models !== 'undefined') {
								dataSet = raw.models;
							}
							return dataSet;
						},
					},
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},

			// layout definition
			layout: {
				scroll: false,
				footer: false,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: this.getColumnsByRole(),

		});
	}

	getColumnsByRole() {
		var config = [];
		if (this.user.hasPermissionTo('manage_'+this.baseUrl+'s')) {
			config = [{
				field: 'id',
				title: '#',
				sortable: 'asc',
				width: 40,
				type: 'number',
				selector: false,
				textAlign: 'center',
				autoHide: !1,
			}, {
				field: 'type',
				title: 'Тип',
				autoHide: !1,
				width: 100,
				textAlign: 'center',
				template: function (row, index, datatable) {
					if (row['type'] == 1) {
						return 'Пополнение';
					} else if (row['type'] == 0) {
						return 'Списание';
					} else {
						return 'Неизвестный статус';
					}
				}
			}, {
				field: 'value',
				title: 'Сумма',
				autoHide: !1,
				width: 100,
				textAlign: 'center',
				template: function (row, index, datatable) {
					return row['value'] + ' руб.';
				}
			},{
				field: 'created_at',
				title: 'Дата',
				autoHide: !1,
				width: 100,
				textAlign: 'center',
				template: function (row, index, datatable) {
					return moment.utc(row['created_at']).local().format('DD.MM.YYYY HH:mm');
				}
			},{
				field: 'comment',
				title: 'Комментарий',
				autoHide: !1,
				width: 100,
				textAlign: 'center'
			},
			];
		}
		// config.push({
		// 	field: 'Actions',
		// 	title: 'Действия',
		// 	sortable: false,
		// 	overflow: 'visible',
		// 	textAlign: 'center',
		// 	autoHide: !1,
		// 	width: 100,
		// 	template: (row, index, datatable) => {
		// 		let visible = this.user.hasPermissionTo('manage_'+this.baseUrl+'s');
		// 		var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
		// 		let result =  '<button href="javascript:;" data-id='+row['id']+' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
         //                <i class="la la-edit"></i>\
         //            </button>';
		// 		if (visible) {
		// 			result +=
		// 				'<button href="javascript:;" data-id='+row['id']+' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
         //                <i class="la la-trash"></i>\
         //            </button>'
		// 		}
		// 		return result;
		// 	},
		// });
		return config;
	}
}
