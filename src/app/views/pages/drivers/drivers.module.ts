import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriversListComponent } from './drivers-list/drivers-list.component';
import { DriversEditComponent } from './drivers-edit/drivers-edit.component'
import {RouterModule, Routes} from "@angular/router";
import {DriverEditResolver} from "../../../core/driver/resolvers/service";
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatIconModule,
	MatInputModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule, MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSnackBarModule,
	MatSortModule,
	MatTableModule,
	MatTabsModule,
	MatTooltipModule
} from "@angular/material";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {PartialsModule} from "../../partials/partials.module";
import {JwtInterceptor} from "../../../core/auth/_interceptor/interceptor";
import {TownsListResolver} from "../../../core/town/resolvers/towns";
import {CarsResolver} from "../../../core/car/resolvers/cars";
import { DriverBalanceComponent } from './drivers-balance/driver-balance.component';
import {DriverBalanceDialogComponent} from "./drivers-balance/modal/driver-balance-dialog.component";
const routes: Routes = [
	// { path: '', redirectTo: 'list'},
	{ path: '', component : DriversListComponent },
	{ path: 'edit', component : DriversEditComponent , resolve : {
		model : DriverEditResolver,
		towns : TownsListResolver,
		cars : CarsResolver
	}}
];
@NgModule({
  declarations: [DriversListComponent, DriversEditComponent, DriverBalanceComponent, DriverBalanceDialogComponent],
	imports: [
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatExpansionModule,
		MatTabsModule,
		MatTooltipModule,
		MatDialogModule
	],
	entryComponents: [
		DriverBalanceDialogComponent
	],
	providers: [
		DriverEditResolver,
		TownsListResolver,
		CarsResolver,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},
	],
})
export class DriversModule { }
