import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from '../../../../core/auth';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {HttpClient} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SubheaderService} from '../../../../core/_base/layout';
import {Observable} from 'rxjs';
import {log} from "util";

@Component({
	selector: 'kt-drivers-edit',
	templateUrl: './drivers-edit.component.html',
	styleUrls: ['./drivers-edit.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DriversEditComponent implements OnInit, AfterViewInit {
	config = {
		orientation: 'horizontal'
	};
	balanceData: any;
	selectedTab = 0;
	loading$: Observable<boolean>;
	title = '';
	baseUrl = 'driver';
	state: string;
	hasFormErrors: boolean = false;
	model: any = {};
	modelForm: FormGroup;
	townsList = [];
	carsList = [];
	user = new User();
	roles: any = {};
	errors: any = [];
	submitted = false;

	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private titleService: Title,
		private http: HttpClient,
		private Fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
		this.initWizard();
	}

	ngAfterViewInit() {
		this.setInputMasks();
		this.setSelect2();
	}

	onTabChange(event) {
		console.log(this.selectedTab);
		if (event.index === 0) {
			this.setSelect2();
		}
	}

	getUserId() {
		return this.modelForm.controls['id'].value;
	}

	setSelect2() {
		$('#cars').val(this.modelForm.controls['cars'].value);
		$('#cars').select2();
		$('#cars').on('select2:select', (e) => {
			this.modelForm.controls['cars'].setValue($('#cars').val());
		});
		$('#cars').on('select2:unselect', (e) => {
			this.modelForm.controls['cars'].setValue($('#cars').val());
		});
	}

	setTitles() {
		let breadcrumbs = [{
			title: 'Список водителей',
			page: '/' + this.baseUrl + 's'
		}];
		this.subheader.setTitle('Водители');
		if (this.state === 'edit') {
			this.title = 'Редактирование водителя';
			breadcrumbs.push({
				title: 'Редактирование',
				page: '/' + this.baseUrl + 's/edit?id=' + this.modelForm.controls['id'].value,
			});
		} else {
			this.title = 'Добавление водителя';
			breadcrumbs.push({
				title: 'Добавление',
				page: '/' + this.baseUrl + 's/edit'
			});
		}
		this.subheader.appendBreadcrumbs(breadcrumbs);
		this.titleService.setTitle(this.title);
	}

	getComponentTitle() {
		return this.title;
	}

	reset() {
		this.modelForm.reset();
		this.setFormValues();
	}

	setFormValues() {
		this.route.data.pipe()
			.subscribe((data) => {
				if (data['towns']) {
					this.townsList = data['towns'];
				}
				if (data['cars']) {
					this.carsList = data['cars'];
					console.log(this.carsList);
				}
				if (data['model']) {
					this.balanceData = {
						id: data['model'].id,
						total: data['model'].total
					};
					this.modelForm.patchValue(data['model'], {
						emitEvent: false,
						onlySelf: true
					});
					console.log(this.modelForm);
				}
			});
	}

	setInputMasks() {
		$('#phone').inputmask('mask', {
			'mask': '+7 (999) 999-9999',
			oncomplete: (e) => {
				console.log(e);
				this.modelForm.controls['phone'].setValue(e.target.value);
			},
		});
	}

	onSubmit(redirect) {
		let request;
		if (this.state === 'edit') {
			request = this.http.put('api/' + this.baseUrl, this.modelForm.value);
		} else {
			request = this.http.post('api/' + this.baseUrl, this.modelForm.value);
		}
		request.subscribe((data) => {
			if (redirect) {
				this.router.navigate(['/' + this.baseUrl + 's']);
			}
			this.hasFormErrors = false;
			this.errors = [];
		}, (data) => {
			this.errors = data.error;
			this.hasFormErrors = true;
			this.cdr.detectChanges();
		});
	}

	initWizard() {
		let options = {
			startStep: 1,
			manualStepForward: false
		};
		let wizard = new KTWizard('kt_wizard_v3', options);
	}

	initFormValues() {
		this.route.queryParams.subscribe((params) => {
			const id = params['id'];
			if (id > 0) {
				this.state = 'edit';
			} else {
				this.state = 'create';
			}
			this.setFormValues();
			this.setTitles();
		});
	}

	createForm() {
		this.modelForm = this.Fb.group({
			id: ['', Validators.required],
			town_id: ['', Validators.required],
			last_name: ['', Validators.required],
			first_name: ['', Validators.required],
			phone: ['', Validators.required],
			blocked: [0, Validators.required],
			password: ['', Validators.required],
			cars: [''],
		});
	}

	formConfig() {
		return [
			{
				field: 'town_id',
				label: 'Город',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options: this.townsList,
				nullValue: !0
			}, {
				field: 'last_name',
				label: 'Фамилия',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			}, {
				field: 'first_name',
				label: 'Имя',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			}, {
				field: 'phone',
				label: 'Телефон',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			}, {
				field: 'cars',
				label: 'Автомобили',
				type: 'select2',
				permission: 'manage_' + this.baseUrl + 's',
				options: this.carsList,
				nullValue: !1
			}, {
				field: 'password',
				label: 'Пароль',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'blocked',
				label: 'Блокировка',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options: [
					{
						value: 0,
						label: 'Нет'
					}, {
						value: 1,
						label: 'Да'
					},
				],
				nullValue: false
			},
		];
	}

	isFieldVisible(permission) {
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field) {
		return {
			'is-invalid': this.errors[field],
			'form-control': true,
		};
	}

	onAlertClose($event) {
		this.hasFormErrors = false;
	}
}
