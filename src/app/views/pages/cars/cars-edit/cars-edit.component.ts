import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {SubheaderService} from '../../../../core/_base/layout';
import {User} from '../../../../core/auth';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Car} from '../../../../core/car/_models/car';

@Component({
    selector: 'kt-cars-edit',
    templateUrl: './cars-edit.component.html',
    styleUrls: ['./cars-edit.component.scss']
})
export class CarsEditComponent implements OnInit, AfterViewInit {
    config = {
        orientation: 'horizontal'
    };
    buttonDisabled = false;
    loading$: Observable<boolean>;
    title = '';
    baseUrl = 'car';
    state: string;
    hasFormErrors: boolean = false;
    model: Car = new Car();
    modelForm: FormGroup;
    user = new User();
    roles: any = {};
    errors: any = [];
    submitted = false;
    constructor(
        private subheader: SubheaderService,
        private userModel: User,
        private router: Router,
        private titleService: Title,
        private http: HttpClient,
        private Fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private route: ActivatedRoute) {

    }
    ngOnInit() {
        this.createForm();
        this.initFormValues();
        console.log(this.modelForm);
    }
    ngAfterViewInit() {
        this.setInputMasks();
    }
    setTitles() {
        let breadcrumbs: any = [{
            title: 'Список автомобилей',
            page: '/' + this.baseUrl + 's'
        }];
        this.subheader.setTitle('Услуги');
        if (this.state === 'edit') {
            this.title = 'Редактирование автомобилей';
            breadcrumbs.push({
                title: 'Редактирование',
                page: '/' + this.baseUrl + 's/edit?id=' + this.modelForm.controls['id'].value,
            });
        } else {
            this.title = 'Добавление автомобилей';
            breadcrumbs.push({
                title: 'Добавление',
                page: '/' + this.baseUrl + 's/edit'
            });
        }
        this.subheader.appendBreadcrumbs(breadcrumbs);
        this.titleService.setTitle(this.title);
    }
    getComponentTitle() {
        return this.title;
    }
    reset() {
        this.modelForm.reset();
        this.setFormValues();
    }
    setInputMasks() {
        // $('#number').inputmask('mask', {
        //     'mask': 'A 999 AA',
        //     oncomplete: (e) => {
        //         console.log(e);
        //         this.modelForm.controls['number'].setValue(e.target.value);
        //     },
        // });
        $('#year').inputmask('mask', {
            'mask': '9999',
            oncomplete: (e) => {
                console.log(e);
                this.modelForm.controls['year'].setValue(e.target.value);
            },
        });
    }
    setFormValues() {
        this.route.data
            .subscribe((data) => {
                if (data['data']) {
                    this.modelForm.patchValue(data['data'], {
                        emitEvent: false,
                        onlySelf: true
                    });
                    data['data']['images'].forEach((item) => {
                    	this.modelForm.controls[item['type']]['name'] = item['name'];
					});
                }
            });
    }
    onFileChange(event, field) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = (e: any) => {
            	let control = this.modelForm.controls[field] as FormArray;
            	let result = '' + reader.result;
				control.controls['base64'].setValue(result.split(',')[1]);
                $('#' + field + '_preview')
                    .attr('src', e.target.result)
                    .width(250);
            };
        }
    }
    onSubmit(redirect) {
        let request;
        this.cdr.detectChanges();
        if (this.state === 'edit') {
            request = this.http.put('api/' + this.baseUrl, this.modelForm.value);
        } else {
            request = this.http.post('api/' + this.baseUrl, this.modelForm.value);
        }
        this.buttonDisabled = true;
        request.subscribe((data) => {
            if (redirect) {
                this.router.navigate(['/' + this.baseUrl + 's']);
            }
            this.hasFormErrors = false;
            this.errors = [];
			this.buttonDisabled = false;
        }, (data) => {
			this.addErrors(data.error);
            this.hasFormErrors = true;
			this.buttonDisabled = false;
            this.cdr.detectChanges();
        });
    }
    initFormValues() {
        this.route.queryParams.subscribe((params) => {
            const id = params['id'];
            if (id > 0) {
                this.state = 'edit';
                this.setFormValues();
            } else {
                this.state = 'create';
            }
            this.setTitles();
        });
    }
    createForm() {
        this.modelForm = this.Fb.group({
            id: [this.model.id, Validators.required],
            mark: [this.model.mark, Validators.required],
            model: [this.model.model, Validators.required],
            number: [this.model.number, Validators.required],
            color: [this.model.color, Validators.required],
            year: [this.model.year, Validators.required],
            seat: [this.model.seat, Validators.required],
            conditioner: [this.model.conditioner, Validators.required],
            bank_card: [this.model.bank_card, Validators.required],
            baby_seat: [this.model.baby_seat, Validators.required],
			baggage_count: [this.model.baggage_count, Validators.required],
            blocked: [this.model.blocked, Validators.required],
            forward: this.createImageForm(),
            // right: this.createImageForm(),
            salon: this.createImageForm(),
            baggage: this.createImageForm(),
        });
    }
    createImageForm() {
        return this.Fb.group({
			name : '',
            value : '',
            base64 : '',
			uploaded : false
        });
    }

    getImageSource(name) {
    	if (this.modelForm.controls[name]['uploaded'] == true) {
    		return false;
		} else {
    		return 'api/public/images/car_images/'+this.modelForm.controls['id'].value+'/'+this.modelForm.controls[name]['name'];
		}
	}

    formConfig() {
        return [
            {
                field: 'forward',
                label: 'Фото перед - правый бок',
                type: 'file',
                permission: 'manage_' + this.baseUrl + 's'
            },
            // {
            //     field: 'right',
            //     label: 'Фото справа',
            //     type: 'file',
            //     permission: 'manage_' + this.baseUrl + 's'
            // },
			{
                field: 'salon',
                label: 'Фото салона',
                type: 'file',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'baggage',
                label: 'Фото багажника',
                type: 'file',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'mark',
                label: 'Марка',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'model',
                label: 'Модель',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'number',
                label: 'Гос.номер',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'color',
                label: 'Цвет',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'year',
                label: 'Год',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'seat',
                label: 'Кол-во пассажирских мест',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            },{
                field: 'baggage_count',
                label: 'Кол-во багажных мест',
                type: 'text',
                permission: 'manage_' + this.baseUrl + 's'
            }, {
                field: 'bank_card',
                label: 'Оплата банковской картой',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            }, {
                field: 'baby_seat',
                label: 'Детское сиденье',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            }, {
                field: 'conditioner',
                label: 'Кондиционер',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            }, {
                field: 'blocked',
                label: 'Блокировка',
                type: 'select',
                permission: 'manage_' + this.baseUrl + 's',
                options: [
                    {
                        value: 0,
                        label: 'Нет'
                    }, {
                        value: 1,
                        label: 'Да'
                    },
                ],
                nullValue: false
            },
        ];
    }
    addErrors(errors) {
    	this.errors = errors;
        // errors.forEach((item) => {
		// 	this.errors.push(item.split('.')[0]);
		// });
	}

    isFieldVisible(permission) {
        return this.userModel.hasPermissionTo(permission);
    }
    trackByFn(index: any, item: any) {
        return index;
    }
    getErrorMessage(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        } else {
            return '';
        }
    }
    hasError(field) {
        return {
            'is-invalid': this.errors[field],
            'form-control': true,
        };
    }
    onAlertClose() {
        this.hasFormErrors = false;
    }
}
