import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CarsEditComponent } from './cars-edit/cars-edit.component';
import {
	MatAutocompleteModule, MatButtonModule,
	MatCardModule,
	MatCheckboxModule, MatDatepickerModule,
	MatDialogModule, MatExpansionModule, MatIconModule, MatInputModule,
	MatMenuModule, MatNativeDateModule, MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule, MatSnackBarModule, MatSortModule, MatTableModule,
	MatTabsModule,
	MatTooltipModule
} from "@angular/material";
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {JwtInterceptor} from "../../../core/auth/_interceptor/interceptor";
import {PartialsModule} from "../../partials/partials.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TranslateModule} from "@ngx-translate/core";
import {CarEditResolver} from "../../../core/car/resolvers/car";

const routes: Routes = [
	{ path: '', component : CarsListComponent },
	{ path: 'edit', component : CarsEditComponent , resolve : { data : CarEditResolver }}
];

@NgModule({
  declarations: [CarsListComponent, CarsEditComponent],
	imports: [
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatExpansionModule,
		MatTabsModule,
		MatTooltipModule,
		MatDialogModule
	],
	providers: [CarEditResolver,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},],
})
export class CarsModule { }
