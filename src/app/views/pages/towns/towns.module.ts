import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TownsEditComponent } from './towns-edit/towns-edit.component';
import { TownsListComponent } from './towns-list/towns-list.component';
import {RouterModule, Routes} from "@angular/router";
import {TownEditResolver} from "../../../core/town/resolvers/town";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {JwtInterceptor} from "../../../core/auth/_interceptor/interceptor";
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatIconModule,
	MatInputModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule, MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSnackBarModule,
	MatSortModule,
	MatTableModule,
	MatTabsModule,
	MatTooltipModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";
import {PartialsModule} from "../../partials/partials.module";


const routes: Routes = [
	{ path: '', redirectTo: 'list'},
	{ path: 'list', component : TownsListComponent },
	{ path: 'edit', component : TownsEditComponent , resolve : { 
		data : TownEditResolver 
	}}
];

@NgModule({
  declarations: [TownsEditComponent, TownsListComponent],
  imports: [
	CommonModule,
	HttpClientModule,
	PartialsModule,
	RouterModule.forChild(routes),
	FormsModule,
	ReactiveFormsModule,
	TranslateModule.forChild(),
	MatButtonModule,
	MatMenuModule,
	MatSelectModule,
	MatInputModule,
	MatTableModule,
	MatAutocompleteModule,
	MatRadioModule,
	MatIconModule,
	MatNativeDateModule,
	MatProgressBarModule,
	MatDatepickerModule,
	MatCardModule,
	MatPaginatorModule,
	MatSortModule,
	MatCheckboxModule,
	MatProgressSpinnerModule,
	MatSnackBarModule,
	MatExpansionModule,
	MatTabsModule,
	MatTooltipModule,
	MatDialogModule
  ],
	providers: [TownEditResolver,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},],
})
export class TownsModule { }
