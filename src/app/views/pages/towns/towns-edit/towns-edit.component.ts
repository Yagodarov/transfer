import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from "../../../../core/auth";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SubheaderService} from "../../../../core/_base/layout";
import {Observable} from "rxjs/index";

@Component({
  selector: 'kt-towns-edit',
  templateUrl: './towns-edit.component.html',
  styleUrls: ['./towns-edit.component.scss']
})
export class TownsEditComponent implements OnInit {
	config = {
		orientation : 'horizontal'
	};
	loading$: Observable<boolean>;
	title = "";
	state : string;
	hasFormErrors : boolean = false;
	model : any = {};
	modelForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private titleService: Title,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
	}

	setTitles() {
		let breadcrumbs = [{
			title : 'Список городов',
			page : '/towns'
		}];
		this.subheader.setTitle("Города");
		if (this.state == 'edit') {
			this.title = "Редактирование города";
			breadcrumbs.push({
				title : 'Редактирование',
				page : '/towns/edit?id='+this.modelForm.controls['id'].value,
			});
		} else {
			this.title = "Добавление города";
			breadcrumbs.push({
				title : 'Добавление',
				page : '/towns/edit'
			});
		}
		this.subheader.appendBreadcrumbs(breadcrumbs);
		this.titleService.setTitle(this.title);
	}

	getComponentTitle() {
		return this.title;
	}

	submitted = false;

	reset() {
		this.modelForm.reset();
	}

	onSubmit(redirect) {
		if (this.state == 'edit') {
			this.http.put('api/town', this.modelForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/towns']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		} else {
			this.http.post('api/town', this.modelForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/towns']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		}
	}

	initFormValues() {
		console.log(this.route.queryParams.subscribe((params) => {
			const id = params['id'];
			if (id > 0) {
				this.state = 'edit';
				this.route.data
					.subscribe((data) => {
						if (data['data']) {
							this.modelForm.patchValue(data['data'], {
								emitEvent : false,
								onlySelf : true
							});
						}
					});

			} else {
				this.state = 'create';
			}
			this.setTitles();
		}));
	}

	createForm() {
		this.modelForm = this.Fb.group({
			id: ['', Validators.required],
			label: ['', Validators.required],
			blocked: [0, Validators.required],
		});
	}

	formConfig() {
		return [
			{
				field: 'label',
				label: 'Название',
				type: 'text',
				permission: 'manage_towns'
			},
			{
				field: 'blocked',
				label: 'Блокировка',
				type: 'select',
				permission: 'manage_towns',
				options: [
					{
						value : 0,
						label : 'Нет'
					},{
						value : 1,
						label : 'Да'
					},
				]
			},
		]
	}

	isFieldVisible(permission) {
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}

	onAlertClose($event) {
		this.hasFormErrors = false;
	}
}
