import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TownsEditComponent } from './towns-edit.component';

describe('TownsEditComponent', () => {
  let component: TownsEditComponent;
  let fixture: ComponentFixture<TownsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TownsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TownsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
