// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import {Action, Store} from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
// Auth
import {AuthNoticeService, AuthService, Login, UserLoaded} from '../../../../core/auth';
import {User} from '../../../../core/auth/_models/user.model';
import {HttpClient} from '@angular/common/http';
import {AuthActionTypes} from "../../../../core/auth/_actions/auth.actions";

/**
 * ! Just example => Should be removed in development
 */

@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None,
	styleUrls: ['./login.css']
})
export class LoginComponent implements OnInit, OnDestroy {
	// Public params
	loginForm: FormGroup;
	loading = false;
	error_message = '';
	isLoggedIn$: Observable<boolean>;
	errors: any = [];

	private unsubscribe: Subject<any>; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param router: Router
	 * @param auth: AuthService
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr
	 */
	constructor(
		private router: Router,
		private http: HttpClient,
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef
	) {
		this.unsubscribe = new Subject();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.initLoginForm();
		this.resetCredentials();
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	resetCredentials() {
		localStorage.clear();
	}

	initAllAndRedirect(user) {
		console.log(user);
		if (user.role === 'admin') {
			this.router.navigateByUrl('/'); // Main page
		}
		if (user.role === 'manager') {
			this.router.navigateByUrl('/orders'); // Main page
		}
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		// demo message to show

		this.loginForm = this.fb.group({
			username: [null],
			password: [null]
		});
	}

	/**
	 * Form Submit
	 */
	submit() {
		const controls = this.loginForm.controls;
		/** check form */

		// this.http.post<User>('api/users/', { 'user' : 'fgh', 'pw' : 'gh' }).subscribe(() => {
        //
		// });

		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			username: controls['username'].value,
			password: controls['password'].value
		};
		this.error_message = '';
		this.auth
			.login(authData.username, authData.password)
			.subscribe((user) => {
				this.loading = false;
				if (user) {
					this.store.dispatch(new Login({authToken: user.accessToken}));
					console.log(user);
					localStorage.setItem('permissions', user.permissions_list);
					localStorage.setItem('authce9d77b308c149d5992a80073637e4d5', user.accessToken);
					localStorage.setItem('options', user['options']);
					this.initAllAndRedirect(user);
				} else {
					this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
				}
				this.cdr.detectChanges();
			}, (error) => {
				this.error_message = error.error;
				this.loading = false;
				this.cdr.detectChanges();
			});
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
