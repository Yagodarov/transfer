import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from "../../../core/auth";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SubheaderService} from "../../../core/_base/layout";
import {Observable} from "rxjs/index";
@Component({
  selector: 'kt-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
	config = {
		orientation : 'horizontal'
	};
	loading$: Observable<boolean>;
	title = "";
	baseUrl = 'setting';
	state : string;
	hasFormErrors : boolean = false;
	model : any = {};
	modelForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private titleService: Title,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
	}

	setTitles() {
		let breadcrumbs = [{
			title : 'Параметры',
			page : '/'+this.baseUrl+'s'
		}];
		this.subheader.setTitle("Параметры");
		this.title = "Редактирование параметров";
		breadcrumbs.push({
			title : 'Редактирование',
			page : '/'+this.baseUrl+'s/edit',
		});
		this.subheader.setBreadcrumbs(breadcrumbs);
		this.titleService.setTitle(this.title);
	}

	getComponentTitle() {
		return this.title;
	}

	submitted = false;

	reset() {
		this.modelForm.reset();
		this.setFormValues();
	}

	setFormValues() {
		this.route.data
			.subscribe((data) => {
				if (data['data']) {
					this.modelForm.patchValue(data['data'], {
						emitEvent : false,
						onlySelf : true
					});
				}
			});
	}

	onSubmit(redirect) {
		let request;
		if (this.state == 'edit') {
			request = this.http.put('api/'+this.baseUrl, this.modelForm.value);
		} else {
			request = this.http.post('api/'+this.baseUrl, this.modelForm.value);
		}
		request.subscribe((data) => {
			if (redirect) this.router.navigate(['/'+this.baseUrl+'s']);
			this.hasFormErrors = false;
		}, (data) => {
			this.errors = data.error;
			this.hasFormErrors = true;
			this.cdr.detectChanges();
		});
	}

	initFormValues() {
		this.route.queryParams.subscribe((params) => {
			this.state = 'edit';
			this.setFormValues();
			this.setTitles();
		});
	}

	createForm() {
		this.modelForm = this.Fb.group({
			order_percent: [0, Validators.required],
			order_time: [0, Validators.required],
		});
	}

	formConfig() {
		return [
			{
				field: 'order_percent',
				label: 'Процент от заказа',
				type: 'text',
				permission: 'manage_settings'
			},{
				field: 'order_time',
				label: 'Время на заказ',
				type: 'text',
				permission: 'manage_settings'
			}
		]
	}

	isFieldVisible(permission) {
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}

	onAlertClose($event) {
		this.hasFormErrors = false;
	}
}
