import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ComponentFixture} from "@angular/core/testing";
import {async} from "q";
import {UserEditResolver} from "../../../core/user/resolvers/user";
import {User, UserEffects, usersReducer} from "../../../core/auth";
import { UsersComponent } from './users.component';
import {
	MatAutocompleteModule,
	MatButtonModule, MatCardModule, MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule, MatExpansionModule, MatIconModule, MatInputModule,
	MatMenuModule, MatNativeDateModule, MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatTooltipModule
} from "@angular/material";
import {StoreModule} from "@ngrx/store";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {EffectsModule} from "@ngrx/effects";
import {PartialsModule} from "../../partials/partials.module";
import {TranslateModule} from "@ngx-translate/core";
import {JwtInterceptor} from "../../../core/auth/_interceptor/interceptor";

const routes: Routes = [
	{
		path : '',
		component : UsersComponent,
		children : [
			{ path: '', component : UsersListComponent },
			{ path: 'edit', component : UserEditComponent , resolve : { data : UserEditResolver }}
		],
	},
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		StoreModule.forFeature('users', usersReducer),
		EffectsModule.forFeature([UserEffects]),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatExpansionModule,
		MatTabsModule,
		MatTooltipModule,
		MatDialogModule
	],
	providers: [UserEditResolver,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},],
  declarations: [UsersListComponent, UserEditComponent, UsersComponent],

})
export class UsersModule { }
