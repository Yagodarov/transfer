import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../../core/auth";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {SubheaderService} from "../../../../core/_base/layout";
import {Title} from "@angular/platform-browser";
import {Observable} from "rxjs/index";
declare var Object: any;
@Component({
  selector: 'kt-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
	config = {
		orientation : 'horizontal'
	};
	loading$: Observable<boolean>;
	title = "";
	state : string;
	hasFormErrors : boolean = false;
	model : any = {};
	userForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private titleService: Title,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
	}

	setTitles() {
		let breadcrumbs = [{
			title : 'Список пользователей',
			page : '/users'
		}];
		this.subheader.setTitle("Пользователи");
		if (this.state == 'edit') {
			this.title = "Редактирование пользователя";
			breadcrumbs.push({
				title : 'Редактирование',
				page : '/users/edit?id='+this.userForm.controls['id'].value,
			});
		} else {
			this.title = "Добавление пользователя";
			breadcrumbs.push({
				title : 'Добавление',
				page : '/users/create'
			});
		}
		this.subheader.setBreadcrumbs(breadcrumbs);
		this.titleService.setTitle(this.title);
	}

	getComponentTitle() {
		return this.title;
	}

	submitted = false;

	reset() {
		this.userForm.reset();
	}

	onSubmit(redirect) {
		if (this.state == 'edit') {
			this.http.put('api/user', this.userForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/users']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		} else {
			this.http.post('api/user', this.userForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/users']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		}
	}

	initFormValues() {
		console.log(this.route.queryParams.subscribe((params) => {
			const id = params['id'];
			if (id > 0) {
				this.state = 'edit';
				this.route.data
					.subscribe((data) => {
						if (data['data']) {
							this.userForm.patchValue(data['data'], {
								emitEvent : false,
								onlySelf : true
							});
						}
					});

			} else {
				this.state = 'create';
			}
			this.setTitles();
		}));
	}

	createForm() {
		this.userForm = this.Fb.group({
			id: [this.user.id, Validators.required],
			username: [this.user.username, Validators.required],
			password: [this.user.password, Validators.required],
			email: [this.user.email, Validators.email],
			first_name: [this.user.first_name, Validators.required],
			surname: [this.user.surname, Validators.required],
			patronymic: [this.user.patronymic, Validators.required],
			role: [this.user.role]
		});
	}

	formConfig() {
		return [
			{
				field: 'username',
				label: 'Имя пользователя',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'password',
				label: 'Пароль',
				type: 'password',
				permission: 'change_password'
			},
			{
				field: 'surname',
				label: 'Фамилия',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'first_name',
				label: 'Имя',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'patronymic',
				label: 'Отчество',
				type: 'text',
				permission: 'manage_users'
			},
			{
				field: 'email',
				label: 'Почта',
				type: 'email',
				permission: 'manage_users'
			},
			{
				field: 'role',
				label: 'Роль',
				type: 'select',
				permission: 'manage_users',
				options: this.userModel.roleList
			},
		]
	}

	isFieldVisible(permission) {
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}

	onAlertClose($event) {
		this.hasFormErrors = false;
	}
}
