import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {SubheaderService} from "../../../../core/_base/layout";
import {AuthService} from "../../../../core/auth/_services";
import {AppState} from "../../../../core/reducers";
import {User} from "../../../../core/auth";
import {HttpClient} from "@angular/common/http";
declare var moment : any;
@Component({
  selector: 'kt-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {
	title = 'Заказы';
	baseUrl = 'order';
	datatable : any;
	models : any;
	user : User = new User();
	constructor(
		private subheader: SubheaderService,
		private cdr: ChangeDetectorRef,
		public auth: AuthService,
		public router : Router,
		private titleService: Title,
		private store: Store<AppState>,
		private http: HttpClient) { }

	ngOnInit() {
		this.subheader.setBreadcrumbs([
			{
				title : 'Список заказов',
				url : 'users'
			}
		]);
		this.subheader.setTitle("Заказы");
		this.cdr.detectChanges();
		this.titleService.setTitle("Заказы");
		this.initDatatable();
	}

	redirectCreateModel() {
		this.router.navigate(['/orders/edit']);
	}

	initDatatable() {
		const token = this.auth.getUserToken();
		this.datatable = $('#'+this.baseUrl).KTDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						method: 'GET',
						url: 'api/'+this.baseUrl+'s',
						headers: {
							'Authorization': token,
						},
						map: (raw) => {
							// sample data mapping
							var dataSet = raw;
							console.log(raw);
							if (typeof raw.models !== 'undefined') {
								dataSet = raw.models;
							}
							return dataSet;
						},
					},
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},

			// layout definition
			layout: {
				scroll: false,
				footer: false,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: this.getColumnsByRole(),

		});
		this.datatable.on('kt-datatable--on-layout-updated', () => {
			setTimeout(() => {
				$('.editModel').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					this.edit(id);
				});
				$('.deleteModel').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					if (confirm('Вы уверены что хотите удалть?')) {
						this.delete(id);
					}
				});
				$('.toggleStatus').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					this.toggleStatus(id);
				});
			});
		});

		this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
			this.router.navigate(['/auth/login']);
		});
	}

	toggleStatus(id) {
		this.http.post('/api/'+this.baseUrl+'/status/' + id, {}).subscribe(() => {
			this.datatable.reload();
		})
	}

	edit(id) {
		this.router.navigate(['/'+this.baseUrl+'s/edit',], { queryParams: { id: id } });
	}

	delete(id) {
		this.http.delete('/api/'+this.baseUrl+'/' + id).subscribe(() => {
			this.datatable.reload();
		})
	}

	getColumnsByRole() {
		var config = [];
		if (this.user.hasPermissionTo('manage_users')) {
			config = [{
				field: 'id',
				title: '#',
				sortable: 'asc',
				width: 40,
				type: 'number',
				selector: false,
				textAlign: 'center',
				autoHide: !1,
			}, {
				field: 'departure_time',
				title: 'Дата отправления',
				autoHide: !1,
				template: function(row, index, datatable) {
					return moment.utc(row['departure_time']).local().format('DD.MM.YYYY HH:mm');
				}
			},{
				field: 'origin',
				title: 'Место отправления',
				autoHide: !1
			},{
				field: 'destination',
				title: 'Место назначения',
				autoHide: !1,
				template: function(row, index, datatable) {
					var destination = '';
					var count = 0;
					if (row['destinations']) {
						row['destinations'].forEach((item) => {
							if (count > 0) {
								destination += ', ';
							}
							destination += item['address'];
							count++;
						});
					}
					return destination;
				}
			},{
				field: 'car',
				title: 'Автомобиль',
				autoHide: !1,
				template: function(row, index, datatable) {
					if (row.car)
						return row.car.mark + ' ' + row.car.model + ' ' + row.car.number + ' ' + row.car.year + ' ' + row.car.color;
					else
						return;
				}
			},{
				field: 'driver',
				title: 'Водитель',
				autoHide: !1,
				template: function(row, index, datatable) {
					if (row.driver)
						return row.driver.first_name + ' ' + row.driver.last_name + ' ' + row.driver.phone;
					else
						return;
				}
			},{
				field: 'status',
				title: 'Статус',
				autoHide: !1,
				template: function(row, index, datatable) {
					if (row.status != undefined) {
						var labels = [{
							value: 0,
							label: 'В обработке',
						}, {
							value: 1,
							label: 'Поиск такси'
						}, {
							value: 2,
							label: 'Принят'
						}, {
							value: 3,
							label: 'Едет к клиенту'
						}, {
							value: 4,
							label: 'Ожидает клиента'
						}, {
							value: 5,
							label: 'Такси задерживается'
						}, {
							value: 6,
							label: 'Нет клиента'
						}, {
							value: 7,
							label: 'Посадка'
						}, {
							value: 8,
							label: 'В рейсе'
						}, {
							value: 9,
							label: 'Отказ клиента'
						}, {
							value: 10,
							label: 'Предварительный заказ'
						}, {
							value: 11,
							label: 'Отказ водителя'
						}, {
							value: 12,
							label: 'Выполнен'
						}, {
							value: 13,
							label: 'Отменен'
						}
						];
						return labels.find(element => element.value === row.status).label;
					}
					else
						return "Статус неизвестен";
				}
			},{
				field: 'comment',
				title: 'Комментарий',
				autoHide: !1
			},];
		}
		config.push({
			field: 'Actions',
			title: 'Действия',
			sortable: false,
			overflow: 'visible',
			textAlign: 'center',
			autoHide: !1,
			template: (row, index, datatable) => {
				let visible = this.user.hasPermissionTo('manage_'+this.baseUrl+'s');
				var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
				let result =  '<button href="javascript:;" data-id='+row['id']+' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
                        <i class="la la-edit"></i>\
                    </button>';
				if (visible) {
					result +=
						'<button href="javascript:;" data-id='+row['id']+' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
                        <i class="la la-trash"></i>\
                    </button>'
				}
				return result;
			},
		});
		return config;
	}
}
