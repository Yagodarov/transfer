import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersListComponent } from './orders-list/orders-list.component';
import {RouterModule, Routes} from "@angular/router";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {JwtInterceptor} from "../../../core/auth/_interceptor/interceptor";
import {OrderEditResolver} from "../../../core/order/_resolvers/orders";
import { OrdersEditComponent } from './orders-edit/orders-edit.component';
import {
	MatAutocompleteModule,
	MatButtonModule, MatCardModule, MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule, MatExpansionModule, MatIconModule, MatInputModule,
	MatMenuModule, MatNativeDateModule, MatPaginatorModule,
	MatProgressBarModule, MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatTooltipModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PartialsModule} from "../../partials/partials.module";
import {TranslateModule} from "@ngx-translate/core";
import {ClientsListResolver} from "../../../core/client/_resolvers/clients-list";
import {DriverListResolver} from "../../../core/driver/resolvers/driver_list";
import {CarListResolver} from "../../../core/car/resolvers/car_list";
const routes: Routes = [
	// { path: '', redirectTo: 'list'},
	{ path: '', component : OrdersListComponent },
	{ path: 'edit', component : OrdersEditComponent , resolve : {
			model : OrderEditResolver,
			clients : ClientsListResolver,
			drivers : DriverListResolver,
			cars : CarListResolver
		}}
];
@NgModule({
  declarations: [OrdersListComponent, OrdersEditComponent],
  imports: [
    CommonModule,


	  CommonModule,
	  HttpClientModule,
	  PartialsModule,
	  RouterModule.forChild(routes),
	  FormsModule,
	  ReactiveFormsModule,
	  TranslateModule.forChild(),
	  MatButtonModule,
	  MatMenuModule,
	  MatSelectModule,
	  MatInputModule,
	  MatTableModule,
	  MatAutocompleteModule,
	  MatRadioModule,
	  MatIconModule,
	  MatNativeDateModule,
	  MatProgressBarModule,
	  MatDatepickerModule,
	  MatCardModule,
	  MatPaginatorModule,
	  MatSortModule,
	  MatCheckboxModule,
	  MatProgressSpinnerModule,
	  MatSnackBarModule,
	  MatExpansionModule,
	  MatTabsModule,
	  MatTooltipModule,
	  MatDialogModule
  ],
	providers: [
		OrderEditResolver,
		ClientsListResolver,
		DriverListResolver,
		CarListResolver,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},
	]
})
export class OrdersModule { }
