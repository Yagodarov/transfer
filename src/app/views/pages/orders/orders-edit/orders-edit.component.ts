import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {SubheaderService} from "../../../../core/_base/layout";
import {Observable} from "rxjs/index";
import {User} from "../../../../core/auth";
import {HttpClient} from "@angular/common/http";
declare var moment : any;
@Component({
  selector: 'kt-orders-edit',
  templateUrl: './orders-edit.component.html',
  styleUrls: ['./orders-edit.component.scss']
})
export class OrdersEditComponent implements OnInit {
	config = {
		orientation : 'horizontal'
	};
	selectedTab = 0;
	loading$: Observable<boolean>;
	title = "";
	state : string;
	baseUrl = 'order';
	hasFormErrors : boolean = false;
	model : any = {};
	modelForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	clientsList : any = [];
	driverList : any = [];
	carList : any = [];
	destinations = [];
	seats = [];
	hasFormError = false;
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private titleService: Title,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
	}

	ngAfterViewInit() {
		this.setInputMasks();
	}

	onTabChange(e) {
		this.cdr.detectChanges();
	}

	setInputMasks() {
		$('#phone').inputmask('mask', {
			'mask': '+7 (999) 999-9999',
			oncomplete: (e) => {
				console.log(e);
				this.modelForm.controls['phone'].setValue(e.target.value);
			},
		});
	}

	setTitles() {
		let breadcrumbs = [{
			title : 'Список заказов',
			page : '/orders'
		}];
		this.subheader.setTitle("Заказы");
		if (this.state == 'edit') {
			this.title = "Редактирование заказа";
			breadcrumbs.push({
				title : 'Редактирование',
				page : '/orders/edit?id='+this.modelForm.controls['id'].value,
			});
		} else {
			this.title = "Добавление заказа";
			breadcrumbs.push({
				title : 'Добавление',
				page : '/orders/create'
			});
		}
		this.subheader.setBreadcrumbs(breadcrumbs);
		this.titleService.setTitle(this.title);
	}

	getComponentTitle() {
		return this.title;
	}

	submitted = false;

	reset() {
		this.modelForm.reset();
	}

	onSubmit(redirect) {
		let submitModel = this.modelForm.value;
		submitModel.departure_time = moment(submitModel.departure_time).toISOString();
		if (this.state == 'edit') {
			this.http.put('api/order', this.modelForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/orders']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		} else {
			this.http.post('api/order', this.modelForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/orders']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		}
	}

	initFormValues() {
		this.route.queryParams.subscribe((params) => {
			const id = params['id'];
			if (id > 0) {
				this.state = 'edit';
			} else {
				this.state = 'create';
			}
			this.route.data
				.subscribe((data) => {
					if (id > 0) {
						if (data['model']) {
							this.modelForm.patchValue(data['model'], {
								emitEvent : false,
								onlySelf : true
							});
							this.setUtcDate(data);
							this.setValues(data);
						}
					} else {
						this.modelForm.addControl('destination', this.Fb.array([this.createItem('destination')]));
						this.modelForm.addControl('seat', this.Fb.array([this.createItem('seat')]));
					}
					if (data['clients']) {
						this.clientsList = data['clients'];
					}
					if (data['drivers']) {
						this.driverList = data['drivers'];
					}
					if (data['cars']) {
						this.carList = data['cars'];
					}
					console.log(data);
				});
			this.setTitles();
		});
	}

	setValues(data) {
		let seats = [];
		let destinations = [];
		if (data['model']['seats'].length) {
			let ad = seats = data['model']['seats'];
			data['model']['seats'].forEach((item) => {
				this.addItem('seat', item);
			});
		} else {
			this.addItem('seat')
		}
		if (data['model']['destinations'].length) {
			data['model']['destinations'].forEach((item) => {
				this.addItem('destination', item);
			});
		} else {
			this.addItem('destination');
		}
	}

	setUtcDate(data)
	{
		var stillUtc = moment.utc(data['model']['departure_time']).toDate();
		var local = moment(stillUtc).local().format('YYYY-MM-DD[T]HH:mm');
		this.modelForm.controls['departure_time'].setValue(local);
	}

	createForm() {
		this.modelForm = this.Fb.group({
			id: '',
			departure_time: [''],
			origin: [''],
			passengers_count: [1],
			baggage_count: [0],
			payment_type: [0],
			conditioner: [0],
			bank_card_payment: [0],
			client_id: [null],
			driver_id: [null],
			car_id: [null],
			comment: [''],
			status: [0],
		});
	}

	createItem(name, data = null) {
		let thisFormGroup;
		if (name === 'destination') {
			thisFormGroup =  this.Fb.group({
				value: data ? data['address'] + '' : ''
			});
		} else if(name === 'seat') {
			thisFormGroup =  this.Fb.group({
				weight: data ? data['weight'] + '' : '',
				age: data ? data['age'] + '' : ''
			});
		}
		return thisFormGroup;
	}


	addItem(name, data = null):void {
		if (!this.modelForm.controls[name]) {
			this.modelForm.addControl(name, this.Fb.array([this.createItem(name, data)]));
		} else {
			let items = this.modelForm.get(name) as FormArray;
			items.push(this.createItem(name, data));
		}
	}

	removeItem(name, id):void {
		let items = this.modelForm.get(name) as FormArray;
		if (items.length > 1)
			items.removeAt(id);
	}

	formConfig() {
		return [
			{
				field: 'departure_time',
				label: 'Дата и время выезда',
				type: 'datetime-local',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'origin',
				label: 'Место отправления',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'passengers_count',
				label: 'Кол-во пассажиров',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'baggage_count',
				label: 'Кол-во багажа',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'payment_type',
				label: 'Способ оплаты',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options: [{
							value: 0,
							label: 'Наличные',
						}, {
							value: 1,
							label: 'Банковская карта'
						},{
							value: 2,
							label: 'Баланс'
						},
				],
				nullValue: false
			},
			{
				field: 'conditioner',
				label: 'Кондиционер',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options: [{
							value: 0,
							label: 'Нет',
						}, {
							value: 1,
							label: 'Да'
						}
				],
				nullValue: false
			},
			{
				field: 'client_id',
				label: 'Клиент',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options : this.clientsList,
				nullValue: true
			},
			{
				field: 'driver_id',
				label: 'Водитель',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options : this.driverList,
				nullValue: true
			},
			{
				field: 'car_id',
				label: 'Автомобиль',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options : this.carList,
				nullValue: true
			},
			{
				field: 'comment',
				label: 'Комментарий',
				type: 'textarea',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'status',
				label: 'Статус',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options: [{
					value: 0,
					label: 'В обработке',
				}, {
					value: 1,
					label: 'Поиск такси'
				}, {
					value: 2,
					label: 'Принят'
				}, {
					value: 3,
					label: 'Едет к клиенту'
				}, {
					value: 4,
					label: 'Ожидает клиента'
				}, {
					value: 5,
					label: 'Такси задерживается'
				}, {
					value: 6,
					label: 'Нет клиента'
				}, {
					value: 7,
					label: 'Посадка'
				}, {
					value: 8,
					label: 'В рейсе'
				}, {
					value: 9,
					label: 'Отказ клиента'
				}, {
					value: 10,
					label: 'Предварительный заказ'
				}, {
					value: 11,
					label: 'Отказ водителя'
				}, {
					value: 12,
					label: 'Выполнен'
				}, {
					value: 13,
					label: 'Отменен'
				}
				],
				nullValue: false
			},
		]
	}

	isFieldVisible(permission) {
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}
}
