// Angular
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
// Lodash
import { shuffle } from 'lodash';
// Services
import {LayoutConfigService, SubheaderService} from '../../../core/_base/layout';
// Widgets model
import { SparklineChartOptions } from '../../../core/_base/metronic';
import { Widget4Data } from '../../partials/content/widgets/widget4/widget4.component';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {User} from "../../../core/auth";
import {Title} from "@angular/platform-browser";

@Component({
	selector: 'kt-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
	model = {};
	message = "";
	form : FormGroup = this.createForm();
	errors = [];
	constructor(
		private user: User,
		private http:HttpClient,
		private titleService: Title,
		private subheader: SubheaderService,
		private Fb:FormBuilder,
		private cdr:ChangeDetectorRef,
		private route: ActivatedRoute
	) {

	}
	ngOnInit() {
		this.titleService.setTitle("Панель управления");
		this.subheader.setTitle("Панель управления");
		// this.subheader.setBreadcrumbs([
		// 	{
		// 		title : 'Список пользователей',
		// 		url : '/users'
		// 	}
		// ]);
		this.createForm();
		this.initFormValues();
	}

	initFormValues() {
		this.http.request('get', '/api/settings').subscribe((data : any) => {
			this.form.patchValue(data);
		});
	}

	onSubmit() {
		this.http.put('/api/settings', this.form.value).subscribe((data) => {
			alert('Сохранено')
		});
	}


	formConfig() {
		return [
			{
				field: 'id',
				label: 'id',
				type: 'text',
				permission: 'hidden',
				hidden: true,
			},{
				field: 'zadarma_key',
				label: 'ZADARMA key',
				type: 'text',
				permission: 'manage_settings'
			},{
				field: 'zadarma_secret',
				label: 'ZADARMA secret',
				type: 'text',
				permission: 'manage_settings'
			},{
				field: 'roistat_key',
				label: 'ROISTAT key',
				type: 'text',
				permission: 'manage_settings'
			},{
				field: 'roistat_project',
				label: 'ROISTAT project',
				type: 'text',
				permission: 'manage_settings'
			},
		]
	}


	isFieldVisible(permission) {
		return this.user.hasPermissionTo(permission);
	}

	createForm() {
		let result = {};
		this.formConfig().forEach((item) => {
			result[item['field']] = '';
		})
		return this.Fb.group(result);
	}

	fillCallsAndContactsZadarma() {
		this.http.request('GET', 'api/zadarma').subscribe((data) => {
			this.message = "Статус : " + data['status'] + "\n";
			this.message += "Загружено/обновлено контактов : " + data['contacts'] + "\n";
			this.message += "Загружено/обновлено вызовов : " + data['calls'] + "\n";
			this.cdr.detectChanges();
		}, error => {
			console.log(error);
		});
	}

	fillContactsRoistat() {
		this.http.request('GET', 'api/roistat').subscribe((data) => {
			this.message = "Заполнено контактов : " + data['contacts'];
			this.cdr.detectChanges();
		}, error => {
			console.log(error);
		});
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	trackByFn(index: any, item: any) {
		return index;
	}
	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}
}
