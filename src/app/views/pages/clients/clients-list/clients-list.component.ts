import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {User} from '../../../../core/auth';
import {HttpClient} from '@angular/common/http';
import {AppState} from '../../../../core/reducers';
import {SubheaderService} from '../../../../core/_base/layout';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {AuthService} from '../../../../core/auth/_services';

@Component({
	selector: 'kt-clients-list',
	templateUrl: './clients-list.component.html',
	styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {
	title = 'Клиенты';
	baseUrl = 'client';
	datatable: any;
	models: any;
	user: User = new User();

	constructor(
		private subheader: SubheaderService,
		private cdr: ChangeDetectorRef,
		public auth: AuthService,
		public router: Router,
		private titleService: Title,
		private store: Store<AppState>,
		private http: HttpClient) {
	}

	ngOnInit() {
		this.subheader.setBreadcrumbs([
			{
				title: 'Список клиентов',
				url: 'users'
			}
		]);
		this.subheader.setTitle('Клиенты');
		this.cdr.detectChanges();
		this.titleService.setTitle('Клиенты');
		this.initDatatable();
	}

	redirectCreateModel() {
		this.router.navigate(['/clients/edit']);
	}

	initDatatable() {
		const token = this.auth.getUserToken();
		this.datatable = $('#' + this.baseUrl).KTDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						method: 'GET',
						url: 'api/' + this.baseUrl + 's',
						headers: {
							'Authorization': token,
						},
						map: (raw) => {
							// sample data mapping
							var dataSet = raw;
							console.log(raw);
							if (typeof raw.models !== 'undefined') {
								dataSet = raw.models;
							}
							return dataSet;
						},
					},
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true,
			},

			// layout definition
			layout: {
				scroll: false,
				footer: false,
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch'),
			},

			// columns definition
			columns: this.getColumnsByRole(),

		});
		this.datatable.on('kt-datatable--on-layout-updated', () => {
			setTimeout(() => {
				$('.editModel').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					this.edit(id);
				});
				$('.deleteModel').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					if (confirm('Вы уверены что хотите удалть?')) {
						this.delete(id);
					}
				});
				$('.toggleStatus').click((e) => {
					var id = $(e.currentTarget).attr('data-id');
					this.toggleStatus(id);
				});
			});
		});

		this.datatable.on('kt-datatable--on-ajax-fail', ($param) => {
			this.router.navigate(['/auth/login']);
		});
	}

	toggleStatus(id) {
		this.http.post('/api/' + this.baseUrl + '/status/' + id, {}).subscribe(() => {
			this.datatable.reload();
		})
	}

	edit(id) {
		this.router.navigate(['/' + this.baseUrl + 's/edit',], {queryParams: {id: id}});
	}

	delete(id) {
		this.http.delete('/api/' + this.baseUrl + '/' + id).subscribe(() => {
			this.datatable.reload();
		})
	}

	getColumnsByRole() {
		var config = [];
		if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
			config = [{
				field: 'id',
				title: '#',
				sortable: 'asc',
				width: 40,
				type: 'number',
				selector: false,
				textAlign: 'center',
				autoHide: !1,
			}, {
				field: 'first_name',
				title: 'Имя, фамилия',
				autoHide: !1,
				template: function (row, index, datatable) {
					return row['first_name'] + ' ' + row['last_name'];
				}
			}, {
				field: 'phone',
				title: 'Тел.номер',
				autoHide: !1
			},];
		}
		if (this.user.hasPermissionTo('manage_' + this.baseUrl + 's')) {
			config.push({
				field: 'blocked',
				title: 'Статус',
				width: 140,
				template: function(row, index, datatable) {
					let html = '';
					if (row['blocked'] === 1) {
						html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-danger btn-sm btn-pill">Заблокирован</button>';
					} else {
						html = ' <button data-id=' + row['id'] + ' type="button" class="toggleStatus btn btn-success btn-sm btn-pill">Активен</button>';
					}
					return html;
				}
			});
		}
		config.push({
			field: 'Actions',
			title: 'Действия',
			sortable: false,
			overflow: 'visible',
			textAlign: 'center',
			autoHide: !1,
			template: (row, index, datatable) => {
				let visible = this.user.hasPermissionTo('manage_' + this.baseUrl + 's');
				let dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
				let result = '<button href="javascript:;" data-id=' + row['id'] + ' class="editModel btn btn-hover-brand btn-icon btn-pill" title="Редактировать">\
                        <i class="la la-edit"></i>\
                    </button>';
				if (visible) {
					result +=
						'<button href="javascript:;" data-id=' + row['id'] + ' class="deleteModel btn btn-hover-danger btn-icon btn-pill" title="Удалить">\
                        <i class="la la-trash"></i>\
                    </button>';
				}
				return result;
			},
		});
		return config;
	}
}
