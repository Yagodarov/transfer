import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsEditComponent } from './clients-edit/clients-edit.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule,
	MatExpansionModule,
	MatIconModule,
	MatInputModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSnackBarModule, MatSortModule,
	MatTableModule,
	MatTabsModule,
	MatTooltipModule
} from "@angular/material";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {PartialsModule} from "../../partials/partials.module";
import {JwtInterceptor} from "../../../core/auth/_interceptor/interceptor";
import {ClientEditResolver} from "../../../core/client/_resolvers/clients";
import { ClientsBalanceComponent } from './clients-balance/clients-balance.component';
import {ClientBalanceDialogComponent} from "./clients-balance/modal/client-balance-dialog.component";

const routes: Routes = [
	// { path: '', redirectTo: 'list'},
	{ path: '', component : ClientsListComponent },
	{ path: 'edit', component : ClientsEditComponent , resolve : {
			model : ClientEditResolver
		}}
];
@NgModule({
  declarations: [ClientsListComponent, ClientsEditComponent, ClientsBalanceComponent, ClientBalanceDialogComponent],
  imports: [
	  CommonModule,
	  HttpClientModule,
	  PartialsModule,
	  RouterModule.forChild(routes),
	  FormsModule,
	  ReactiveFormsModule,
	  TranslateModule.forChild(),
	  MatButtonModule,
	  MatMenuModule,
	  MatSelectModule,
	  MatInputModule,
	  MatTableModule,
	  MatAutocompleteModule,
	  MatRadioModule,
	  MatIconModule,
	  MatNativeDateModule,
	  MatProgressBarModule,
	  MatDatepickerModule,
	  MatCardModule,
	  MatPaginatorModule,
	  MatSortModule,
	  MatCheckboxModule,
	  MatProgressSpinnerModule,
	  MatSnackBarModule,
	  MatExpansionModule,
	  MatTabsModule,
	  MatTooltipModule,
	  MatDialogModule
  ],
	entryComponents: [
		ClientBalanceDialogComponent
	],
	providers: [
		ClientEditResolver,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},
	]
})
export class ClientsModule { }
