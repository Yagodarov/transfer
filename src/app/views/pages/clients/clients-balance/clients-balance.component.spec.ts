import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsBalanceComponent } from './clients-balance.component';

describe('ClientsBalanceComponent', () => {
  let component: ClientsBalanceComponent;
  let fixture: ComponentFixture<ClientsBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
