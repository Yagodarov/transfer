import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, ɵEMPTY_ARRAY} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpClient} from "@angular/common/http";
export interface DialogData {
	type: string;
	client_id: string;
}
@Component({
	selector: 'kt-client-balance-dialog',
	templateUrl: './client-balance-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClientBalanceDialogComponent {
	form = {
		value : '0',
		comment : '',
		client_id : '',
		type : ''
	};
	errors : any;
	constructor(public dialogRef: MatDialogRef<ClientBalanceDialogComponent>,
				@Inject(MAT_DIALOG_DATA) public data: DialogData,
				private cdr : ChangeDetectorRef,
				private htpp: HttpClient) {
	}

	getTitle() {
		if (this.data.type == '0') {
			return "Списывание баланса";
		} else if (this.data.type == '1'){
			return "Пополнение баланса";
		} else {
			return "Неихвестно";
		}
	}

	submitDialog() {
		this.form.client_id = this.data.client_id;
		this.form.type = this.data.type;
		this.htpp.post('api/client_balance', this.form).subscribe(() => {
			this.dialogRef.close(true);
		}, (error) => {
			this.cdr.detectChanges();
			this.errors = error['error'];
		});
	}

	closeDialog() {
		this.dialogRef.close(false);
	}
}
