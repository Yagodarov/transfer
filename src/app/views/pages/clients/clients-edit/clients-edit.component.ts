import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs/index";
import {User} from "../../../../core/auth";
import {HttpClient} from "@angular/common/http";
import {SubheaderService} from "../../../../core/_base/layout";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'kt-clients-edit',
  templateUrl: './clients-edit.component.html',
  styleUrls: ['./clients-edit.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClientsEditComponent implements OnInit, AfterViewInit {
	config = {
		orientation : 'horizontal'
	};
	balanceData : any;
	loading$: Observable<boolean>;
	title = "";
	selectedTab = 0;
	state : string;
	baseUrl = 'client';
	hasFormErrors : boolean = false;
	model : any = {};
	modelForm : FormGroup;
	user = new User();
	roles : any = {};
	errors : any = [];
	constructor(
		private subheader: SubheaderService,
		private userModel: User,
		private router: Router,
		private titleService: Title,
		private http: HttpClient,
		private Fb:FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute) {

	}

	ngOnInit() {
		this.createForm();
		this.initFormValues();
	}

	ngAfterViewInit() {
		this.setInputMasks();
	}

	setInputMasks() {
		$('#phone').inputmask('mask', {
			'mask': '+7 (999) 999-9999',
			oncomplete: (e) => {
				console.log(e);
				this.modelForm.controls['phone'].setValue(e.target.value);
			},
		});
		$("#phone").change((e) => {
			console.log(e);
			this.modelForm.controls['phone'].setValue(e.target.value);
		});
	}

	setTitles() {
		let breadcrumbs = [{
			title : 'Список клиентов',
			page : '/users'
		}];
		this.subheader.setTitle("Клиенты");
		if (this.state == 'edit') {
			this.title = "Редактирование клиента";
			breadcrumbs.push({
				title : 'Редактирование',
				page : '/clients/edit?id='+this.modelForm.controls['id'].value,
			});
		} else {
			this.title = "Добавление клиента";
			breadcrumbs.push({
				title : 'Добавление',
				page : '/clients/create'
			});
		}
		this.subheader.setBreadcrumbs(breadcrumbs);
		this.titleService.setTitle(this.title);
	}

	getComponentTitle() {
		return this.title;
	}

	submitted = false;

	reset() {
		this.modelForm.reset();
	}

	onSubmit(redirect) {
		if (this.state == 'edit') {
			this.http.put('api/client', this.modelForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/clients']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		} else {
			this.http.post('api/client', this.modelForm.value).subscribe((data) => {
				if (redirect) this.router.navigate(['/clients']);
				this.hasFormErrors = false;
			}, (data) => {
				this.errors = data.error;
				this.hasFormErrors = true;
				this.cdr.detectChanges();
			});
		}
	}

	initFormValues() {
		this.route.queryParams.subscribe((params) => {
			const id = params['id'];
			if (id > 0) {
				this.state = 'edit';
				this.route.data
					.subscribe((data) => {
						if (data['model']) {
							this.balanceData = {
								id : data['model'].id,
								total : data['model'].total
							};
							this.modelForm.patchValue(data['model'], {
								emitEvent : false,
								onlySelf : true
							});
						}
					});

			} else {
				this.state = 'create';
			}
			this.setTitles();
		});
	}

	createForm() {
		this.modelForm = this.Fb.group({
			id: '',
			first_name: [''],
			last_name: [''],
			phone: [''],
			password: [''],
			blocked: [0],
		});
	}

	formConfig() {
		return [
			{
				field: 'first_name',
				label: 'Имя',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'last_name',
				label: 'Фамилия',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'phone',
				label: 'Телефон',
				type: 'text',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'password',
				label: 'Пароль',
				type: 'password',
				permission: 'manage_' + this.baseUrl + 's',
			},
			{
				field: 'blocked',
				label: 'Блокировка',
				type: 'select',
				permission: 'manage_' + this.baseUrl + 's',
				options: [
					{
						value: 0,
						label: 'Нет'
					}, {
						value: 1,
						label: 'Да'
					},
				],
				nullValue: false
			},
		]
	}

	isFieldVisible(permission) {
		return this.userModel.hasPermissionTo(permission);
	}

	trackByFn(index: any, item: any) {
		return index;
	}

	getErrorMessage(field) {
		if (this.errors[field]) {
			return this.errors[field][0];
		} else {
			return '';
		}
	}

	hasError(field)
	{
		let classList = {
			'is-invalid' : this.errors[field],
			'form-control' : true,
		};
		return classList;
	}
}
