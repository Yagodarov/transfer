/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Town} from "../../town/_models/town";
import {Client} from "../models/client";

@Injectable()
export class ClientsListResolver implements Resolve<any>{
	constructor(private http: HttpClient,private activatedRoute: ActivatedRoute) {
	}
	resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Client[]> {
		return this.http.get<Client[]>('/api/clients').pipe(
			map((res) =>
				res['models'].map((model: Client) => new Client(model))
			)
		);
	}

}
