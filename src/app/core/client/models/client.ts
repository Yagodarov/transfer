export class Client
{
	id : string;
	first_name : string;
	last_name : string;
	label : string;
	value : string;
	constructor( Client?: any ) {
		if( Client ) {
			Object.assign(this, Client);
			this.label = this.first_name + ' ' + this.last_name;
			this.value = this.id;
		}
	}
	getLabel() {
		return this.first_name + ' ' + this.last_name;
	}
}
