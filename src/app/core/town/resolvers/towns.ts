import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {Town} from "../_models/town";
import {map} from "rxjs/operators";
declare var Object : any;
@Injectable()
export class TownsListResolver implements Resolve<any>{

	constructor(private http: HttpClient,private activatedRoute: ActivatedRoute) {
	}

	resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Town[]> {
		return this.http.get<Town[]>('/api/towns').pipe(
			map((res) =>
				res['models'].map((town: Town) => new Town(town))
			)
		);
	}
}
