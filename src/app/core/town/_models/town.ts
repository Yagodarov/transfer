export class Town
{
	id : number;
	label : string;
	constructor( Town?: any ) {
		if( Town ) {
			Object.assign(this, Town);
		}
	}
}
