import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

declare var JSON:any;

@Injectable()
export class UserNotificationService {
	url = '/api/listennotify';
	request : any;
	constructor(private http: HttpClient, private router: Router) {

	}

	start() {
		// if (window.location.origin != 'http://roistat') {
		// 	this.request = this.http.request('get', this.url).subscribe((data) => {
		// 		if(data['contact_id']) {
		// 			this.router.navigate(['/contacts/edit',], { queryParams: { id: data['contact_id'] } });
		// 		}
		// 		else {
		// 			this.start();
		// 		}
		// 	}, error => {
		// 		setTimeout(() => {
		// 			this.start()
		// 		}, 1000	);
		// 	});
		// }
	}

	stop() {
		if (this.request) this.request.unsubscribe();
	}
}
