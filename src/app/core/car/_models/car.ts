export class Car {
    id: number;
    mark: string;
    model: string;
    number: number;
    color: string;
    year: number | null;
    seat: number;
    conditioner: number = 0;
    bank_card: number = 0;
    baby_seat: number = 0;
	baggage_count: number = 0;
    blocked: number = 0;
    forward: string;
    right: string;
    salon: string;
    baggage: string;
	label: string;
	value: any;
    getParam(param) {
    	return this[param];
	}
	getLabel() {
		return this.mark + ' ' + this.model + ' ' + this.number + ' ' + this.year + ' ' + this.color;
	}
    constructor(Car?: any) {
        if (Car) {
            Object.assign(this, Car);
			this.label = this.getLabel();
			this.value = this.id;
        }
    }
}
