/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Car} from "../_models/car";

@Injectable()
export class CarListResolver implements Resolve<any>{
	constructor(private http: HttpClient,private activatedRoute: ActivatedRoute) {
	}
	resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Car[]> {
		return this.http.get<Car[]>('/api/cars?all=true').pipe(
			map((res) =>
				res['models'].map((car: Car) => {
					let car2 = new Car(car);
					return car2;
				})
			)
		);
	}

}
