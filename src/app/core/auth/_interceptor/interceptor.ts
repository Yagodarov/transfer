import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import { Observable} from 'rxjs';
import { tap } from 'rxjs/operators';
import {AuthService} from '../_services';
import {select, Store} from "@ngrx/store";
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	constructor(private router: Router, private auth: AuthService) {
	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// add authorization header with jwt token if available
		// console.log(request.headers);
		// console.log(next);
		let token = this.auth.getUserToken();
		if (token) {
			request = request.clone({
				setHeaders: {
					Authorization: token
				}
			});
			// request.headers.append('Access-Control-Allow-Origin', 'http://localhost:3377');
			request.headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
			request.headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
			request.headers.append('Access-Control-Allow-Credentials', 'true');
		}

		return next.handle(request).pipe(tap(event => {
			if (event instanceof HttpErrorResponse) {
				if ((event.status === 401 || event.status === 400)) {
					localStorage.clear();
					this.router.navigate(['login']);
				}

			}
		}));
		// return next.handle(request).pipe(event => {
		// }, err => {
		// 	// console.log(err);
		// 	if (err instanceof HttpErrorResponse) {
		// 		if ((err.status === 401 || err.status === 400)) {
		// 			localStorage.clear();
		// 			this.router.navigate(['login']);
		// 		}
        //
		// 	}
		// });
	}
}
