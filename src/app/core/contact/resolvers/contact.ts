/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";

@Injectable()
export class ContactEditResolver implements Resolve<any>{
	constructor(private http: HttpClient,private activatedRoute: ActivatedRoute) {
	}
	resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any> {
		return this.http.get('/api/contact/'+route.queryParams.id);
	}

}
