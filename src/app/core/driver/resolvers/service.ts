/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable()
export class DriverEditResolver implements Resolve<any>{
	constructor(private http: HttpClient,private activatedRoute: ActivatedRoute) {
	}
	resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any> {
		if (route.queryParams.id) {
			return this.http.get('/api/driver/'+route.queryParams.id).pipe(
				map((res) => {
						let cars = [];
						res['cars'].forEach((item) => {
							cars.push(item['car_id']);
						});
						res['cars'] = cars;
						return res;
					}
				)
			);
		} else {
			return;
		}
	}

}
