/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Driver} from "../_models/driver";

@Injectable()
export class DriverListResolver implements Resolve<any>{
	constructor(private http: HttpClient,private activatedRoute: ActivatedRoute) {
	}
	resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Driver[]> {
		return this.http.get<Driver[]>('/api/drivers?all=true').pipe(
			map((res) =>
				res['models'].map((driver: Driver) => {
					let models = new Driver(driver);
					return models;
				})
			)
		);
	}

}
