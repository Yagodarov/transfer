export class Driver {
    id: string;
	town_id: string;
	first_name: string;
	last_name: string;
	phone: string;
	password: string;
	label: string;
	value: any;
	cars: null;
    getParam(param) {
    	return this[param];
	}
	getLabel() {
		return this.first_name + ' ' + this.last_name;
	}
    constructor(Driver?: any) {
        if (Driver) {
            Object.assign(this, Driver);
			this.label = this.first_name + ' ' + this.last_name;
			this.value = this.id;
        }
    }
}
